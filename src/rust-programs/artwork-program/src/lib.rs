use std::str;
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    pubkey::Pubkey,
		program_error::ProgramError,
		decode_error::DecodeError,
		program::invoke_signed,
};
use arrayref::{array_mut_ref, array_ref, array_refs, mut_array_refs};
use thiserror::Error;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct AccountData {
	pub rights: [Pubkey; 20],
	pub title: [u8; 20]
}

#[derive(Clone, Debug, Error)]
pub enum Errors {
	#[error("Invalid arguments")]
	InvalidArgument,
}

impl From<Errors> for ProgramError {
	fn from(e: Errors) -> Self {
			ProgramError::Custom(e as u32)
	}
}
impl<T> DecodeError<T> for Errors {
	fn type_of() -> &'static str {
			"TokenError"
	}
}

entrypoint!(process);

pub fn process(
	program_id:&Pubkey,
	accounts: &[AccountInfo],
	instruction_data: &[u8]
) -> ProgramResult {
	let (&instruction, rest) = instruction_data.split_first().ok_or(Errors::InvalidArgument)?;
	msg!("instruction = {:?}", instruction);
	match instruction {
		0 => process_instruction(program_id, accounts, rest),
		1 => process_activate_licence(program_id, accounts, rest),
		_ => Err(Errors::InvalidArgument.into())
	}

}

pub fn process_instruction(
	program_id:&Pubkey,
	accounts: &[AccountInfo],
	instruction_data: &[u8],
) -> ProgramResult{
	
	let accounts_iter = &mut accounts.iter();
	let account = next_account_info(accounts_iter)?;

	if account.owner != program_id {
		msg!("Account does not have the correct program id {} {}", account.owner, program_id);
		return Err(ProgramError::IncorrectProgramId);
	}
	if instruction_data.len() == 0 {
		msg!("Invalid instruction data sent {} {}", account.owner, program_id);
		return Err(ProgramError::InvalidInstructionData);
	}

	let mut i: usize = 0;
	let length = instruction_data.len();
	let dst = &mut account.data.borrow_mut();
	while i < length {
		dst[i] = instruction_data[i];
		i = i + 1;
	}
	Ok(())
}


pub fn process_activate_licence(
	program_id:&Pubkey,
	accounts: &[AccountInfo],
	instruction_data: &[u8]
) -> ProgramResult {
	let account_info_iter = &mut accounts.iter();
	let user_lort_info = next_account_info(account_info_iter)?;
	let licence_lort_info = next_account_info(account_info_iter)?;
	let user_licence_mint_info = next_account_info(account_info_iter)?;
	let licence_mint_info = next_account_info(account_info_iter)?;
	let user_authority_info = next_account_info(account_info_iter)?;
	let mint_authority_info = next_account_info(account_info_iter)?;
	let token_program_info = next_account_info(account_info_iter)?;



	let input = array_ref![instruction_data, 0, 10];
	
	let (nonce, licenceNonce, inputAmount) = array_refs![input, 1, 1, 8];
	
	let amount = u64::from_le_bytes(*inputAmount);

	token_transfer_lorts(
		token_program_info.clone(),
		user_lort_info.clone(), 
		licence_lort_info.clone(), 
		user_authority_info.clone(),
		nonce[0],
		amount
	)?;

	token_mint_to(
		token_program_info.clone(), 
		licence_mint_info.clone(), 
		user_licence_mint_info.clone(), 
		mint_authority_info.clone(),
		licenceNonce[0],
		1
	)?;

	Ok(())
}

pub fn token_transfer_lorts<'a>(
	token_program: AccountInfo<'a>,
	source: AccountInfo<'a>,
	destination: AccountInfo<'a>,
	authority: AccountInfo<'a>,
	nonce: u8,
	amount: u64,
) -> Result<(), ProgramError> {
	let swap_bytes = b"artcoins";
	let authority_signature_seeds = [&swap_bytes[..8], &[nonce]];
	let signers = &[&authority_signature_seeds[..]];
	let ix = spl_token::instruction::transfer(
			token_program.key,
			source.key,
			destination.key,
			authority.key,
			&[],
			amount,
	)?;
	invoke_signed(
			&ix,
			&[source, destination, authority, token_program],
			signers,
	)
}

pub fn token_mint_to<'a>(
	token_program: AccountInfo<'a>,
	mint: AccountInfo<'a>,
	destination: AccountInfo<'a>,
	authority: AccountInfo<'a>,
	nonce: u8,
	amount: u64,
) -> Result<(), ProgramError> {
	let swap_bytes =  b"artcoins"; //swap.to_bytes();
	let authority_signature_seeds = [&swap_bytes[..8], &[nonce]];
	let signers = &[&authority_signature_seeds[..]];
	let ix = spl_token::instruction::mint_to(
			token_program.key,
			mint.key,
			destination.key,
			authority.key,
			&[],
			amount,
	)?;
	//msg!("M I N T TO: {} {} {} {} {} {}",token_program.key,mint.key,destination.key,authority.key,amount,destination.is_writable);
	invoke_signed(&ix, &[mint, destination, authority, token_program], signers)
}