use std::str;
use std::cell::{RefCell, RefMut};
use std::rc::Rc;
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    pubkey::Pubkey,
		program_error::ProgramError,
		decode_error::DecodeError,
};
use thiserror::Error;

#[derive(Clone, Debug, Error)]
pub enum Errors {
	#[error("Invalid arguments")]
	InvalidArgument,
}

impl From<Errors> for ProgramError {
	fn from(e: Errors) -> Self {
			ProgramError::Custom(e as u32)
	}
}
impl<T> DecodeError<T> for Errors {
	fn type_of() -> &'static str {
			"TokenError"
	}
}

entrypoint!(process_instruction);

pub fn process_instruction(
	program_id:&Pubkey,
	accounts: &[AccountInfo],
	instruction_data: &[u8],
) -> ProgramResult{
	
    let accounts_iter = &mut accounts.iter();
	let account = next_account_info(accounts_iter)?;

	if account.owner != program_id {
		msg!("Account does not have the correct program id {} {}", account.owner, program_id);
		return Err(ProgramError::IncorrectProgramId);
	}
	if instruction_data.len() == 0 {
		msg!("Invalid instruction data sent {} {}", account.owner, program_id);
		return Err(ProgramError::InvalidInstructionData);
	}

	let (action, data) = instruction_data.split_at(1);
	msg!("Action {}", action[0]);
	
	match action[0]
	{
		0 => {
			msg!("Create Action!");
			wtite_account_data(account, data);
		},
		1 => {
			msg!("Edit Action!");
			wtite_account_data(account, data);
		},
		2 => {
			// let wallet = next_account_info(accounts_iter)?;
			// let mut t: &mut[u8] = &mut[];
			// let c = RefCell::new(t);
			// account.data.swap(&c);
			// *account.data.borrow_mut() = &mut[];
			msg!("Delete Action!");
		},
		_ => {
            msg!("Non matching instruction!");
			return Err(ProgramError::InvalidInstructionData);
        }
	}
	Ok(())
}

fn wtite_account_data(account:&AccountInfo, data:&[u8])
{
	let mut i: usize = 0;
		let length = data.len();
		let dst = &mut account.data.borrow_mut();
		msg!("Instruction data length {}", length);
		while i < length {
			dst[i] = data[i];
			i = i + 1;
		}
}