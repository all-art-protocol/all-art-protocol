use std::str;
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    pubkey::Pubkey,
		program_error::ProgramError,
		decode_error::DecodeError,
};
use thiserror::Error;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct AccountData {
	pub rights: [Pubkey; 20],
	pub title: [u8; 20]
}

#[derive(Clone, Debug, Error)]
pub enum Errors {
	#[error("Invalid arguments")]
	InvalidArgument,
}

impl From<Errors> for ProgramError {
	fn from(e: Errors) -> Self {
			ProgramError::Custom(e as u32)
	}
}
impl<T> DecodeError<T> for Errors {
	fn type_of() -> &'static str {
			"TokenError"
	}
}

entrypoint!(process_instruction);

pub fn process_instruction(
	program_id:&Pubkey,
	accounts: &[AccountInfo],
	instruction_data: &[u8],
) -> ProgramResult{
	
    let accounts_iter = &mut accounts.iter();
	let account = next_account_info(accounts_iter)?;

	if account.owner != program_id {
		msg!("Account does not have the correct program id {} {}", account.owner, program_id);
		return Err(ProgramError::IncorrectProgramId);
	}
	if instruction_data.len() == 0 {
		msg!("Invalid instruction data sent {} {}", account.owner, program_id);
		return Err(ProgramError::InvalidInstructionData);
	}

	msg!("Write User Account");

	let mut i: usize = 0;
	let length = instruction_data.len();
	let dst = &mut account.data.borrow_mut();
	msg!("Instruction data length {}", length);
	while i < length {
		// msg!("Write byte {}", i);
		dst[i] = instruction_data[i];
		i = i + 1;
	}
	Ok(())
}
