import {FC, useState} from 'react';
import { arrayBufferToBase64 } from '../../../client/utils/ipfs';
import { fromByteArray } from '../../../client/utils/string';
import GLBRenderer from '../image/GLBRenderer';
import { getFileTypeFromBuffer } from '../../../client/utils/fileType';
import './style.css';

const ImageSelector:FC<{
    className?:string,
    alt?:string,
    onChange?:(buffer:Buffer)=>void
}> = (props)=>
{
    const [img, setImg] = useState<string | null>(null);
    const [glb, setGlb] = useState<Buffer | null>(null);
    const [showUpload, setShowUpload] = useState<boolean>(false);

    async function updateImagePreview(buffer:Buffer)
    {
        console.log('File Type: ', getFileTypeFromBuffer(buffer));
        const fileType = getFileTypeFromBuffer(buffer);
        if(fileType === undefined)
            alert("Unsupported file type!");
        else
        {
            if(fileType === 'glTF')
                setGlb(buffer);
            else
                setImg(await arrayBufferToBase64(buffer));
        }
    }


    return(
        <div 
            className={(props.className) ? props.className : "image-selector"}
            onMouseOver={(e)=>{
                setShowUpload(true);
            }}
            onMouseLeave={(e)=>{
                setShowUpload(false);
            }}
        >
            {
                (img) && <img src={`${img}`} alt={(props.alt)?props.alt:""} />
            }
            {
                (glb) && <GLBRenderer data={glb} width={300} height={300} />
            }
            {
                ((img === null && glb === null) || showUpload)&&
                <label htmlFor="file-upload" className="custom-file-upload">
                    <i className="fa fa-cloud-upload"></i>Upload Preview
                </label>
            }
            {/* <input style={{display:"none"}} type="file" id="file-upload" accept="image/png, image/jpeg" multiple={false} onChange={(e)=>{ */}
            <input style={{display:"none"}} type="file" id="file-upload" multiple={false} onChange={(e)=>{
                e.preventDefault()
                if(e.target.files && e.target.files.length > 0)
                {
                    setImg(null);
                    setGlb(null);

                    const file = e.target.files[0];
                    const reader = new FileReader();
                    reader.readAsArrayBuffer(file);
                    reader.onloadend = () => 
                    {
                        if(reader.result)
                        {
                            const buf = Buffer.from(reader.result);
                            updateImagePreview(buf);
                            if(props.onChange)
                                props.onChange(buf);
                        }
                    }
                }
            }} />
        </div>
    );
};
export default ImageSelector;