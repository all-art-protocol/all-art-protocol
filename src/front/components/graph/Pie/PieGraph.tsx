import React, { FC, useState } from "react";
import {
	ResponsiveContainer,
	PieChart,
	Pie,
	Cell,
	Sector,
	Tooltip,
	Legend
} from "recharts";
import { Payload } from "recharts/types/component/DefaultLegendContent";
import { IPieGraphData } from '../IGraphData';
import "./style.css"

export interface IPieGraphStyle
{
	dataKey:string,
	nameKey:string,
	stroke:string,
	innerRadius:number | string,
	outerRadius:number | string,
	innerFontSize:string,
	innerFontOffset:number,
	innerFontColor:string,
	outerFontSize:string,
	outerFontOffset:number,
	outerFontColor:string,
}

const PieGraph: FC<{
	data: IPieGraphData[],
	style: IPieGraphStyle,
	className?:string,
	legend?:Payload[]
}> = (props) => {

	const [index, setIndex] = useState<number>(0);
	const [zIndex, setZIndex] = useState<number>(10);
	const classNames = (props.className) ? `pie-graph ${props.className}` : 'pie-graph';
	const renderActiveShape = (config) => {
		const RADIAN = Math.PI / 180;
		const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = config;
		const sin = Math.sin(-RADIAN * midAngle);
		const cos = Math.cos(-RADIAN * midAngle);
		
		const sx = cx + (outerRadius + 10) * cos;
		const sy = cy + (outerRadius + 10) * sin;
		const mx = cx + (outerRadius + 15) * cos;
		const my = cy + (outerRadius + 15) * sin;
		const ex = mx + (cos >= 0 ? 1 : -1) * 22;
		const ey = my;
		
		const textAnchor = cos >= 0 ? 'start' : 'end';

		return (
			<g>
				{/* <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}> */}
				<text x={cx} y={cy} dy={props.style.innerFontOffset} textAnchor="middle" fill="#FFF" fontSize={props.style.innerFontSize}>
					{`${value} ${payload.name}`}
					{/* {payload.name} */}
				</text>
				<Sector
					cx={cx}
					cy={cy}
					innerRadius={innerRadius}
					outerRadius={outerRadius}
					startAngle={startAngle}
					endAngle={endAngle}
					fill={fill}
					stroke="#580d2c"
				/>
				<Sector
					cx={cx}
					cy={cy}
					startAngle={startAngle}
					endAngle={endAngle}
					innerRadius={outerRadius + 6}
					outerRadius={outerRadius + 10}
					fill={fill}
					// stroke="#580d2c"
				/>
				<path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
				<circle cx={ex} cy={ey} r={5} fill={fill} stroke="none" />
				{/* <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#FFF">{`LORT's ${value}`}</text> */}
				<text 
					x={ex + (cos >= 0 ? 1 : -1) * 12} 
					y={ey} 
					dy={props.style.outerFontOffset} 
					textAnchor={textAnchor} 
					fill="#FFF"
					fontSize={props.style.outerFontSize}
				>
					{`${(percent * 100).toFixed(2)}%`}
					{/* {`(Percent ${(percent * 100).toFixed(2)}%)`} */}
				</text>
			</g>
		);
	};

	return (
		<div className={classNames} style={{zIndex:zIndex}}>
			<ResponsiveContainer width="100%" height="100%">
				<PieChart>
					{/* <Tooltip
						position={{ x: 0, y: 0 }}
						cursor={{ stroke: "white", strokeWidth: 0.1 }}
						content={({ active, payload, label }) => {
							if (payload && payload.length > 0 && payload[0].payload) {
								const sample = payload[0].payload
								return (
									<div className="pie-graph-tooltip">
										<h1>{`${sample.valuePrefix}${sample.value}`}</h1>
									</div>
								)
							}
							return null;
						}}
					/> */}
					<Pie
						{...props.style}
						data={props.data}
						activeIndex={index}
						activeShape={renderActiveShape}
						animationBegin={300}
						animationDuration={700}
						label={false}
						onMouseEnter={(_, index)=>{
							setIndex(index);
							setZIndex(9999);
						}}
						onMouseOut={(data, index, e)=>{
							setZIndex(10);
						}}
					>
						{
							props.data.map((item, index) => {
								return (
									<Cell key={`pie-cell-${index}`} fill={item.color} />
								);
							})
						}
					</Pie>
					{/* {
						(props.legend && props.legend.length) &&
						<Legend verticalAlign="bottom" payload={props.legend} height={36}/>
					} */}
				</PieChart>
			</ResponsiveContainer>
		</div>
	)
}
export default PieGraph
