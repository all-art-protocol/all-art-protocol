import React, { FC } from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import { IGraphData } from '../IGraphData';
import "./style.css"

const AreaGraph: FC<{
  label?: string;
  data: IGraphData[]
}> = (props) => {
  return (
    <div className="area-graph">
      <ResponsiveContainer width="100%" height="100%">
        <AreaChart
          data={props.data}
          margin={{
            top: 5,
            right: 5,
            left: 5,
            bottom: 0,
          }}
        >
          <defs>
            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
              <stop offset="0%" stopColor="#ac0053" stopOpacity={1} />
              <stop offset="100%" stopColor="#1d000e" stopOpacity={0.1} />
            </linearGradient>
          </defs>
          <XAxis dataKey="name" tickSize={0} axisLine={false} interval={2} />
          <YAxis hide={true} />
          <Tooltip
            position={{ x: 0, y: 0 }}
            cursor={{ stroke: "white", strokeWidth: 0.1 }}
            content={({ active, payload, label }) => {
              let sample = props.data[props.data.length - 1]
              if (payload && payload.length > 0 && payload[0].payload)
                sample = payload[0].payload
              return (
                <div className="graph-tooltip">
                  {label && <h3>{props.label}</h3>}
                  <h1>{`${sample.valuePrefix}${sample.value}`}</h1>
                  <h6>{sample.date}</h6>
                </div>
              )
            }}
          />
          <Area
            type="monotone"
            dataKey="value"
            stroke="#E3036F"
            fill="url(#colorUv)"
            strokeWidth="2"
          />
        </AreaChart>
      </ResponsiveContainer>
    </div>
  )
}
export default AreaGraph
