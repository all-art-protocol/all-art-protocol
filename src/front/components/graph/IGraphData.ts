export interface IGraphData {
	name: string | number;
	date: string;
	valuePrefix: string;
	value: number;
}

export interface IPieGraphData extends IGraphData {
	color: string;
}