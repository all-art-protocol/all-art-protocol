import React, { FC } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import { IGraphData } from '../IGraphData';
import "./style.css";

const BarGraph: FC<{
  label?: string;
  data: IGraphData[]
}> = (props) => {
  return (
    <div className="bar-graph">
      <ResponsiveContainer width="100%" height="100%">
        <BarChart
          data={props.data}
          margin={{
            top: 5,
            right: 5,
            left: 5,
            bottom: 0,
          }}
        >
          <defs>
            <linearGradient id="colorBar" x1="0" y1="0" x2="0" y2="1">
              <stop offset="0%" stopColor="#2172E5" stopOpacity={1} />
              <stop offset="100%" stopColor="#57ccbe" stopOpacity={1} />
            </linearGradient>
          </defs>
          <XAxis dataKey="name" tickSize={0} axisLine={false} interval={2} />
          <YAxis hide={true} />
          <Tooltip
            position={{ x: 0, y: 0 }}
            cursor={{
              fill: "rgba(255, 255, 255, .2)",
            }}
            content={({ active, payload, label }) => {
              let sample = props.data[props.data.length - 1]
              if (payload && payload.length > 0 && payload[0].payload)
                sample = payload[0].payload
              return (
                <div className="graph-tooltip">
                  {props.label && <h3>{props.label}</h3>}
                  <h1>{`${sample.valuePrefix}${sample.value}`}</h1>
                </div>
              )
            }}
          />
          <Bar
            dataKey="value"
            fill="url(#colorBar)"
            shape={(props) => {
              return (
                <rect
                  fill={props.fill}
                  x={props.x}
                  y={props.y}
                  width={props.width}
                  height={props.height}
                  rx="3"
                />
              )
            }}
          />
        </BarChart>
      </ResponsiveContainer>
    </div>
  )
}
export default BarGraph
