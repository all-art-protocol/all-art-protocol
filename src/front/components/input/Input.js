import React from "react";
import { build } from "./factory";
import * as inputTypes from './types';
// import styles from "./style.scss";

export default class Input extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            className: (this.props.className) ? [this.props.className] : []
        };
    }

    render = () =>
    {
        const props = {
            ...this.normalizedProps(),
            className: (this.props.isValidated) ? this.className(this.props.isValid) : this.state.className.join(" "),
            onChange:(event) => {
                this.props.onChange?.(event);
                if(this.props.type === inputTypes.CHECKBOX)
                    this.validate(event.target.checked);
                else
                    this.validate(event.target.value);
            },
        };

        return build(this.props.type, props, this.props.children);
    };

    validate = (value) =>
    {
        try
        {
            let isValid = (this.props.validator)
                ? this.props.validator(value) 
                : true;
            this.props.onValidate?.(this.props.id, isValid, value);
            this.setState({
                className: this.className(isValid),
                isValid: isValid,
            });
        }
        catch(ex){console.log(ex)}
    }

    className = (isValid) => {
        const className = [];
        if(this.props.className)
            className.push(this.props.className);
        if(isValid && this.props.validStyle)
            className.push(this.props.validStyle);
        if(!isValid && this.props.invalidStyle)
            className.push(this.props.invalidStyle);
        return className;
    };

    normalizedProps = () =>
    {
        const {
            type, 
            validator,
            className,
            validStyle,
            invalidStyle,
            onValidate,
            onChange,
            children,
            validate,
            isValidated,
            isValid,
            ...htmlProps
        } = this.props;
        return htmlProps;
    };
}