import React from "react";
import * as types from "./types"

export const build = (type, props, children) => {
    switch(type)
    {
        case types.TEXT:
            return <input type="text" {...props}/>;
        case types.NUMBER:
            return <input type="number" {...props}/>;
        case types.EMAIL:
            return <input type="email" {...props}/>;
        case types.PHONE:
            return <input type="tel" {...props}/>;
        case types.PASSWORD:
            return <input type="password" {...props}/>;
        case types.SEARCH:
            return <input type="search" {...props}/>;
        case types.URL:
            return <input type="url" {...props}/>;
        case types.CHECKBOX:
            const {value, ...rest} = props;
            return <input type="checkbox" {...rest} checked={value}/>;
        case types.RADIO:
            return <input type="radio" {...props}/>;
        case types.BUTTON:
            return <button {...props}>{children}</button>;
        case types.TEXTAREA:
            return <textarea {...props}></textarea>;
        case types.SELECT:
            return <select {...props}>
                    {
                        (children !== undefined && children !== null)
                            ? children.map((child) => <option key={child.value} value={child.value}>{child.label}</option>)
                            : null
                    }
                </select>;
        default: return "";
    }
}