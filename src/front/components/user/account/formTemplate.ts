export const first_name = {
    props:{
        id:"first_name",
        name:"first_name",
        type:"text",
        placeholder:"",
        value: "",
        required: "required"
    },
    label: "Handle",
};
export const last_name = {
    props:{
        id:"last_name",
        name:"last_name",
        type:"text",
        placeholder:"",
        value: "",
        required: "required"
    },
    label: "Full Name",
};
export const country = {
    props:{
        id:"country",
        name:"country",
        type:"text",
        placeholder:"",
        value: "",
        required: "required"
        
    },
    label: "Country",
};
export const bio = {
    props:{
        id:"bio",
        name:"bio",
        type:"textarea",
        placeholder:"",
        value: "",
        // required: "required"
    },
    label: "Bio",
};