import {FC, useState} from 'react';
import {buildInputElement, buildTextboxElement, updateElementProps} from '../../form/factory';
import * as formTemplate from './formTemplate';
import UserAccount from '../../../../client/data/UserAccount';
import CreateUserAccount from '../../../../client/programs/CreateUserAccount';
import { observer } from 'mobx-react-lite';
import { useAppStore } from '../../../store/store';
import { PublicKey } from '@solana/web3.js';
import { runInAction } from 'mobx';
import { postFile } from '../../../../client/utils/ipfs';
import ImageSelector from '../../upload/ImageSelector';
import './style.css';

const CreateAccount:FC = observer(()=>
{
    const store = useAppStore();
    const [firstName, setFirstName] = useState({...formTemplate.first_name});
    const [lastName, setLastName] = useState({...formTemplate.last_name});
    const [country, setCountry] = useState({...formTemplate.country});
    const [bio, setBio] = useState({...formTemplate.bio});
    const [signature, setSignature] = useState<string | null>(null);
    const [pubkey, setPubKey] = useState<PublicKey | null>(null);
    const [imageBuffer, setImageBuffer] = useState<Buffer>();

    async function Send()
    {
        let cid = "";
        if(imageBuffer)
        {
            cid = (await postFile(imageBuffer)).toString();
        }
        const userAccount:UserAccount = new UserAccount({
            first_name:firstName.props.value,
            last_name:lastName.props.value,
            country:country.props.value,
            bio:bio.props.value,
            image:cid
        });
        const createResult = await new CreateUserAccount(store.wallet, store.walletKey, userAccount, store.cluster.url).run();
        if(createResult)
        {
            const [signatureResult, accountPublicKey] = createResult;
            
            runInAction(() => {
                store.userData = userAccount;
                store.userKey = accountPublicKey;
            });

            console.log(`Create account succeed! Signature: ${signatureResult} PublicKey: ${accountPublicKey.toBase58()}`);
            setSignature(signatureResult);
            setPubKey(accountPublicKey);
        }
        
    }

    return(
        <div>
            <h1>Create Account</h1>
            <p>First create your account that will be linked to your wallet</p>
            <div className="create-account">
                <div className="create-account-image-box">
                    <ImageSelector className="create-account-image-upload" onChange={(buffer)=>{
                        setImageBuffer(buffer);
                    }} />
                </div>
                <ul className="create-account-details">
                    <li>{buildInputElement(firstName, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setFirstName(updateElementProps(firstName, {value:value}));
                        } 
                    })}</li>
                    <li>{buildInputElement(lastName, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setLastName(updateElementProps(lastName, {value:value}));
                        } 
                    })}</li>
                    <li>{buildInputElement(country, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setCountry(updateElementProps(country, {value:value}));
                        } 
                    })}</li>
                    <li>{buildTextboxElement(bio, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setBio(updateElementProps(bio, {value:value}));
                        } 
                    })}</li>
                    {/* <li>
                        <input type="file" onChange={(e)=>{
                            e.preventDefault()
                            if(e.target.files)
                            {
                                const file = e.target.files[0];
                                const reader = new FileReader()
                                reader.readAsArrayBuffer(file)
                                reader.onloadend = () => 
                                {
                                    if(reader.result)
                                    {
                                        const buf = Buffer.from(reader.result);
                                        setImageBuffer(buf);
                                    }
                                }
                            }
                        }} />
                    </li> */}
                    {/* {
                        (signature !== null) &&
                        <li>
                            <ul>
                                <li>Signature: {signature}</li>
                                <li>PublicKey: {pubkey?.toBase58()}</li>
                            </ul>
                        </li>
                    } */}
                    <li>
                        <button onClick={Send}>Create</button>
                    </li>
                </ul>
            </div>
        </div>
    );
});
export default CreateAccount;