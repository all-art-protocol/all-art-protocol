import { PublicKey } from '@solana/web3.js';
import { observer } from 'mobx-react-lite';
import { FC, useEffect, useState } from 'react';
import { AppStore, useAppStore } from '../../../store/store';
import './style.css';
import { Link } from 'react-router-dom';
import { getFile } from '../../../../client/utils/ipfs';
import UserAccount from '../../../../client/data/UserAccount';

const Avatar:FC<{user:UserAccount | null | undefined}> = observer((props) =>
{
    const [image, setImage] = useState<string | null>(null);

    useEffect(()=>{
        readImage();
    }, [props.user]);
    async function readImage()
    {
        try {
            // console.log("Try load avatar");
            if(props.user)
            {
                const ipfsB64 = await getFile(props.user.image);
                // console.log("avatar loaded - ", ipfsB64);
                setImage(ipfsB64);
            } 
        } 
        catch (error) {console.log(error);}
    }

    return(
        <div className="account-avatar">
            <Link to="/read/user">
                <div style={{
                    width:"40px",
                    height:"40px",
                    backgroundImage:`url(${image})`,
                    backgroundSize: "cover"
                }}></div>
            </Link>
        </div>
    );
});
export default Avatar;
