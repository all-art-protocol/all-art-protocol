import { PublicKey } from '@solana/web3.js';
import { observer } from 'mobx-react-lite';
import { useState } from 'react';
import UserAccount from '../../../../client/data/UserAccount';
import ReadUserAccount from '../../../../client/programs/ReadUserAccount';
import { getFile } from '../../../../client/utils/ipfs';
import { AppStore, useAppStore } from '../../../store/store';
import {buildInputElement, updateElementProps} from '../../form/factory';
import './style.css';

const ReadAccount = observer(() =>
{
    const store:AppStore = useAppStore();
    const [hash, setHash] = useState({
        props:{
            id:"hash",
            name:"hash",
            type:"text",
            placeholder:"Your user account hash",
            value: "",
        },
        label: "",
    });
    const [account, setAccount] = useState<UserAccount | null>(null);
    const [image, setImage] = useState<string | null>(null);
    
    async function Send()
    {
        if(hash.props.value.length === 44)
        {
            const ua = await new ReadUserAccount(new PublicKey(hash.props.value), store.cluster.url).run();
            if(ua)
            {
                const img = await getFile(ua.image);
                setImage(img);
                setAccount(ua);
            }
            else
                alert(`User with hash ${hash.props.value} doesnt exist!`);
        }
        else
            alert('Please type valid hash');
    }
    
    return(
        <div className="read-account">
            <div>
                {buildInputElement(hash,{onValidate:(id:any, isValid:any, value:string)=>{
                    setHash(updateElementProps(hash, {value:value}));
                }})}
            </div>
            <button onClick={Send}>Read</button>
            {
                (account) &&
                <ul>
                    <li>First name: {account.first_name}</li>
                    <li>Last name: {account.last_name}</li>
                    <li>Country: {account.country}</li>
                    <li>Bio: {account.bio}</li>
                    <li><img className="create-artwork-preview" src={`${image}`} alt="Artwork preview" /></li>
                </ul>
            }
        </div>
    );
});
export default ReadAccount;
