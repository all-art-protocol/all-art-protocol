import { FC } from "react";
import { NavLink } from "react-router-dom";
import './style.css';

const Header:FC = ()=>{
    return (
        <nav className="header">
            <NavLink to="/">Home</NavLink>
            <NavLink to="/create/user">Create User</NavLink>
            <NavLink to="/read/user">View User</NavLink>
            <NavLink to="/create/right">Create Right</NavLink>
            <NavLink to="/create/rights">Create Predefined Rights</NavLink>
            <NavLink to="/read/rights">View Rights</NavLink>
            <NavLink to="/create/licence-category">Create Licence Category</NavLink>
            <NavLink to="/read/licence-categories">Read Licence Categories</NavLink>
            <NavLink to="/create/licence-preset">Create Licence Preset</NavLink>
            <NavLink to="/read/licence-presets">Read Licence Presets</NavLink>
            <NavLink to="/create/artwork">Create NFTPro</NavLink>
            <NavLink to="/read/artwork">Read NFTPro</NavLink>
        </nav>
    );
};
export default Header;