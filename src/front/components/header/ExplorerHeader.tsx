import { FC } from "react";
import { useAppStore } from "../../store/store";
import ClusterSelector from '../cluster/ClusterSelector';
import { clusters, defaultClusterIndex, ICluster, walletUrl } from '../../../client/core/consts';
import { WalletProviders } from "../../../client/core/enums";
import { runInAction } from 'mobx';
import logo from "../../../front/assets/img/artcoins.svg";
import { createWallet } from "../../../client/core/wallet";
import { observer } from "mobx-react-lite";

import './style.css';

const ExplorerHeader:FC = observer(()=>{
    const store = useAppStore();
    
    return (
        <header className="creator-header">
            <div className="container">
                <div className="logo">
                    <img src={logo} />
                    NFT-PRO Explorer
                </div>
                <aside>
                    <ClusterSelector
                        defaultCluster={store.cluster}
                        clusters={[...clusters]}
                        onChange={(cluster:ICluster)=>{
                            runInAction(()=>{
                                store.wallet.disconnect();
                                store.wallet = createWallet(WalletProviders.Sollet, cluster.url);
                                store.cluster = cluster;
                            });
                        }}  
                    />
                </aside>
            </div>
        </header>
    );
});
export default ExplorerHeader;