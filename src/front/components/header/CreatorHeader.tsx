import { FC } from "react";
import { AppStore, useAppStore } from "../../store/store";
import Avatar from "../user/account/Avatar";
import { ICluster } from '../../../client/core/consts';
import ClusterSelector from '../cluster/ClusterSelector';
import { clusters } from '../../../client/core/consts';
import { defaultCluster, walletUrl } from '../../../client/core/consts';
import logo from "../../../front/assets/img/artcoins.svg";
import { createWallet } from "../../../client/core/wallet";
import { WalletProviders } from "../../../client/core/enums";
import './style.css';
import { runInAction } from "mobx";

const CreatorHeader:FC = ()=>{
    const store:AppStore = useAppStore();
    return (
        <header className="creator-header">
            <div className="container">
                <div className="logo">
                    <img src={logo} />
                    NFT-PRO Creator
                </div>
                <aside>
                    {
                        (store.userData)&&
                        <div className="creator-header-avatar-container">
                            <Avatar user={store.userData} />
                            <span>{store.userData.first_name}</span>
                        </div>
                    }
                    <ClusterSelector
                        defaultCluster={store.cluster}
                        clusters={[...clusters]}
                        onChange={(cluster:ICluster)=>{
                            runInAction(()=>{
                                store.wallet.disconnect();
                                store.wallet = createWallet(WalletProviders.Sollet, cluster.url);
                                store.cluster = cluster;
                            });
                        }} 
                    />
                </aside>
            </div>
        </header>
    );
};
export default CreatorHeader;