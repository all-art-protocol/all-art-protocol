export const title = {
    props:{
        id:"title",
        name:"title",
        type:"text",
        placeholder:"Title",
        value: "",
    },
    label: "Title",
};
export const description = {
    props:{
        id:"description",
        name:"description",
        type:"textarea",
        placeholder:"Description",
        value: "",
    },
    label: "Description",
};
export const isExtended = {
    props:{
        id:"isExtended",
        name:"isExtended",
        type:"checkbox",
        value: false,
    },
    label: "Is Extended",
};