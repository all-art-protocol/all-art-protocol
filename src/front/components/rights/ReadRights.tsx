import { PublicKey, SystemProgram, Transaction } from '@solana/web3.js';
import { useState, useEffect } from 'react';
import { connect } from '../../../client/core/connection';
import { CREATE_RIGHT_PROGRAM } from '../../../client/core/consts';
import Right from '../../../client/data/Right';
import ReadRightAccounts from '../../../client/programs/ReadRightAccounts';
import { useAppStore } from '../../store/store';
import DeleteAccount from '../../../client/programs/DeleteAccountProgram';
import './style.css';

const ReadRights = () =>
{
    const store = useAppStore();
    const [rights, setRights] = useState<Right[]>([]);
    const [pubKeys, setKeys] = useState<PublicKey[]>([]);
    
    useEffect(()=>{
        loadRights()
    },[]);
    
    async function loadRights()
    {
        const {rights, keys} = await new ReadRightAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        setKeys(keys);
        setRights(rights);
    }

    async function deleteRight(right:PublicKey)
    {
        console.log("Delete Right", right.toBase58());
        // const result = await new DeleteAccount(store.wallet, store.walletKey, right, store.cluster.url).run();
        // if(result)
        //     await loadRights();
        // else
        //     alert(`Unable to delete right: ${right.toBase58()}`);
    }

    return(
        <div className="rights">
            {
                rights.reverse().map((item, index)=>{
                    return(
                        <div key={index}>
                            <ul>
                                <li>Title: {item.title}</li>
                                <li>Description: {item.description}</li>
                                <li>PublicKey: {pubKeys[index].toBase58()}</li>
                                <li>
                                    <button onClick={(e)=>{
                                        deleteRight(pubKeys[index]);
                                    }}>
                                        Delete
                                    </button>
                                </li>
                            </ul>
                        </div>
                    );
                })
            }
        </div>
    );
};
export default ReadRights;
