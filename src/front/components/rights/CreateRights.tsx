import {FC, useState} from 'react';
import Right from '../../../client/data/Right';
import { useAppStore } from '../../store/store';
import { Keypair, PublicKey, SystemProgram, Transaction } from '@solana/web3.js';
import { RIGHTS } from '../../../client/data/LicencePresets';
import { connect } from '../../../client/core/connection';
import { CREATE_RIGHT_PROGRAM } from '../../../client/core/consts';
import { sendMultipleTransactions } from '../../../client/core/transactions';
import './style.css';
import { createRight } from '../../../client/instructions/rights';

const CreateRights:FC = ()=>
{
    const store = useAppStore();

    async function Send()
    {
        const connection = await connect(store.cluster.url);
        const rights = RIGHTS;
        const programId = new PublicKey(CREATE_RIGHT_PROGRAM);
        const transactions:Transaction[] = [];
        let { blockhash } = await connection.getRecentBlockhash();
        for(let i=0; i<rights.length; i++)
        {
            const rightTemplate = rights[i];
            const salt = Keypair.generate().publicKey;
            const right = new Right({
                title:rightTemplate.title,
                description:rightTemplate.description,
                salt:salt,
            });
            const transaction = new Transaction();
            const rightAccount = await connection.getAccountInfo(new PublicKey(rightTemplate.key))
            if(rightAccount == null)
            {
                const space = right.getSize();
                const account = await PublicKey.createWithSeed(store.walletKey, rightTemplate.seed, programId);
                const rent = await connection.getMinimumBalanceForRentExemption(space);
                transaction.add(
                    SystemProgram.createAccountWithSeed({
                        fromPubkey: store.walletKey,
                        basePubkey: store.walletKey,
                        lamports: rent,
                        newAccountPubkey: account,
                        programId,
                        seed:rightTemplate.seed,
                        space,
                    })
                );
            }
            transaction.add(createRight(new PublicKey(rightTemplate.key), false, true, programId, right));
            transaction.recentBlockhash = blockhash;
            transaction.feePayer = store.walletKey;
            transactions.push(transaction);
        }
        if(transactions.length > 0)
        {
            const ids = await sendMultipleTransactions(transactions, connection, store.wallet);
            console.log("IDS ", ids);
        }
    }

    return(
        <div>
            <h1>Create predefined rights </h1>
            <button onClick={Send}>Create</button>
        </div>
    );
};
export default CreateRights;