import {FC, useState} from 'react';
import {buildInputElement, updateElementProps} from '../form/factory';
import * as formTemplate from './formTemplate';
import Right from '../../../client/data/Right';
import CreateRightAccount from '../../../client/programs/CreateRightProgram';
import { observer } from 'mobx-react-lite';
import { useAppStore } from '../../store/store';
import { Keypair, PublicKey } from '@solana/web3.js';
import './style.css';

const CreateRight:FC = observer(()=>
{
    const store = useAppStore();
    const [title, setTitle] = useState({...formTemplate.title});
    const [description, setDescription] = useState({...formTemplate.description});
    const [signature, setSignature] = useState<string | null>(null);
    const [pubkey, setPubKey] = useState<PublicKey | null>(null);

    async function Send()
    {
        const salt = Keypair.generate().publicKey;
        const createResult = await new CreateRightAccount(store.wallet, store.walletKey, new Right({
            title:title.props.value,
            description:description.props.value,
            salt:salt,
        }), store.cluster.url).run();
        
        if(createResult)
        {
            const [signatureResult, pubkeyResult] = createResult;
            console.log(`Create right succeed! Signature: ${signatureResult} PublicKey: ${pubkeyResult.toBase58()}`);
            setSignature(signatureResult);
            setPubKey(pubkeyResult);
        }
    }

    return(
        <ul className="create-rights">
            <li>{buildInputElement(title, {
                onValidate:(id:any, isValid:any, value:any)=>{
                    setTitle(updateElementProps(title, {value:value}));
                } 
            })}</li>
            <li>{buildInputElement(description, {
                onValidate:(id:any, isValid:any, value:any)=>{
                    setDescription(updateElementProps(description, {value:value}));
                } 
            })}</li>
            {
                (signature !== null) &&
                <li>
                    <ul>
                        <li>Signature: {signature}</li>
                        <li>PublicKey: {pubkey?.toBase58()}</li>
                    </ul>
                </li>
            }
            <li>
                <button onClick={Send}>Create</button>
            </li>
        </ul>
    );
});
export default CreateRight;