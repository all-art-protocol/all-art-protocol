import { PublicKey } from '@solana/web3.js';
import { useState, useEffect } from 'react';
import { CREATE_RIGHT_PROGRAM } from '../../../client/core/consts';
import { useAppStore } from '../../store/store';
import DeleteAccount from '../../../client/programs/DeleteAccountProgram';
import ReadLicenceCategoryAccounts from '../../../client/programs/ReadLicenceCategoryAccounts';
import LicenceCategory from '../../../client/data/LicenceCategory';
import ReadLicenceCategoryRightsAccounts from '../../../client/programs/ReadLicenceCategoryRightsAccounts';
import { createLicenceCategoryAccountSeed, createLicenceCategoryRightsAccountSeed } from '../../../client/core/seed';
import './style.css';

const ReadLicenceCategories = () =>
{
    const store = useAppStore();
    const [categories, setCategories] = useState<{category:LicenceCategory, key:PublicKey}[]>([]);
    
    useEffect(()=>{
        loadCategories()
    },[]);
    
    async function loadCategories()
    {
        const {categories, keys} = await new ReadLicenceCategoryAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        setCategories(categories.map((item, index)=>{
            return {
                category:item,
                key:keys[index]
            };
        }));
    }

    async function deleteCategory(item:{category:LicenceCategory, key:PublicKey})
    {
        // const rights = await new ReadLicenceCategoryRightsAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url, item.key).run();
        // if(rights)
        // {
        //     console.log(rights);
        //     const seed = await createLicenceCategoryRightsAccountSeed(rights.data.salt.toBase58(), item.key);
        //     await new DeleteAccount(
        //         store.wallet, 
        //         store.walletKey, 
        //         rights.pubkey,
        //         seed,
        //         store.cluster.url)
        //     .run();
        // }
        
        const seed = await createLicenceCategoryAccountSeed(item.category.salt.toBase58());
        await new DeleteAccount(
            store.wallet, 
            store.walletKey, 
            item.key,
            seed,
            store.cluster.url)
        .run();

        loadCategories();
    }

    return(
        <div className="licence-categories">
            {
                categories.sort((a, b)=>{
                    if(a.category.order < b.category.order)
                        return -1;
                    if(a.category.order > b.category.order)
                        return 1;
                    return 0;
                })
                .map((item, index)=>{
                    return(
                        <div className="licence-category-item" key={item.key.toBase58()}>
                            <ul className="details">
                                <li>Title: {item.category.title}</li>
                                <li>Description: {item.category.description}</li>
                                <li>Order: {item.category.order}</li>
                                <li>Parent: {(!item.category.parent) ? 'None' : item.category.parent.toBase58()}</li>
                                <li>PublicKey: {item.key.toBase58()}</li>
                            </ul>
                            <ul className="controlls">
                                <li>
                                    <button onClick={(e)=>{
                                        deleteCategory(item);
                                    }}>
                                        Delete
                                    </button>
                                </li>
                                <li>
                                    <button onClick={(e)=>{
                                        deleteCategory(item);
                                    }}>
                                        Edit
                                    </button>
                                </li>
                            </ul>
                        </div>
                    );
                })
            }
        </div>
    );
};
export default ReadLicenceCategories;
