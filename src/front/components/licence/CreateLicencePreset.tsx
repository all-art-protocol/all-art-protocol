import {FC, useEffect, useState} from 'react';
import {buildInputElement, updateElementProps} from '../form/factory';
import * as formTemplate from './licencePresetFormTemplate';
import { observer } from 'mobx-react-lite';
import { useAppStore } from '../../store/store';
import { Keypair, PublicKey } from '@solana/web3.js';
import ReadLicenceCategoryAccounts from '../../../client/programs/ReadLicenceCategoryAccounts';
import ReadRightAccounts from '../../../client/programs/ReadRightAccounts';
import { CREATE_RIGHT_PROGRAM } from '../../../client/core/consts';
import CreateLicencePresetAccount from '../../../client/programs/CreateLicencePresetAccount';
import './style.css';
import LicencePreset from '../../../client/data/LicencePreset';

const CreateLicencePreset:FC = observer(()=>
{
    const store = useAppStore();
    const [title, setTitle] = useState({...formTemplate.title});
    const [categories, setCategories] = useState<{
        pubKey:PublicKey,
        props:{
            id:string,
            name:string,
            type:string,
            value: boolean,
        },
        label:string,
    }[]>([]);
    const [rights, setRights] = useState<{
        pubKey:PublicKey,
        props:{
            id:string,
            name:string,
            type:string,
            value: boolean,
        },
        label:string,
    }[]>([]);
    
    const [signature, setSignature] = useState<string | null>(null);
    const [pubkey, setPubKey] = useState<PublicKey | null>(null);

    useEffect(()=>{
        loadLicenceCategories();
        loadRights();
    }, []);

    async function loadLicenceCategories()
    {
        const {categories, keys} = await new ReadLicenceCategoryAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        setCategories(categories.map((item, index)=>{
            return {
                pubKey:keys[index],
                props:{
                    id:keys[index].toBase58(),
                    key:keys[index].toBase58(),
                    name:keys[index].toBase58(),
                    type:"checkbox",
                    value: false,
                },
                label:item.title,
            };
        }));
    }

    async function loadRights()
    {
        const {rights, keys} = await new ReadRightAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        setRights(rights.map((item, index) =>{
            return {
                pubKey:keys[index],
                props:{
                    id:keys[index].toBase58(),
                    key:keys[index].toBase58(),
                    name:keys[index].toBase58(),
                    type:"checkbox",
                    value: false,
                },
                label:item.title,
            };
        }));
    }

    async function Send()
    {
        const salt = Keypair.generate().publicKey;
        const createResult = await new CreateLicencePresetAccount(
            store.wallet, 
            store.walletKey, 
            new LicencePreset({
                title:title.props.value,
                salt:salt,
                rights:rights.filter((item)=>{
                    return item.props.value;
                })
                .map((item)=>{
                    return item.pubKey;
                }),
                categories:categories.filter((item)=>{
                    return item.props.value;
                })
                .map((item)=>{
                    return item.pubKey;
                }),
            }),
            store.cluster.url).run();
        
        if(createResult)
        {
            const [signatureResult, pubkeyResult] = createResult;
            console.log(`Create Preset succeed! Signature: ${signatureResult[0]} PublicKey: ${pubkeyResult.toBase58()}`);
            setSignature(signatureResult[0]);
            setPubKey(pubkeyResult);
        }
    }

    return(
        <ul className="create-licence-category">
            <li>
                {
                    buildInputElement(title, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setTitle(updateElementProps(title, {value:value}));
                        } 
                    })
                }
            </li>
            <li>
                <h3>Select Categories</h3>
                <ul>
                    {
                        categories.map((item, index)=>{
                            return (
                                buildInputElement(item, {
                                    onValidate:(id:any, isValid:any, value:boolean)=>{
                                        const categoryUpdate = updateElementProps(item, {value:value});
                                        const categoriesUpdate = [...categories];
                                        const index = categoriesUpdate.findIndex((categoryItem)=>{
                                            if(categoryItem.props.id === item.props.id)
                                                return true;
                                            return false;
                                        });
                                        if(index >= 0)
                                        {
                                            categoriesUpdate[index] = categoryUpdate;
                                            setCategories(categoriesUpdate);
                                        }
                                    } 
                                })
                            );
                        })
                    }
                </ul>
            </li>
            <li>
                <h3>Select Rights</h3>
                <ul>
                    {
                        rights.map((item, index)=>{
                            return (
                                buildInputElement(item, {
                                    onValidate:(id:any, isValid:any, value:boolean)=>{
                                        const rightUpdate = updateElementProps(item, {value:value});
                                        const rightsUpdate = [...rights];
                                        const index = rightsUpdate.findIndex((rightItem)=>{
                                            if(rightItem.props.id === item.props.id)
                                                return true;
                                            return false;
                                        });
                                        if(index >= 0)
                                        {
                                            rightsUpdate[index] = rightUpdate;
                                            setRights(rightsUpdate);
                                        }
                                    } 
                                })
                            );
                        })
                    }
                </ul>
            </li>
            {
                (signature !== null) &&
                <li>
                    <ul>
                        <li>Signature: {signature}</li>
                        <li>PublicKey: {pubkey?.toBase58()}</li>
                    </ul>
                </li>
            }
            <li>
                <button onClick={Send}>Save</button>
            </li>
        </ul>
    );
});
export default CreateLicencePreset;