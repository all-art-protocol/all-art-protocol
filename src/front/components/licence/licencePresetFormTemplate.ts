export const title = {
    props:{
        id:"title",
        name:"title",
        type:"text",
        placeholder:"Title",
        value: "",
    },
    label: "Title",
};