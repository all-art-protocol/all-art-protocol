import {FC, ReactNode, useEffect, useState} from 'react';
import {buildInputElement, buildTextboxElement, updateElementProps} from '../form/factory';
import { useAppStore } from '../../store/store';
import { fromByteArray } from '../../../client/utils/string';
import { Keypair, PublicKey } from '@solana/web3.js';
import { CREATE_RIGHT_PROGRAM, CRYPTO_KEY } from '../../../client/core/consts';
import ReadRightAccounts from '../../../client/programs/ReadRightAccounts';
import Right from '../../../client/data/Right';
import { LicenceCategory, LicencePreset, LICENCE_CATEGORIES, LICENCE_PRESETS, RIGHTS } from '../../../client/data/LicencePresets';

interface RightCombo{
    right:Right,
    key:PublicKey,
    base58:string,
    quantity:number,
}

export interface Licence{
    id:string,
    title:string,
    rights:RightCombo[],
    quantity:number,
    lorts:number,
    file:Buffer | null,
}

export default function LicenceCreator(props:{
    rights:RightCombo[], 
    onAdd:(licence:Licence)=>void,
    onClose:()=>void,
})
{
    const [selectedCategories, setSelectedCategories] = useState<LicenceCategory[]>([]);
    const [selectedRights, setSelectedRights] = useState<RightCombo[]>([]);
    const [licenseTitle, setLicenseTitle] = useState({
        props:{
            id:"title",
            name:"title",
            type:"text",
            placeholder:"Licence title",
            value: "",
        },
        label:"Licence title"
    });
    const categories = [...LICENCE_CATEGORIES];
    const renderers:ReactNode[] = [];
    const presets:LicencePreset[] = [...LICENCE_PRESETS];

    categories.forEach((category)=>{
        if(category.parent === '0')
        {
            renderers.push(
                <CategorySelectorItem
                    key={`category-${category.id}`}
                    category={category}
                    className="table-license-top-category table-license-row"
                    selected={isCategorySelected(category)}
                    onSelect={categorySelectHandler}
                    onDeselect={categoryDeselectHandler}
                />
            );
            const subRenderers = createCategories(category);
            if(subRenderers.length > 0)
                renderers.push(...subRenderers);
        }
    });

    function categorySelectHandler(selection:LicenceCategory)
    {
        const categories = [...selectedCategories, selection];
        const relatedCategories = findRelatedCategories(selection);
        for(let i=0; i<relatedCategories.length; i++)
        {
            const alreadyInStack = categories.find((item)=>{
                if(item.id === relatedCategories[i].id)
                    return true;
                return false;
            });
            if(!alreadyInStack)
                categories.push(relatedCategories[i]);
        }
        const rights = [...selectedRights];
        const relatedRights = findRelatedRights(categories);
        for(let i=0; i<relatedRights.length; i++)
        {
            const alreadyInStack = rights.find((item)=>{
                if(relatedRights[i].base58 === item.base58)
                    return true;
                return false;
            });
            if(!alreadyInStack)
                rights.push(relatedRights[i]);
        }
        setSelectedCategories(categories);
        setSelectedRights(rights);
    }
    function categoryDeselectHandler(selection:LicenceCategory)
    {
        const categories:LicenceCategory[] = [];
        const removeCategories:LicenceCategory[] = [];
        const inStackCategories = [...selectedCategories];
        const relatedCategories = [...findRelatedCategories(selection), selection];
        inStackCategories.forEach((item)=>{
            const shouldRemove = relatedCategories.find((remove)=>{
                if(remove.id === item.id)
                    return true;
                return false;
            });
            if(!shouldRemove)
                categories.push(item);
            else
                removeCategories.push(item);
        });
        const rights:RightCombo[] = [];
        const relatedRights = findRelatedRights(removeCategories);
        const inStackRights = [...selectedRights];
        inStackRights.forEach((item)=>{
            const shouldRemove = relatedRights.find((remove)=>{
                if(remove.base58 === item.base58)
                    return true;
                return false;
            });
            if(!shouldRemove)
                rights.push(item);
        });

        setSelectedRights(rights);
        setSelectedCategories(categories);
    }
    function findCategoryById(id:string):LicenceCategory | null
    {
        let result:LicenceCategory | null = null;
        categories.forEach((item)=>{
            if(item.id == id)
            {
                result = item;
                return;
            }
        });
        return result;
    }
    function findRelatedCategories(category:LicenceCategory):LicenceCategory[]
    {
        const result:LicenceCategory[] = [];
        for(let i=0; i<categories.length; i++)
        {
            const c = categories[i];
            if(c.parent === category.id)
            {
                result.push(c);
                const children = findRelatedCategories(c);
                if(children.length)
                    result.push(...children);
            }
        }
        return result;
    }
    function findRightById(id:number):RightCombo | null
    {
        let result:RightCombo | null = null;
        const rights = [...props.rights];
        const templates = [...RIGHTS];
        const template = templates.find((rightTemplate)=>{
            if(rightTemplate.id === id)
                return true;
            return false;
        });
        if(template)
        {
            const right = rights.find((rightItem)=>{
                if(rightItem.base58 === template.key)
                    return true;
                return false;
            });
            result = (right) ? right : null;
        }
        return result;
    }
    function findRelatedRights(categories:LicenceCategory[]):RightCombo[]
    {
        const result:RightCombo[] = [];
        const templates = [...RIGHTS];
        for(let i=0; i<categories.length; i++)
        {
            props.rights.forEach((item)=>{
                const template = templates.find((t)=>{
                    if(t.key === item.base58)
                        return true;
                    return false;
                });
                if(template && template.category === categories[i].id)
                    result.push(item);
            });
        }
        return result;
    }
    function createCategories(category:LicenceCategory):ReactNode[]
    {
        const result:ReactNode[] = [];
        categories.forEach((c)=>{
            if(c.parent === category.id)
            {
                result.push(
                    <CategorySelectorItem
                        key={`category-${c.id}`}
                        category={c}
                        className="table-license-sub-category table-license-row"
                        selected={isCategorySelected(c)}
                        onSelect={categorySelectHandler}
                        onDeselect={categoryDeselectHandler}
                    />
                );
                const subcategories = createCategories(c);
                if(subcategories.length > 0)
                    result.push(...subcategories);
                const rightsRenderers = createRights(c);
                if(rightsRenderers.length > 0)
                    result.push(...rightsRenderers);
            }
        });
        return result;
    }
    function createRights(category:LicenceCategory):ReactNode[]
    {
        const result:ReactNode[] = [];
        const templates = [...RIGHTS];
        props.rights.forEach((right)=>{
            const template = templates.find((item)=>{
                if(item.key === right.base58)
                    return true;
                return false;
            });
            if(template && template.category === category.id)
            {
                result.push(
                    <RightSelectorItem 
                        key={right.base58} 
                        rightCombo={right} 
                        selected={isRightSelected(right)}
                        onSelect={(rightComboItem:RightCombo)=>{
                            const rights = [...selectedRights, rightComboItem];
                            setSelectedRights(rights);
                        }}
                        onDeselect={(rightComboItem:RightCombo)=>{
                            let rights = [...selectedRights];
                            const index = rights.findIndex((item, index)=>{
                                if(item.key.toBase58() === rightComboItem.key.toBase58())
                                    return true;
                                return false;
                            });
                            if(index >= 0)
                                rights.splice(index, 1);
                            setSelectedRights(rights);
                        }}
                    />
                );
            }
        });
        return result;
    }
    function isCategorySelected(category:LicenceCategory){
        const selected = selectedCategories.find((selectedCategory)=>{
            if(selectedCategory.id === category.id)
                return true;
            return false;
        });
        return (selected) ? true : false;
    }
    function isRightSelected(right:RightCombo){
        const selected = selectedRights.find((selectedRight)=>{
            if(selectedRight.base58 === right.base58)
                return true;
            return false;
        });
        return (selected) ? true : false;
    }

    return(
        <div className="rights-selector">
            <div className="rights-selector-dialogue">
                <button className="rights-selector-close" onClick={(e)=>{
                    props.onClose();
                }}><i className="fa fa-times"></i>
                </button>
                <h2>License creator</h2>
                <div className="licence-details">
                    {
                        buildInputElement(licenseTitle, {
                            onValidate:(id:any, isValid:any, value:any)=>{
                                setLicenseTitle(updateElementProps(licenseTitle, {value:value}));
                            } 
                        })
                    }
                </div>
                <div className="licence-presets">
                    {
                        presets.map((preset)=>{
                            return(
                                <button 
                                    key={`preset-${preset.name}`} 
                                    onClick={(e)=>{
                                        const presetCategories:LicenceCategory[] = [];
                                        const presetRights:RightCombo[] = [];
                                        preset.categories.forEach((id)=>{
                                            const category = findCategoryById(id);
                                            if(category)
                                            {
                                                presetCategories.push(category, ...findRelatedCategories(category));
                                                presetRights.push(...findRelatedRights(presetCategories));
                                            }
                                        });

                                        preset.rights.forEach((id)=>{
                                            const right = findRightById(id);
                                            if(right)
                                                presetRights.push(right);
                                        });

                                        setSelectedRights(presetRights);
                                        setSelectedCategories(presetCategories);
                                    }}
                                >
                                    {preset.name}
                                </button>
                            );
                        })
                    }
                </div>
                <div className="rights-selector-list">
                    {renderers}
                </div>
                <div className="rights-selector-footer">
                    <button disabled={!selectedRights.length} className="rights-selector-add" onClick={(e)=>{
                        props.onAdd({
                            id:Keypair.generate().publicKey.toBase58(),
                            title:licenseTitle.props.value,
                            rights:[...selectedRights],
                            quantity:0,
                            lorts:0,
                            file:null,
                        });
                        props.onClose();
                    }}>Create license</button>
                </div>
            </div>
        </div>
    );
}

// CATEGORY ITEM
function CategorySelectorItem(props:{
    category:LicenceCategory,
    className:string,
    selected:boolean,
    onSelect:(category:LicenceCategory)=>void, 
    onDeselect:(category:LicenceCategory)=>void
})
{
    return(
        <div className={props.className} key={`category-key-${props.category.id}`}>
            <input 
                id={`category-${props.category.id}`} 
                type="checkbox" 
                checked={props.selected} 
                onChange={(e)=>{
                    if(e.target.checked)
                        props.onSelect(props.category);
                    else
                        props.onDeselect(props.category);
                }} 
            />
            <label htmlFor={`category-${props.category.id}`}>{props.category.title}</label>
        </div>
    );
}

// RIGHT ITEM
function RightSelectorItem(props:{
    rightCombo:RightCombo,
    selected:boolean,
    onSelect:(right:RightCombo)=>void, 
    onDeselect:(right:RightCombo)=>void}
)
{
    return(
        <div className="table-license-right table-license-row">
            <input id={props.rightCombo.base58} type="checkbox" checked={props.selected} onChange={(e)=>{
                if(e.target.checked)
                    props.onSelect(props.rightCombo);
                else
                    props.onDeselect(props.rightCombo);
            }} />
            <label className="noselect" htmlFor={props.rightCombo.base58}>{props.rightCombo.right.title}</label>
        </div>
    );
}