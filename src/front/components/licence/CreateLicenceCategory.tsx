import {FC, useEffect, useState} from 'react';
import {buildInputElement, updateElementProps} from '../form/factory';
import * as formTemplate from './licenceCategoryFormTemplate';
import Right from '../../../client/data/Right';
import CreateLicenceCategoryAccount from '../../../client/programs/CreateLicenceCategoryAccount';
import { observer } from 'mobx-react-lite';
import { useAppStore } from '../../store/store';
import { Keypair, PublicKey } from '@solana/web3.js';
import './style.css';
import LicenceCategory from '../../../client/data/LicenceCategory';
import ReadLicenceCategoryAccounts from '../../../client/programs/ReadLicenceCategoryAccounts';
import { CREATE_RIGHT_PROGRAM } from '../../../client/core/consts';
import ReadRightAccounts from '../../../client/programs/ReadRightAccounts';
import LicenceCategoryRights from '../../../client/data/LicenceCategoryRights';

const CreateLicenceCategory:FC = observer(()=>
{
    const store = useAppStore();
    const [title, setTitle] = useState({...formTemplate.title});
    const [description, setDescription] = useState({...formTemplate.description});
    const [parentCategory, setParentCategory] = useState({...formTemplate.parent});
    const [order, setOrder] = useState({...formTemplate.order});
    const [rights, setRights] = useState<{
        right:PublicKey,
        props:{
            id:string,
            name:string,
            type:string,
            value: boolean,
        },
        label:string,
    }[]>([]);
    const [signature, setSignature] = useState<string | null>(null);
    const [pubkey, setPubKey] = useState<PublicKey | null>(null);

    useEffect(()=>{
        loadLicenceCategories();
        loadRights();
    }, []);

    console.log(parentCategory);

    async function loadLicenceCategories()
    {
        const {categories, keys} = await new ReadLicenceCategoryAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        const items:{label:string, value:string | undefined}[] = categories.map((item, index) =>{
            return {label: item.title, value:keys[index].toBase58()};
        })
        items.unshift({label:"Select parent category", value:undefined});
        
        setParentCategory({
            ...parentCategory,
            props:{
                ...parentCategory.props,
                children:items
            }
        });
    }

    async function loadRights()
    {
        const {rights, keys} = await new ReadRightAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        const items:{label:string, value:string | undefined}[] = rights.map((item, index) =>{
            return {label: item.title, value:keys[index].toBase58()};
        })
        items.unshift({label:"Select parent category", value:undefined});
        
        setRights(rights.map((item, index) =>{
            return {
                right:keys[index],
                props:{
                    id:keys[index].toBase58(),
                    key:keys[index].toBase58(),
                    name:keys[index].toBase58(),
                    type:"checkbox",
                    value: false,
                },
                label:item.title,
            };
        }));
    }

    async function Send()
    {
        const salt = Keypair.generate().publicKey;
        const createResult = await new CreateLicenceCategoryAccount(
            store.wallet, 
            store.walletKey, 
            new LicenceCategory({
                title:title.props.value,
                description:description.props.value,
                order:order.props.value,
                salt:salt,
                parent:(parentCategory.props.value === "")
                    ? undefined
                    : new PublicKey(parentCategory.props.value)

            }),
            rights.filter((item)=>{
                return item.props.value;
            })
            .map((item)=>{
                return item.right;
            }),
            store.cluster.url).run();
        
        if(createResult)
        {
            const [signatureResult, pubkeyResult] = createResult;
            console.log(`Create right succeed! Signature: ${signatureResult[0]} PublicKey: ${pubkeyResult.toBase58()}`);
            setSignature(signatureResult[0]);
            setPubKey(pubkeyResult);
        }
    }

    return(
        <ul className="create-licence-category">
            <li>
                {
                    buildInputElement(title, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setTitle(updateElementProps(title, {value:value}));
                        } 
                    })
                }
            </li>
            <li>
                {
                    buildInputElement(description, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setDescription(updateElementProps(description, {value:value}));
                        } 
                    })
                }
            </li>
            <li>
                {
                    buildInputElement(parentCategory, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setParentCategory(updateElementProps(parentCategory, {value:value}));
                        } 
                    })
                }
            </li>
            <li>
                {
                    buildInputElement(order, {
                        onValidate:(id:any, isValid:any, value:any)=>{
                            setOrder(updateElementProps(order, {value:value}));
                        } 
                    })
                }
            </li>
            <li>
                <h3>Select Rights</h3>
                <ul>
                    {
                        rights.map((item, index)=>{
                            return (
                                buildInputElement(item, {
                                    onValidate:(id:any, isValid:any, value:boolean)=>{
                                        const rightUpdate = updateElementProps(item, {value:value});
                                        const rightsUpdate = [...rights];
                                        const index = rightsUpdate.findIndex((rightItem)=>{
                                            if(rightItem.props.id === item.props.id)
                                                return true;
                                            return false;
                                        });
                                        if(index >= 0)
                                        {
                                            rightsUpdate[index] = rightUpdate;
                                            setRights(rightsUpdate);
                                        }
                                    } 
                                })
                            );
                        })
                    }
                </ul>
            </li>
            {
                (signature !== null) &&
                <li>
                    <ul>
                        <li>Signature: {signature}</li>
                        <li>PublicKey: {pubkey?.toBase58()}</li>
                    </ul>
                </li>
            }
            <li>
                <button onClick={Send}>Save</button>
            </li>
        </ul>
    );
});
export default CreateLicenceCategory;