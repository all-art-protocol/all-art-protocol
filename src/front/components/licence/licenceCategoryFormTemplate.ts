export const title = {
    props:{
        id:"title",
        name:"title",
        type:"text",
        placeholder:"Title",
        value: "",
    },
    label: "Title",
};
export const description = {
    props:{
        id:"description",
        name:"description",
        type:"textarea",
        placeholder:"Description",
        value: "",
    },
    label: "Description",
};
export const order = {
    props:{
        id:"order",
        name:"order",
        type:"number",
        placeholder:"Order",
        value: 0,
    },
    label: "Order",
};
export const parent = {
    props:{
        id:"parent",
        name:"parent",
        type:"select",
        children:new Array<{label:string, value:string | undefined}>(),
        value:"",
    },
    label: "Parent Category",
};