import { PublicKey } from '@solana/web3.js';
import { useState, useEffect } from 'react';
import { CREATE_RIGHT_PROGRAM } from '../../../client/core/consts';
import { useAppStore } from '../../store/store';
import DeleteAccount from '../../../client/programs/DeleteAccountProgram';
import ReadLicenceCategoryAccounts from '../../../client/programs/ReadLicenceCategoryAccounts';
import LicencePreset from '../../../client/data/LicencePreset';
import { createLicenceCategoryAccountSeed } from '../../../client/core/seed';
import './style.css';
import ReadLicencePresetAccounts from '../../../client/programs/ReadLicencePresetAccounts';

const ReadLicencePresets = () =>
{
    const store = useAppStore();
    const [presets, setPresets] = useState<{preset:LicencePreset, key:PublicKey}[]>([]);
    
    useEffect(()=>{
        loadCategories()
    },[]);
    
    async function loadCategories()
    {
        const {presets, keys} = await new ReadLicencePresetAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        setPresets(presets.map((item, index)=>{
            return {
                preset:item,
                key:keys[index]
            };
        }));
    }

    async function deleteCategory(item:{preset:LicencePreset, key:PublicKey})
    {
        // const rights = await new ReadLicenceCategoryRightsAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url, item.key).run();
        // if(rights)
        // {
        //     console.log(rights);
        //     const seed = await createLicenceCategoryRightsAccountSeed(rights.data.salt.toBase58(), item.key);
        //     await new DeleteAccount(
        //         store.wallet, 
        //         store.walletKey, 
        //         rights.pubkey,
        //         seed,
        //         store.cluster.url)
        //     .run();
        // }
        
        const seed = await createLicenceCategoryAccountSeed(item.preset.salt.toBase58());
        await new DeleteAccount(
            store.wallet, 
            store.walletKey, 
            item.key,
            seed,
            store.cluster.url)
        .run();

        loadCategories();
    }

    return(
        <div className="licence-categories">
            {
                presets.map((item, index)=>{
                    return(
                        <div className="licence-category-item" key={item.key.toBase58()}>
                            <ul className="details">
                                <li>Title: {item.preset.title}</li>
                                <li>PublicKey: {item.key.toBase58()}</li>
                            </ul>
                            <ul className="controlls">
                                <li>
                                    <button onClick={(e)=>{
                                        deleteCategory(item);
                                    }}>
                                        Delete
                                    </button>
                                </li>
                                <li>
                                    <button onClick={(e)=>{
                                        deleteCategory(item);
                                    }}>
                                        Edit
                                    </button>
                                </li>
                            </ul>
                        </div>
                    );
                })
            }
        </div>
    );
};
export default ReadLicencePresets;
