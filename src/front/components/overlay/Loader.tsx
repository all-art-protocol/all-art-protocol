import { FC } from 'react';
import './style.css';

const Loader:FC = () =>
{
    return(
        <div className="overlay-loader">
            <div className="overlay-loader-spinner">
                <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        </div>
    );
};
export default Loader;