import { isUsable } from "../../utils/validation";

/**
 * 
 * @param {object} form 
 * Object filled with input element templates.
 * Template:
 * {
    props:{
        id:"string",
        name:"string",
        type:"string",
        placeholder:"string",
        validStyle:string,
        invalidStyle:string,
        value:string,
        validator: function(value):bool,
    },
    isValid:bool,
    isValidated:bool,
    label:string,
}
 */
export function isValid(form)
{
    const inputElements = Object.values(form);
    for(let i=0; i<inputElements.length; i++)
    {
        if(!inputElements[i].isValid)
            return false;
    }
    return true;
}

/**
 * Runs validation for every input element in the form
 * @param {object} form 
 * Object filled with input element templates.
 * Template:
 * {
    props:{
        id:"string",
        name:"string",
        type:"string",
        placeholder:"string",
        validStyle:string,
        invalidStyle:string,
        value:string,
        validator: function(value):bool,
    },
    isValid:bool,
    isValidated:bool,
    label:string,
}
 */
export function validate(form)
{
    const result = {...form};
    const inputs = Object.values(result);
    const keys = Object.keys(result);
    for(let i=0; i<inputs.length; i++)
    {
        let input = {...inputs[i]};
        input.isValid = (isUsable(input.props.validator)) ? input.props.validator(input.props.value) : true;
        input.isValidated = true;
        result[keys[i]] = input;
        // if(!inputs[i].isValidated)
        // {
        //     let input = {...inputs[i]};
        //     input.isValid = (isUsable(input.props.validator)) ? input.props.validator(input.props.value) : true;
        //     input.isValidated = true;
        //     result[keys[i]] = input;
        // }
    }
    return result;
}