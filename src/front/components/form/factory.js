import React from "react";
import Input from "../input/Input";

/**
 * buildFormElements
 * Returns array of Input Elements
 * @param {object} template 
 * 
 * Element template:
 * {
    props:{
        id:"string",
        name:"string",
        type:"string",
        placeholder:"string",
        validStyle:string,
        invalidStyle:string,
        value:string,
        validator: function(value):bool,
    },
    isValid:bool,
    isValidated:bool,
    label:string,
}
 * @param {function} onChangeCallback 
 */
export function buildFormElements(template, onChangeCallback)
{
    return Object.values(template).map((item)=>{
        return buildInputElement(item, {onValidate:onChangeCallback});
    });
}

/**
 * buildInputElement
 * @param {object} template 
 * 
 * Element template:
 * {
    props:{
        id:"string",
        name:"string",
        type:"string",
        placeholder:"string",
        validStyle:string,
        invalidStyle:string,
        value:string,
        validator: function(value):bool,
    },
    isValid:bool,
    isValidated:bool,
    label:string,
}
 * @param {function(string Boolean any)} onChangeCallback 
 */
export function buildInputElement(template, callbacks = {})
{
    return (
        <label key={template.props.name + "_label"}>
            <Input 
                {...callbacks}
                isValid={template.isValid}
                isValidated={template.isValidated}
                {...template.props}
            />
            <div className="label-text">{template.label}</div>
        </label>
    );
}

export function buildTextboxElement(template, callbacks = {})
{
    return (
        <label key={template.props.name + "_label"}>
            <div className="label-textbox">{template.label}</div>
            <Input 
                {...callbacks}
                isValid={template.isValid}
                isValidated={template.isValidated}
                {...template.props}
            />
        </label>
    );
}

/**
 * @param {object} form
 * * Key Value pairs where Key is element id and Value is element template.
 * 
 * Element template:
 * {
    props:{
        id:"string",
        name:"string",
        type:"string",
        placeholder:"string",
        validStyle:string,
        invalidStyle:string,
        value:string,
        validator: function(value):bool,
    },
    isValid:bool,
    isValidated:bool,
    label:string,
}
 * @param {object} props
 * Key Value pairs
 */
export function updateFormWithProps(form, props)
{
    const result = {...form};
    for(const key in result)
    {
        const element = result[key];
        if(props.hasOwnProperty(element.props.name))
            element.props.value = props[element.props.name];
    }
    return result;
}

export function updateElementProps(element, props)
{
    const el = {...element};
    el.props = {...element.props};
    for(const key in el.props)
    {
        if(props.hasOwnProperty(key))
            el.props[key] = props[key];
    }
    return el;
}

/**
 * 
 * @param {object} form 
 * Key Value pairs where Key is element id and Value is element template.
 * 
 * Element template:
 * {
    props:{
        id:"string",
        name:"string",
        type:"string",
        placeholder:"string",
        validStyle:string,
        invalidStyle:string,
        value:string,
        validator: function(value):bool,
    },
    isValid:bool,
    isValidated:bool,
    label:string,
}
 * 
 * Returns normalized form prepared for submission.
 */
export function prepareForSubmit(form)
{
    const result = {};
    const inputElements = Object.values(form);
    for(const key in form)
        result[form[key].props.name] = form[key].props.value;
    return result;
}