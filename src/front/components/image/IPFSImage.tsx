import { has } from 'mobx';
import {FC, useEffect, useState} from 'react';
import { getFileTypeFromBuffer } from '../../../client/utils/fileType';
import { arrayBufferToBase64, getFile } from '../../../client/utils/ipfs';
import { fromByteArray } from '../../../client/utils/string';
import GLBRenderer from './GLBRenderer';
import './style.css';

const IPFSImage:FC<{
    hash?:string | undefined,
    className?:string,
    alt?:string,
}> = (props)=>
{
    console.log("IPFS Image hash - ", props.hash)
    const [img, setImg] = useState<string | null>(null);
	const [glb, setGlb] = useState<Buffer | null>(null);

    async function load()
    {
        if(props.hash)
		{
			const ipfsFile = await getFile(props.hash);
			const ipfsFileNormalized = ipfsFile.replace('data:application/octet-stream;base64,', "");
			const ipfsFileBuffer = Buffer.from(ipfsFileNormalized, 'base64');
			const fileType = getFileTypeFromBuffer(ipfsFileBuffer);
			if(fileType === 'glTF')
			{
				setImg(null);
				setGlb(ipfsFileBuffer);
			}
			else
			{
				setImg(ipfsFile);
				setGlb(null);
			}
		}
    }

    useEffect(()=>{
        setImg(null);
		setGlb(null);
		load();
    }, [props.hash]);

    const style = (img) ? { backgroundImage:`url(${img})`} : null;
	return(
        <div className={(props.className) ? props.className : "ipfs-image"} style={(style)?style:{}}>
            {
				(glb) && <GLBRenderer data={glb} width={300} height={300} />
			}
        </div>
    );
};
export default IPFSImage;