import { useEffect, useRef } from 'react';
import * as THREE from 'three'
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { toArrayBuffer } from '../../../client/utils/buffer';

export default function GLBRenderer(props:{
    data:Buffer,
    width:number,
    height:number
})
{
    const container = useRef<HTMLDivElement>(null);
    
    useEffect(()=>{
        const gtf = new GLTFLoader();
        const scene = new THREE.Scene();
        const light = new THREE.AmbientLight(0x404040, 10);
        const camera = new THREE.PerspectiveCamera();
        const glbRenderer = new THREE.WebGLRenderer();
        glbRenderer.setSize(props.width, props.height);
        
        const controls = new OrbitControls(camera, glbRenderer.domElement);
        controls.autoRotate = true;
        controls.enablePan = false;
        // controls.enableZoom = false;
        // controls.enableRotate = false;
        controls.rotateSpeed = 2;
        
        if(container && container.current)
            container.current.appendChild(glbRenderer.domElement);
        
        gtf.parse(toArrayBuffer(props.data), "", (gltf)=>
        {
            console.log(gltf);
            scene.add(gltf.scene);
            scene.add(camera);
            scene.add(light);

            const box = getCompoundBoundingBox(gltf.scene);
            if(box)
            {
                const center = box.getCenter(new THREE.Vector3());
                console.log('BOX: ', box);
                console.log('CENTER: ', center);
                camera.position.z = (box.getSize(new THREE.Vector3()).y / 2 / Math.tan(Math.PI * camera.fov / 360)) + .5;
                controls.target.set(0, Math.abs(center.y), 0);
            }

            var animate = function () {
                glbRenderer.render(scene, camera);
                controls.update();
                requestAnimationFrame(animate);
              };
            animate();
        })

        return () => {
            if(container && container.current)
                container.current.removeChild( glbRenderer.domElement);
        };
    }, [props.data]);

    return (
        <div style={{width:`${props.width}px`, height:`${props.height}px`}} ref={container}></div>
    );
}

function getCompoundBoundingBox(scene:THREE.Scene | THREE.Group):THREE.Box3 | null
{
    var box:THREE.Box3 | null = null;
    scene.traverse((obj3D:any) => 
    {
        if(obj3D.type === "Mesh")
        {
            var boundingBox = new THREE.Box3();
            boundingBox.copy(obj3D.geometry.boundingBox);
            obj3D.updateMatrixWorld(true);
            boundingBox.applyMatrix4(obj3D.matrixWorld);
            if (box === null)
                box = boundingBox;
            else 
                box.union(boundingBox);
        }
    });
    return box;
}