import { FC, ReactElement, ReactNode } from 'react';
import { Route, Switch } from 'react-router';
import Header from '../../components/header/Header';
import { AppStore, useAppStore } from '../../store/store';
import './style.css';

const Layout:FC<{children:ReactNode}> = (props)=>
{
    const store:AppStore = useAppStore();
    return(
        <main className="layout">
            <Header />
            <section>
                {props.children}
            </section>
        </main>
    );
};
export default Layout;