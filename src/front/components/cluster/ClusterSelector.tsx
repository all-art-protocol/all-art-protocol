import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { connect } from '../../../client/core/connection';
import { ICluster } from '../../../client/core/consts';
import { ClusterID } from '../../../client/core/enums';
import './style.css';


const ClusterSelector = observer((props:{
    defaultCluster:ICluster,
    clusters:ICluster[],
    onChange?:(clusetr:ICluster)=>void
}) =>
{
    console.log("Cluster selector render", props.defaultCluster.url);
    const [cluster, setCluster] = useState(props.defaultCluster);
    const [customCluster, setCustomCluster] = useState<ICluster>({id:ClusterID.Custom, name:"Custom", url:""});
    const [showSelector, setShowSelector] = useState<boolean>(false);

    useEffect(()=>{
        const isCustomCluster = props.clusters.findIndex((item)=>{
            if(item.url === props.defaultCluster.url)
                return true;
            return false;
        });
        if(isCustomCluster < 0)
            setCustomCluster(props.defaultCluster);
    },[]);
    useEffect(()=>{
        console.log("Update cluster from the store", props.defaultCluster.url);
        setCluster(props.defaultCluster);
    }, [props.defaultCluster]);

    const documentClickHandler = (e:Event)=>{
        if(e && e.target && e.target instanceof Element)
        {
            const element:Element = e.target;
            if(element.closest(".creator-cluster-selector") === null)
            {
                setShowSelector(false);
                document.removeEventListener("mousedown", documentClickHandler);
            }
        }
    }
    return (
        <>
            <div 
                className="creator-cluster-container"
                onClick={(e)=>{
                    setShowSelector(true);
                    document.addEventListener("mousedown", documentClickHandler);
                }}
            >
                <span>{cluster.url}</span>
            </div>
            {
                (showSelector) &&
                <Selector 
                    active={cluster}
                    custom={customCluster}
                    clusters={props.clusters}
                    onSelected={(selectedCluster:ICluster)=>{
                        setCluster(selectedCluster);
                        props.onChange?.(selectedCluster);
                    }}
                    onCustomSelected={(selectedCluster:ICluster)=>{
                        setCluster(selectedCluster);
                        setCustomCluster(selectedCluster);
                        props.onChange?.(selectedCluster);
                    }}
                    onCustomChange={(selectedCluster:ICluster)=>{
                        setCustomCluster(selectedCluster);
                    }}
                    onClose={()=>{
                        setShowSelector(false);
                        document.removeEventListener("mousedown", documentClickHandler);
                    }} 
                />
            }
        </>
    );
});
export default ClusterSelector;

function Selector(props:{
    active:ICluster,
    custom:ICluster,
    clusters:ICluster[],
    onSelected:(cluster:ICluster)=>void,
    onCustomSelected:(cluster:ICluster)=>void,
    onCustomChange:(cluster:ICluster)=>void,
    onClose?:()=>void
})
{
    return(
        <div className="creator-cluster-selector-container">
            <div className="creator-cluster-selector">
                <span 
                    onClick={(e)=>{
                        props.onClose?.();
                    }}
                >x</span>
                <h2>Choose a Cluster</h2>
                <div>
                    {
                        props.clusters.map((cluster)=>{
                            return(
                                <SelectorItem
                                    active={props.active.url === cluster.url ? true : false}
                                    key={cluster.url}
                                    cluster={cluster}
                                    onSelect={(cluster:ICluster)=>{
                                        props.onSelected(cluster);
                                    }}
                                />
                            );
                        })
                    }
                    <CustomSelectorItem
                        active={props.active.url === props.custom.url}
                        cluster={props.custom.url}
                        onSelect={(cluster:ICluster)=>{
                            props.onCustomSelected(cluster);
                        }}
                        onChange={(cluster:ICluster)=>{
                            props.onCustomChange(cluster);
                        }}
                    />
                </div>
            </div>
        </div>
    );
}

function SelectorItem(props:{
    active:boolean,
    cluster:ICluster,
    onSelect:(cluster:ICluster)=>void
})
{
    const classNames = (props.active)
    ? ["creator-cluster-selector-item", "creator-cluster-selector-item-active"]
    : ["creator-cluster-selector-item"];
    return(
        <div 
            className={classNames.join(" ")}
            onClick={(e)=>{
                props.onSelect(props.cluster);
            }}
        >
            <span>{props.cluster.name}:&nbsp;</span>
            <span>{props.cluster.url}</span>
        </div>
    );
}


function CustomSelectorItem(props:{
    active:boolean,
    cluster:string,
    onChange:(cluster:ICluster)=>void,
    onSelect:(cluster:ICluster)=>void
})
{
    const [inFocus, setInFocus] = useState<boolean>(false);
    const [connecting, setConnecting] = useState<boolean>(false);
    const [cluster, setCluster] = useState<string>("");
    
    useEffect(()=>{
        if(props.active)
            setCluster(props.cluster);
    }, [props.active]);

    const classNames = (inFocus || props.active)
    ? ["creator-cluster-selector-item", "creator-cluster-selector-item-active"]
    : ["creator-cluster-selector-item"];
    if(connecting)
        classNames.push("creator-cluster-selector-item-connecting");
    
    const runClusterVerification = (clusterUrl:string) => {
        setConnecting(true);
        props.onChange({id:ClusterID.Custom, name:"Custom", url:clusterUrl});
        verifyCluster(clusterUrl, 
            ()=>{
                props.onSelect({id:ClusterID.Custom, name:"Custom", url:clusterUrl});
                setConnecting(false);
                setCluster(clusterUrl);
            }, 
            ()=>{
                setConnecting(false);
            }
        );
    }

    return(
        <div 
            className={classNames.join(" ")}
            onClick={(e)=>{
                if(props.cluster !== "")
                {
                    runClusterVerification(props.cluster);
                }
            }}
        >
            <span>Custom:&nbsp;</span>
            <input 
                defaultValue={props.cluster}
                type="text" 
                onFocus={()=>{
                    setInFocus(true);
                }}
                onBlur={()=>{
                    setInFocus(false);
                }}
                onChange={(e)=>{
                    runClusterVerification(e.target.value);
                }} 
            />
        </div>
    );
}

const clusterTestQueue:string[] = [];
async function verifyCluster(clusterUrl:string, onValid:()=>void, onInvalid:()=>void):Promise<void>
{
    try{
        clusterTestQueue.push(clusterUrl);
        await connect(clusterUrl);
        clusterTestQueue.splice(0, 1);
        onValid();
    }
    catch(err){
        clusterTestQueue.splice(0, 1);
        onInvalid();
    }
}