import React, { FC, useEffect, useState } from "react";
import Licence from "../../../client/data/Licence";
import PieGraph from "../graph/Pie/PieGraph";
import IPFSImage from "../image/IPFSImage";
import Avatar from "../user/account/Avatar";
import { Payload } from "recharts/types/component/DefaultLegendContent";
import logo from "../../assets/img/artcoins.svg";
import { u64 } from "@solana/spl-token";
import AreaGraph from "../graph/Area/AreaGraph";
import BarGraph from "../graph/Bar/BarGraph";
import { INFTProData } from "../../hooks/useNFTPRO";
import { PublicKey } from "@solana/web3.js";
import { IGraphData, IPieGraphData } from "../graph/IGraphData";
import PropTypes from 'prop-types';

interface IProps{
	data:INFTProData, 
	publicKey:string | null,
	// lortGraph:{data:IPieGraphData[], legend:Payload[]},
	lortGraph:IPieGraphData[],
	licenceGraph:IPieGraphData[],
	tradeVolume:IGraphData[],
	priceOverTime:IGraphData[],
}

export default function ViewNFTPro({data, publicKey, lortGraph, licenceGraph, tradeVolume, priceOverTime}:IProps)
{
	const currency = 1.3;
	const calcLortPrice = ()=>{
		if(data.lort && data.artCoin)
			return ((data.lort?.amount * data.artCoin.amount)/(data.lort.amount-1))-data.artCoin.amount
		return 0;
	}
	const calcLicencePrice = (licence:Licence)=>{
		if(data.lort && data.artCoin)
			return ((data.lort.amount * data.artCoin.amount)/(data.lort.amount-licence.lorts))-data.artCoin.amount
		return 0;
	}

	const pieStyle = {
		dataKey:"value",
		nameKey:"name",
		stroke:"black",
		innerRadius:"35%",
		outerRadius:"60%",
		innerFontSize:"12px",
		innerFontOffset:6,
		innerFontColor:"#fff",
		outerFontSize:"14px",
		outerFontOffset:6,
		outerFontColor:"#fff",
	};
	
	return (
		<section className="container">
			<div className="nft-details">
				<div>
					<IPFSImage hash={data.nftPro?.file_hash} />
					<ul>
						<li className="explorer-nftpro-title">{data.nftPro.title}</li>
						<li className="explorer-nftpro-key">{publicKey}</li>
						<li className="explorer-nftpro-description">{data.nftPro?.description}</li>
						<li className="author-details-content">
							<Avatar user={data.artist}/>
							<div className="author-details">
								<div className="author-handle">{data.artist?.first_name}</div>
								<div className="author-hash">{data.nftPro?.artist.toBase58()}</div>
							</div>
						</li>
					</ul>
				</div>
				<div className="explorer-lorts-details">
					<div className="currency-box">
						<span>Available LORTs</span>
						<span>{data.lort?.amount}</span>
					</div>
					<div className="currency-box">
						<span>LORT Price</span>
						<span>
							<img src={logo} />
							{calcLortPrice().toFixed(2)}
						</span>
						<span>
							~ {(calcLortPrice() * currency).toFixed(2)}$
						</span>
					</div>
					{
						(data 
							&& data.largestLortAccounts 
							&& data.largestLortAccounts.value 
							&& data.largestLortAccounts.value.length > 0) &&
						<>
							<PieGraph 
								className="explorer-pie-chart" 
								style={pieStyle}
								data={lortGraph} 
							/>
							<PieGraph 
								className="explorer-pie-chart" 
								data={licenceGraph} 
								style={pieStyle}
							/>
						</>
					}
				</div>
			</div>
			<div>
				<h3>Licences</h3>
				<table  className="styled-table">
					<thead>
						<tr>
							<th className="table-license-title text-align-left">License Title</th>
							<th>Available</th>
							<th>Sold</th>
							<th>Price in LORTs</th>
							<th>Price in ACC</th>
							<th>Owners</th>
						</tr>
					</thead>
					<tbody>
						{
							data.licences.map((licenceItem, index)=>{
								return(
									<LicenceItem 
										key={licenceItem.licenceKey.toBase58()}
										licence = {licenceItem.licence}
										licenceMint = {licenceItem.mint}
										accPrice = {calcLicencePrice(licenceItem.licence)}
									/>
								);
							})
						}
					</tbody>
				</table>
			</div>
			<div className="explorer-graph-container">
				<div>
					<h3>Trade volume</h3>
					<AreaGraph data={tradeVolume} />
				</div>
				<div>
					<h3>Price over time</h3>
					<BarGraph data={priceOverTime} />
				</div>
			</div>
		</section>
	);
}

// Licence Item
const LicenceItem:FC<{
	licence:Licence,
	licenceMint:any,
	accPrice:number
}> = (props)=>{
	return(
		<tr>
			<td className="text-align-left">{props.licence.title}</td>
			<td>{props.licence.quantity - u64.fromBuffer(props.licenceMint.supply).toNumber()}</td>
			<td>{u64.fromBuffer(props.licenceMint.supply).toNumber()}</td>
			<td>{props.licence.lorts.toFixed()}</td>
			<td className="acc-price-icon"><img src={logo}/>{props.accPrice.toFixed(2)}</td>
			<td>
				<a className="explorer-mint-button" href={`https://explorer.solana.com/address/${props.licence.licence_mint.toBase58()}/largest`}>
					{/* {props.licence.licence_mint.toBase58()} */}
					SHOW
				</a>
			</td>
		</tr>
	);
}
LicenceItem.propTypes = {
	licence:PropTypes.instanceOf(Licence).isRequired,
	licenceMint:PropTypes.any.isRequired,
	accPrice:PropTypes.number.isRequired,
};