import React, {FC, ReactNode, useEffect, useState} from 'react';
// import IPFS from 'ipfs-core';
import { postFile, getFile } from '../../../client/utils/ipfs';
import {buildInputElement, buildTextboxElement, updateElementProps} from '../form/factory';
import * as formTemplate from './formTemplate';
import { useAppStore } from '../../store/store';
import { fromByteArray } from '../../../client/utils/string';
import { Keypair, PublicKey } from '@solana/web3.js';
import ImageSelector from '../upload/ImageSelector';
import { CREATE_RIGHT_PROGRAM, CRYPTO_KEY } from '../../../client/core/consts';
import ReadRightAccounts from '../../../client/programs/ReadRightAccounts';
import Right from '../../../client/data/Right';
import CreateNFTPRO from '../../../client/programs/CreateNFTPRO';
import { LicenceCategory, LicencePreset, LICENCE_CATEGORIES, LICENCE_PRESETS, RIGHTS } from '../../../client/data/LicencePresets';
import Avatar from '../user/account/Avatar';
import { useHistory } from 'react-router';
import { runInAction } from 'mobx';
import Loader from '../overlay/Loader';

import artcoin from "../../../front/assets/img/artcoins.svg"
import './style.css';
import { encrypt } from '../../../client/utils/crypto';

interface RightCombo{
    right:Right,
    key:PublicKey,
    base58:string,
    quantity:number,
}
interface Licence{
    id:string,
    title:string,
    rights:RightCombo[],
    quantity:number,
    lorts:number,
    file:Buffer | null,
}

const CreateNFTPro:FC = ()=>
{
    const store = useAppStore();
    const history = useHistory();
    const [title, setTitle] = useState({...formTemplate.title});
    const [description, setDescription] = useState({...formTemplate.description});
    const [previewBuffer, setPreviewBuffer] = useState<Buffer>();
    const [rights, setRights] = useState<RightCombo[]>([]);
    const [licences, setLicences] = useState<Licence[]>([]);
    const [finalPrice, setFinalPrice] = useState<number>(0);
    const [showLoader, setShowLoader] = useState<boolean>(false);
    const [showLicenceCreator, setShowLicenceCreator] = useState<boolean>(false);

    const totalLorts = 150;
    const totalPoolLorts = 100;
    const currency:number = 1.3;

    const calcLortPrice = ():number=>{
        if(finalPrice === 0 || isNaN(finalPrice))
            return 0;
        return (finalPrice / totalPoolLorts);
    }
    const [signature, setSignature] = useState<string | null>(null);
    const [pubkey, setPubKey] = useState<PublicKey | null>(null);

    useEffect(()=>{
        ReadAllRights();
    }, []);

    async function ReadAllRights()
    {
        const rightAccounts = await new ReadRightAccounts(new PublicKey(CREATE_RIGHT_PROGRAM), store.cluster.url).run();
        if(rightAccounts) setRights(rightAccounts.rights.map((item, index)=>{
            return {
                right:item,
                key:rightAccounts.keys[index],
                base58:rightAccounts.keys[index].toBase58(),
                quantity:0
            };
        }));
    }

    async function Send()
    {
        if(previewBuffer)
        {
            const cid = await postFile(previewBuffer);
            const licenceFiles:string[] = [];
            for(let i=0; i<licences.length; i++)
            {
                const licenceFile = licences[i].file;
                if(licenceFile !== null)
                {
                    licenceFiles.push(
                        (await postFile(Buffer.from(licenceFile))).toString()
                    );
                }
                else
                {
                    alert(`Licence ${licences[i].title} doesn't have any file!`);
                    return;
                }
            }
            
            if(store.userKey)
            {
                setShowLoader(true);
                
				try{
					const createNFTPROResult = await new CreateNFTPRO(store.wallet, store.walletKey, {
						title:title.props.value, 
						description:description.props.value,
						licences:licences.map((item, index)=>{
							return {
								title:item.title,
								rights:item.rights.map(item=>item.key),
								quantity:item.quantity,
								lorts:item.lorts,
								file_hash:licenceFiles[index]
							}
						}),
						artist:store.userKey,
						preview:cid,
						price:finalPrice
					}, store.cluster.url).run();
					
					if(createNFTPROResult)
					{
						console.log("NFTPRO created successfully!");
						runInAction(()=>{
							store.nft = createNFTPROResult[2];
							history.replace('/create/congrats');
						});
					}
					else
					{
						setShowLoader(false);
						alert("ERROR: NFT creation failed!");
					}
				}
				catch(err){
					setShowLoader(false);
					alert(`ERROR: NFT creation failed! ${err.message}`);
					console.log(err);
				}
                
            }
            else
            {
                alert(`There is no artist account! Please create profile first!`);
            }
        }
        else
        {
            alert("NFT preview not selected");
        }
    }

    return(
        <section className="create-artwork">
            {
                (showLoader)&& <Loader/>
            }
            <div className="create-artwork details">
                <div className="create-artwork-details-data">
                    <ImageSelector 
                        className="create-artwork-image-upload" 
                        onChange={(buffer:Buffer)=>{
                            setPreviewBuffer(buffer);
                        }} 
                    />
                    <ul className="create-artwork form-details">
                        <li className="title">{buildInputElement(title, {
                            onValidate:(id:any, isValid:any, value:any)=>{
                                setTitle(updateElementProps(title, {value:value}));
                            } 
                        })}</li>
                        <li className="description">{buildTextboxElement(description, {
                            onValidate:(id:any, isValid:any, value:any)=>{
                                setDescription(updateElementProps(description, {value:value}));
                            } 
                        })}</li>
                        <li>
                            <label className="label-textbox">
                                <div className="label-textbox">
                                Author
                                </div>
                            </label>
                            <div className="author">
                                <Avatar user={store.userData} />
                                <div className="author-details">
                                    <div className="author-handle">{store.userData?.first_name}</div>
                                    <div className="author-hash">{store.userKey?.toBase58()}</div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                
                
                
                <div className="priceDetails">
                    <h2>NFTPRO PRICE</h2>
                    <div className="price-calculation">
                        
                        <ul>
                            <li className="price-label">
                                Enter price
                            </li>
                            <li className="price-container">
                                <div className="price-artcoins-container">
                                    <img className="artcoins-logo" src={artcoin} />
                                    <input 
                                        type="number"
                                        className="create-artwork-final-price" 
                                        defaultValue="0" 
                                        onChange={(e)=>{
                                            if(e.target.value !== "")
                                                setFinalPrice(parseInt(e.target.value));
                                        }}
                                    />
                                </div>
                                <div className="price-dollars">
                                ~ {new Intl.NumberFormat("de-DE", {style: 'currency', currency: 'USD'}).format(finalPrice*currency)}
                                </div>

                            </li>
                        </ul>
                        
                    </div>
                    <div className="mint-button">
                        <button onClick={Send}>Mint</button>
                    </div>
                </div>
                

                <div className="lorts-details">
                    
                    <ul>
                        <li className="price-label">
                            LORTs
                        </li>
                        <li className="price-container">
                            <div className="lort-artcoins-container">
                                <div className="price-artcoins">
                                    {totalLorts}
                                </div>
                            </div>
                            <div className="lorts-bottom">
                                <div>
                                    POOL 100
                                </div>
                                <div>
                                    CREATOR 50
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul>
                        <li className="price-label">
                            LORT price
                        </li>
                        <li className="price-container">
                            <div className="lort-artcoins-container">
                                <img className="artcoins-logo" src={artcoin} />
                                <div className="price-artcoins">
                                    {calcLortPrice().toFixed(2)}
                                </div>
                            </div>
                            <div className="price-dollars">
                            ~ {new Intl.NumberFormat("de-DE", {style: 'currency', currency: 'USD'}).format(calcLortPrice()*currency)}
                            </div>
                        </li>
                    </ul>
                </div>



            </div>
            <div className="create-artwork rights-controls">
                <button onClick={(e)=>{
                    setShowLicenceCreator(true);
                }}>+ ADD LICENCE</button>
            </div>
            <div className="create-artwork-selected-rights">
                {
                    (licences.length > 0)&&
                    <table  className="styled-table">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th className="table-license-title">License Title</th>
                                <th>Number</th>
                                <th>Price</th>
                                <th>LORTs</th>
                                <th className="table-license-file">File</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                licences.map((item)=>{
                                    return(
                                        <LicenceItem
                                            key={item.id}
                                            licence={item}
                                            totalPrice={finalPrice}
                                            totalLorts={totalPoolLorts}
                                            onDelete={(licence:Licence)=>{
                                                licences.findIndex((item, index)=>{
                                                    if(item.id === licence.id)
                                                    {
                                                        const items = [...licences];
                                                        items.splice(index, 1);
                                                        setLicences(items);
                                                        return true;
                                                    }
                                                    return false;
                                                });
                                            }}
                                            onChange={(licence:Licence)=>{
                                                licences.findIndex((item, index)=>{
                                                    if(item.id === licence.id)
                                                    {
                                                        const items = [...licences];
                                                        // items.splice(index, 1);
                                                        // items.push(licence);
                                                        items[index] = licence;
                                                        console.log(items);
                                                        setLicences(items);
                                                        return true;
                                                    }
                                                    return false;
                                                });
                                            }}
                                        />
                                    );
                                })
                            }
                        </tbody>
                    </table>
                }
            </div>
            {
                (showLicenceCreator) &&
                <LicenceCreator 
                    rights={rights} 
                    onAdd={(licence:Licence)=>{
                        setLicences([...licences, licence]);
                    }}
                    onClose={()=>{
                        setShowLicenceCreator(false);
                    }}
                />
            }
        </section>
    );
};
export default CreateNFTPro;




// LICENCE ITEM
function LicenceItem(props:{
    licence:Licence,
    totalPrice:number,
    totalLorts:number,
    onDelete:(licence:Licence)=>void,
    onChange:(licence:Licence)=>void,
})
{
    const defaultFileUploadLabel = "Upload File";
    const [fileLabel, setFileLabel] = useState<string>(defaultFileUploadLabel);
    const calcPrice = ()=>{
        if(props.licence.quantity === 0 || isNaN(props.licence.quantity) || props.totalPrice === 0 || isNaN(props.totalPrice))
            return 0;

        const multi = props.totalPrice * props.totalLorts
        return multi / (props.totalLorts - props.totalLorts / props.licence.quantity) - props.totalPrice
    }
    const calcLorts = (licenceQuantity:number)=>{        
        if(props.totalLorts === 0 || isNaN(props.totalLorts) || licenceQuantity === 0 || isNaN(licenceQuantity))
            return 0;
        return (props.totalLorts / (licenceQuantity));
    }
    const currency:number = 1.3;

    return(
        <tr>
            <td>
                <button className="close" onClick={(e)=>{
                    props.onDelete(props.licence);
                }}>REMOVE</button>
            </td>
            <td><div className="license-title">{props.licence.title}</div></td>
            <td>
                <input 
                    className="input-on-license input-quantity"
                    name="quantity" 
                    type="number" 
                    defaultValue={props.licence.quantity}
                    onChange={(e)=>{
                        if(e.target.value !== "")
                        {
                            props.onChange({
                                ...props.licence,
                                quantity:parseInt(e.target.value),
                                lorts:calcLorts(parseInt(e.target.value)),
                            });
                        }
                    }}
                />
            </td>
            <td>
                <span>{new Intl.NumberFormat("de-DE", {style: 'currency', currency: 'USD'}).format(calcPrice()*currency)}</span>
            </td>
            <td>
                <span>{props.licence.lorts.toFixed(2)}</span>
            </td>
            <td>
                <div className="file-upload-button">
                    <label htmlFor={`licence-${props.licence.id}-file-upload`} className="custom-file-upload">
                        <i className="fa fa-cloud-upload"></i>{fileLabel}
                    </label>
                    <input 
                        style={{display:"none"}} 
                        type="file" 
                        id={`licence-${props.licence.id}-file-upload`}
                        multiple={false} 
                        onChange={(e)=>{
                            e.preventDefault()
                            if(e.target.files)
                            {
                                const file = e.target.files[0];
                                setFileLabel(`${file.name.slice(0, defaultFileUploadLabel.length-3)}...`);
                                const reader = new FileReader();
                                reader.readAsDataURL(file);
                                reader.onloadend = () => 
                                {
                                    if(reader.result)
                                    {
                                        props.onChange({
                                            ...props.licence,
                                            file:Buffer.from(encrypt(reader.result.toString(), CRYPTO_KEY)),
                                        });
                                    }
                                }
                            }
                        }} 
                    />
                </div>
            </td>
        </tr>
    );
}




// LICENSE CREATOR
function LicenceCreator(props:{
    rights:RightCombo[], 
    onAdd:(licence:Licence)=>void,
    onClose:()=>void,
})
{
    const [selectedCategories, setSelectedCategories] = useState<LicenceCategory[]>([]);
    const [selectedRights, setSelectedRights] = useState<RightCombo[]>([]);
    const [licenseTitle, setLicenseTitle] = useState({...formTemplate.licenseTitle});
    const categories = [...LICENCE_CATEGORIES];
    const renderers:ReactNode[] = [];
    const presets:LicencePreset[] = [...LICENCE_PRESETS];

    categories.forEach((category)=>{
        if(category.parent === '0')
        {
            renderers.push(
                <CategorySelectorItem
                    key={`category-${category.id}`}
                    category={category}
                    className="table-license-top-category table-license-row"
                    selected={isCategorySelected(category)}
                    onSelect={categorySelectHandler}
                    onDeselect={categoryDeselectHandler}
                />
            );
            const subRenderers = createCategories(category);
            if(subRenderers.length > 0)
                renderers.push(...subRenderers);
        }
    });

    function categorySelectHandler(selection:LicenceCategory)
    {
        const categories = [...selectedCategories, selection];
        const relatedCategories = findRelatedCategories(selection);
        for(let i=0; i<relatedCategories.length; i++)
        {
            const alreadyInStack = categories.find((item)=>{
                if(item.id === relatedCategories[i].id)
                    return true;
                return false;
            });
            if(!alreadyInStack)
                categories.push(relatedCategories[i]);
        }
        const rights = [...selectedRights];
        const relatedRights = findRelatedRights(categories);
        for(let i=0; i<relatedRights.length; i++)
        {
            const alreadyInStack = rights.find((item)=>{
                if(relatedRights[i].base58 === item.base58)
                    return true;
                return false;
            });
            if(!alreadyInStack)
                rights.push(relatedRights[i]);
        }
        setSelectedCategories(categories);
        setSelectedRights(rights);
    }
    function categoryDeselectHandler(selection:LicenceCategory)
    {
        const categories:LicenceCategory[] = [];
        const removeCategories:LicenceCategory[] = [];
        const inStackCategories = [...selectedCategories];
        const relatedCategories = [...findRelatedCategories(selection), selection];
        inStackCategories.forEach((item)=>{
            const shouldRemove = relatedCategories.find((remove)=>{
                if(remove.id === item.id)
                    return true;
                return false;
            });
            if(!shouldRemove)
                categories.push(item);
            else
                removeCategories.push(item);
        });
        const rights:RightCombo[] = [];
        const relatedRights = findRelatedRights(removeCategories);
        const inStackRights = [...selectedRights];
        inStackRights.forEach((item)=>{
            const shouldRemove = relatedRights.find((remove)=>{
                if(remove.base58 === item.base58)
                    return true;
                return false;
            });
            if(!shouldRemove)
                rights.push(item);
        });

        setSelectedRights(rights);
        setSelectedCategories(categories);
    }
    function findCategoryById(id:string):LicenceCategory | null
    {
        let result:LicenceCategory | null = null;
        categories.forEach((item)=>{
            if(item.id == id)
            {
                result = item;
                return;
            }
        });
        return result;
    }
    function findRelatedCategories(category:LicenceCategory):LicenceCategory[]
    {
        const result:LicenceCategory[] = [];
        for(let i=0; i<categories.length; i++)
        {
            const c = categories[i];
            if(c.parent === category.id)
            {
                result.push(c);
                const children = findRelatedCategories(c);
                if(children.length)
                    result.push(...children);
            }
        }
        return result;
    }
    function findRightById(id:number):RightCombo | null
    {
        let result:RightCombo | null = null;
        const rights = [...props.rights];
        const templates = [...RIGHTS];
        const template = templates.find((rightTemplate)=>{
            if(rightTemplate.id === id)
                return true;
            return false;
        });
        if(template)
        {
            const right = rights.find((rightItem)=>{
                if(rightItem.base58 === template.key)
                    return true;
                return false;
            });
            result = (right) ? right : null;
        }
        return result;
    }
    function findRelatedRights(categories:LicenceCategory[]):RightCombo[]
    {
        const result:RightCombo[] = [];
        const templates = [...RIGHTS];
        for(let i=0; i<categories.length; i++)
        {
            props.rights.forEach((item)=>{
                const template = templates.find((t)=>{
                    if(t.key === item.base58)
                        return true;
                    return false;
                });
                if(template && template.category === categories[i].id)
                    result.push(item);
            });
        }
        return result;
    }
    function createCategories(category:LicenceCategory):ReactNode[]
    {
        const result:ReactNode[] = [];
        categories.forEach((c)=>{
            if(c.parent === category.id)
            {
                result.push(
                    <CategorySelectorItem
                        key={`category-${c.id}`}
                        category={c}
                        className="table-license-sub-category table-license-row"
                        selected={isCategorySelected(c)}
                        onSelect={categorySelectHandler}
                        onDeselect={categoryDeselectHandler}
                    />
                );
                const subcategories = createCategories(c);
                if(subcategories.length > 0)
                    result.push(...subcategories);
                const rightsRenderers = createRights(c);
                if(rightsRenderers.length > 0)
                    result.push(...rightsRenderers);
            }
        });
        return result;
    }
    function createRights(category:LicenceCategory):ReactNode[]
    {
        const result:ReactNode[] = [];
        const templates = [...RIGHTS];
        props.rights.forEach((right)=>{
            const template = templates.find((item)=>{
                if(item.key === right.base58)
                    return true;
                return false;
            });
            if(template && template.category === category.id)
            {
                result.push(
                    <RightSelectorItem 
                        key={right.base58} 
                        rightCombo={right} 
                        selected={isRightSelected(right)}
                        onSelect={(rightComboItem:RightCombo)=>{
                            const rights = [...selectedRights, rightComboItem];
                            setSelectedRights(rights);
                        }}
                        onDeselect={(rightComboItem:RightCombo)=>{
                            let rights = [...selectedRights];
                            const index = rights.findIndex((item, index)=>{
                                if(item.key.toBase58() === rightComboItem.key.toBase58())
                                    return true;
                                return false;
                            });
                            if(index >= 0)
                                rights.splice(index, 1);
                            setSelectedRights(rights);
                        }}
                    />
                );
            }
        });
        return result;
    }
    function isCategorySelected(category:LicenceCategory){
        const selected = selectedCategories.find((selectedCategory)=>{
            if(selectedCategory.id === category.id)
                return true;
            return false;
        });
        return (selected) ? true : false;
    }
    function isRightSelected(right:RightCombo){
        const selected = selectedRights.find((selectedRight)=>{
            if(selectedRight.base58 === right.base58)
                return true;
            return false;
        });
        return (selected) ? true : false;
    }

    return(
        <div className="rights-selector">
            <div className="rights-selector-dialogue">
                <button className="rights-selector-close" onClick={(e)=>{
                    props.onClose();
                }}><i className="fa fa-times"></i>
                </button>
                <h2>License creator</h2>
                <div className="licence-details">
                    {
                        buildInputElement(licenseTitle, {
                            onValidate:(id:any, isValid:any, value:any)=>{
                                setLicenseTitle(updateElementProps(licenseTitle, {value:value}));
                            } 
                        })
                    }
                </div>
                <div className="licence-presets">
                    {
                        presets.map((preset)=>{
                            return(
                                <button 
                                    key={`preset-${preset.name}`} 
                                    onClick={(e)=>{
                                        const presetCategories:LicenceCategory[] = [];
                                        const presetRights:RightCombo[] = [];
                                        preset.categories.forEach((id)=>{
                                            const category = findCategoryById(id);
                                            if(category)
                                            {
                                                presetCategories.push(category, ...findRelatedCategories(category));
                                                presetRights.push(...findRelatedRights(presetCategories));
                                            }
                                        });

                                        preset.rights.forEach((id)=>{
                                            const right = findRightById(id);
                                            if(right)
                                                presetRights.push(right);
                                        });

                                        setSelectedRights(presetRights);
                                        setSelectedCategories(presetCategories);
                                    }}
                                >
                                    {preset.name}
                                </button>
                            );
                        })
                    }
                </div>
                <div className="rights-selector-list">
                    {renderers}
                </div>
                <div className="rights-selector-footer">
                    <button disabled={!selectedRights.length} className="rights-selector-add" onClick={(e)=>{
                        props.onAdd({
                            id:Keypair.generate().publicKey.toBase58(),
                            title:licenseTitle.props.value,
                            rights:[...selectedRights],
                            quantity:0,
                            lorts:0,
                            file:null,
                        });
                        props.onClose();
                    }}>Create license</button>
                </div>
            </div>
        </div>
    );
}

// CATEGORY ITEM
function CategorySelectorItem(props:{
    category:LicenceCategory,
    className:string,
    selected:boolean,
    onSelect:(category:LicenceCategory)=>void, 
    onDeselect:(category:LicenceCategory)=>void
})
{
    return(
        <div className={props.className} key={`category-key-${props.category.id}`}>
            <input 
                id={`category-${props.category.id}`} 
                type="checkbox" 
                checked={props.selected} 
                onChange={(e)=>{
                    if(e.target.checked)
                        props.onSelect(props.category);
                    else
                        props.onDeselect(props.category);
                }} 
            />
            <label htmlFor={`category-${props.category.id}`}>{props.category.title}</label>
        </div>
    );
}

// RIGHT ITEM
function RightSelectorItem(props:{
    rightCombo:RightCombo,
    selected:boolean,
    onSelect:(right:RightCombo)=>void, 
    onDeselect:(right:RightCombo)=>void}
)
{
    return(
        <div className="table-license-right table-license-row">
            <input id={props.rightCombo.base58} type="checkbox" checked={props.selected} onChange={(e)=>{
                if(e.target.checked)
                    props.onSelect(props.rightCombo);
                else
                    props.onDeselect(props.rightCombo);
            }} />
            <label className="noselect" htmlFor={props.rightCombo.base58}>{props.rightCombo.right.title}</label>
        </div>
    );
}