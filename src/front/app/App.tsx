import { observer } from 'mobx-react-lite';
import { Route, Switch } from 'react-router';
import { runInAction } from 'mobx';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { AppStore, AppStoreProvider, useAppStore } from '../store/store';
import ConnectWallet from '../pages/connect/Connect';
import Creator from '../pages/creator/Creator';
import Explorer from '../pages/explore/Explorer';
import Layout from '../components/layout/Layout';
import Create from '../pages/create/Create';
import Read from '../pages/read/Read';
import { getIPFS } from '../../client/utils/ipfs';
import CreatorHeader from '../components/header/CreatorHeader';
import { PublicKey } from '@solana/web3.js';
import LicenceCategory from '../../client/data/LicenceCategory';

const App:FC = observer(()=>
{
    console.log(process.env['REACT_APP_BUILD']);
    const store:AppStore = useAppStore();
    const [ipfsReady, setIpfsReady] = useState<boolean>(false);

    useEffect(()=>{
        initIPFS();
        const walletConnectHandler = (publicKey:PublicKey)=>{
            console.log('Wallet Connected');
            runInAction(()=>{
                store.walletKey = publicKey;
                store.connected = true;
            });
        };
        const walletDisconnectHandler = ()=>{
            console.log('Wallet Disconnected');
            runInAction(()=>store.connected = false);
        };
        store.wallet.on('connect', walletConnectHandler);
        store.wallet.on('disconnect', walletDisconnectHandler);
        return ()=>{
            store.wallet.off('connect', walletConnectHandler);
            store.wallet.off('disconnect', walletDisconnectHandler);
        }
    },[store.wallet])

    async function initIPFS()
    {
        const ipfs = await getIPFS();
        setIpfsReady(true);
    }

    let view: ReactNode | null = null;
    switch(process.env['REACT_APP_BUILD'])
    {
        case "creator":
            console.log("RUN CREATOR");
            view = <Creator />;
            break;
        case "explorer":
            console.log("RUN EXPLORER");
            view = <Explorer />;
            break;
        case "develop":
            console.log("RUN DEVELOPER");
            view =  <Layout>
                        <Switch>
                            <Route path="/create">
                                <Create />
                            </Route>
                            <Route path="/read">
                                <Read />
                            </Route>
                            <Route path="/explorer">
                                <Explorer />
                            </Route>
                        </Switch>
                    </Layout>;
            break;
    }
    
    if(ipfsReady)
    {
        if(process.env['REACT_APP_BUILD'] === 'explorer')
        {
            return(
                <AppStoreProvider store={store}>
                    {view}
                </AppStoreProvider>
            )
        }
        else
        {
            return(
                <AppStoreProvider store={store}>
                    {
                        (!store.connected)
                        ?   <>
                                <CreatorHeader />
                                <ConnectWallet />
                            </>
                        :   view
                    }
                </AppStoreProvider>
            );
        }
    }
    else return null;
});
export default App;