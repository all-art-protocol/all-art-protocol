import { FC, useEffect, useState } from "react"
import { INFTProData, IToken, useNFTPRO } from "../../hooks/useNFTPRO"
import ExplorerHeader from "../../components/header/ExplorerHeader";
import { PublicKey } from "@solana/web3.js";
import { useStateWithCallback } from "../../hooks/useStateWithCallback";
import logo from "../../../front/assets/img/artcoins.svg"
import { u64 } from "@solana/spl-token"
import { useHistory } from "react-router";
import BarGraph from "../../components/graph/Bar/BarGraph";
import AreaGraph from "../../components/graph/Area/AreaGraph";
import { useAppStore } from "../../store/store";
import { IGraphData, IPieGraphData } from "../../components/graph/IGraphData";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { clusters, defaultCluster } from "../../../client/core/consts";
import { validate } from "../../../client/core/validation";
import { randomColor, randomShade } from "../../../client/utils/color";
import { Payload } from "recharts/types/component/DefaultLegendContent";
import ViewNFTPro from "../../components/artwork/ViewNFTPro";
import "./style.css";

const Explorer: FC = observer(() => {
	const history = useHistory()
	const store = useAppStore();

	const nftKeyValidator = (param:string | null):boolean =>{
		if(param === null || param.length !== 44)
			return false;
		return true;
	}
	const clusterIdValidator = (param:string | null):boolean => {
		if(param === null || param.length === 0)
			return false;
		const cluster = clusters.find((item)=>{
			if(item.id === param)
				return true;
			return false;
		});
		if(cluster === undefined || cluster === null)
			return false;
		return true;
	}
	
	const search = new URLSearchParams(history.location.search)
	const nftUrlParam = search.get('pk');
	const clusterIDUrlParam = search.get('cluster');
	const isClusterIDUrlParamValid = validate<string | null>(
		clusterIdValidator, 
		clusterIDUrlParam, 
		"Invalid Cluster!"
	);
	const isNftUrlParamValid = validate<string | null>(
		nftKeyValidator, 
		nftUrlParam, 
		"Invalid NFT key!"
	);
	
	const [nftKey, setNftKey] = useState(
		isNftUrlParamValid.isValid 
			? isNftUrlParamValid.data 
			: ""
		);
	const {nftData, nftError} = useNFTPRO({ publicKey: nftKey, cluster: store.cluster.url });

	const [init, setInit] = useStateWithCallback(false, ()=>{
		console.log(`Update Init state ${isNftUrlParamValid.isValid}`);
		if(isNftUrlParamValid.isValid)
		{
			if(isNftUrlParamValid.data !== nftKey)
				setNftKey(isNftUrlParamValid.data);
		}
	});
	useEffect(() => {
		if(isNftUrlParamValid.isValid && !init)
		{
			if(!isClusterIDUrlParamValid.isValid) 
			{
				runInAction(()=>{
					store.cluster = defaultCluster;
				});
			}
			else
			{
				if(clusterIDUrlParam !== store.cluster.id)
				{
					const clusterFromUrl = clusters.find((item)=>{
						if(item.id === clusterIDUrlParam)
							return true;
						return false;
					});
					if(clusterFromUrl)
					{
						runInAction(()=>{
							store.cluster = clusterFromUrl;
						});
					}
				}
				else
					setInit(true);
			}
		}
		else
			setInit(true);
	}, [store.cluster]);

	const [lortGraphData, setLortGraphData] = useState<IPieGraphData[]>([]);
	const [licenceGraphData, setLicenceGraphData] = useState<IPieGraphData[]>([]);
	useEffect(()=>{
		if(nftData)
		{
			setLortGraphData(lortDataProvider(nftData).data);
			setLicenceGraphData(licenceDataProvider());
		}
	}, [nftData]);
	const dataProvider = ():IGraphData[] => {
		const result:IGraphData[] = []
		const v = 0
		for (let i = 45; i >= 0; i--) 
		{
			const date = new Date()
			date.setDate(date.getDate() - i)
			result.push({
				// name: 45 - i + 1,
				name: date.getDate(),
				date: date.toDateString(),
				value: Math.abs(i - 45) * 10 + Math.round(Math.random() * 100),
				valuePrefix: "$",
			})
		}
		return result
	}
	const lortDataProvider = (nftData:INFTProData):{data:IPieGraphData[], legend:Payload[]} => {
		let color = "#2172E5";
		
		if(nftData)
		{
			const legend:Payload[] = [];
			const poolLortAccount = nftData.lort.publicKey.toBase58();
			const creatorAccount = nftData.tokenSwap.tokenAccountA.toBase58();

			return {
				data:nftData.largestLortAccounts.value.filter((item)=>{
					if(parseInt(item.amount) > 0)
						return true;
					return false;
				})
				.map((item, index)=>{
					color = (index > 0) ? randomShade(color) : color;
					if(poolLortAccount === item.address.toBase58())
					{
						legend.push({
							value: "Pool",
							color: color,
							type:'square'
						});
					}
					if(creatorAccount === item.address.toBase58())
					{
						legend.push({
							value: "Creator",
							color: color,
							type:'square'
						});
					}
					
					return {
						name: "Lorts",
						date: new Date().toString(),
						value: parseInt(item.amount),
						valuePrefix: "$",
						color:color,
					}
				}),
				legend
			}
		}
		return {
			data:[],
			legend:[],
		};
	}
	const licenceDataProvider = ():IPieGraphData[] => {
		const result:IPieGraphData[] = [];
		if(nftData)
		{
			nftData.licences.forEach((licence, index)=>{
				let color = randomColor();
				const quantity = licence.licence.quantity;
				const supply = u64.fromBuffer(licence.mint.supply).toNumber();
				result.push({
					name: "Lic.",
					date: new Date().toString(),
					value: quantity - supply,
					valuePrefix: "$",
					color:color,
				});
				if(quantity - supply != quantity)
				{
					result.push({
						name: "Lic.",
						date: new Date().toString(),
						value: quantity - (quantity - supply),
						valuePrefix: "$",
						color:randomShade(color),
					});
				}
			});
		}
		return result;
	}

	console.log("NFT DATA", nftData);

	return(
		<main>
			<ExplorerHeader/>
			<section className="container">
				<SearchBar onChange={(publicKey:PublicKey | null)=>{
					if(publicKey)
						setNftKey(publicKey.toBase58());
				}} />
			</section>
			{ 
				(nftData)
				? 
					<ViewNFTPro
						data={nftData}
						publicKey={nftKey}
						lortGraph={lortGraphData}
						licenceGraph={licenceGraphData}
						tradeVolume={dataProvider()}
						priceOverTime={dataProvider()}
					/>
				: 
					<section className={"container"}>
						{
							(nftError)
							&& <h3 className="explorer-error-message">{nftError.message}</h3>
						}
						<div className="explorer-dashboard">
							<div>
								<h3>NFT-PROs Minted</h3>
								<h2>521</h2>
								<h4>Last 24h (39)</h4>
							</div>
							<div>
								<h3>Average price</h3>
								<h2><img src={logo} />1.521</h2>
								<h4>~ $ 1.650</h4>
							</div>
							<div>
								<h3>Total value</h3>
								<h2><img src={logo} />610.400</h2>
								<h4>~ $ 804.000</h4>
							</div>
						</div>
						<div className="explorer-graph-container">
							<div>
								<h3>Trade volume</h3>
								<AreaGraph data={dataProvider()} />
							</div>
							<div>
								<h3>Price over time</h3>
								<BarGraph data={dataProvider()} />
							</div>
						</div>
					</section>
			}
		</main>
	);
});
export default Explorer


// SEARCHBAR COMPONENT
const SearchBar:FC<{onChange:(publicKey:PublicKey)=>void}> = (props)=>{
	
	const [publicKeyString, setPublicKeyString] = useState<string>("");
	const [publicKey, setPublicKey] = useState<PublicKey | null>(null);
	
	return(
		<div className="searchbar">
			<input 
				className="searchbar-input" 
				type="text" 
				value={publicKeyString}
				onChange={(e)=>{
					setPublicKeyString(e.target.value);
					try{
						const publicKey = new PublicKey(e.target.value);
						setPublicKey(publicKey);
					}
					catch(err){
						setPublicKey(null);
						console.log("Invalid PublicKey")
					}
				}}
				onKeyPress={(e)=>{
					if(e.code === 'Enter')
					{
						if(publicKey)
						{
							setPublicKeyString("");
							props.onChange(publicKey);
						}
						else alert("Invalid PublicKey");
					}
				}}
			/>
			<button onClick={(e)=>{
				if(publicKey)
				{
					setPublicKeyString("");
					props.onChange(publicKey);
				}
				else alert("Invalid PublicKey");
			}}>
				<i className="fa fa-search" aria-hidden="true"></i>
			</button>
		</div>
	);
}