import { runInAction } from 'mobx';
import React, { FC, useState } from 'react';
import { AppStore, useAppStore } from '../../store/store';
import { createWallet, IWallet, IWalletInfo, walletInfo } from '../../../client/core/wallet';
import './style.css';

const ConnectWallet:FC = ()=>
{
    const store:AppStore = useAppStore();
	const [showPicker, setShowPicker] = useState<boolean>();

    return(
        <div className="connect">
            <button onClick={async (e)=>{
                setShowPicker(true);
				// await store.wallet.connect();
            }}>CONNECT YOUR WALLET</button>
			{
				(showPicker) && 
				<WalletPicker
					onClose={()=>{
						setShowPicker(false);
					}}
					onSelect={(selectedWallet:IWalletInfo)=>{
						runInAction(()=>{
							const wallet:IWallet = createWallet(selectedWallet.id, store.cluster.url);
							store.wallet = wallet;
							wallet.connect();
						});
						setShowPicker(false);
						console.log(`Wallet selected ${selectedWallet.name}`);
					}}
				/>
			}
        </div>
    );
};
export default ConnectWallet;

const WalletPicker:FC<{
	onSelect:(wallet:IWalletInfo)=>void,
	onClose:()=>void
}> = (props)=>{
	const walletProvider = walletInfo();
	return (
		<div className="wallet-picker">
			<button 
				className="wallet-picker-close-button"
				onClick={(e)=>{
					props.onClose();
				}}
			>
				<span>X</span>
			</button>
			<ul>
				{
					walletProvider.map((item)=>{
						return (
							<li key={`wallet-${item.name}`}>
								<button 
									className="wallet-picker-button"
									onClick={(e)=>{
										props.onSelect(item);
									}}
								>
									<img src={item.icon} />
									<span>{item.name}</span>
								</button>
							</li>
						);
					})
				}
			</ul>
		</div>
	);
}