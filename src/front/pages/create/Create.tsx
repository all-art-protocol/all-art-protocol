import { ReactElement } from 'react';
import { Route, Switch } from 'react-router';
import CreateRight from '../../components/rights/CreateRight';
import CreateRights from '../../components/rights/CreateRights';
import CreateAccount from '../../components/user/account/CreateAccount';
import CreateNFTPro from '../../components/artwork/CreateNFTPro';
import { AppStore, useAppStore } from '../../store/store';
import CreateLicenceCategory from '../../components/licence/CreateLicenceCategory';
import CreateLicencePreset from '../../components/licence/CreateLicencePreset';

export default function Create():ReactElement
{
    const store:AppStore = useAppStore();
    return(
        <Switch>
            <Route path="/create/user" exact>
                <CreateAccount />
            </Route>
            <Route path="/create/right" exact>
                <CreateRight />
            </Route>
            <Route path="/create/rights" exact>
                <CreateRights />
            </Route>
            <Route path="/create/licence-category" exact>
                <CreateLicenceCategory />
            </Route>
            <Route path="/create/licence-preset" exact>
                <CreateLicencePreset />
            </Route>
            <Route path="/create/artwork" exact>
                <CreateNFTPro />
            </Route>
        </Switch>
    );
}