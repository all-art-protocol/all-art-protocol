import { PublicKey } from '@solana/web3.js';
import { runInAction } from 'mobx';
import { observer } from 'mobx-react-lite';
import { Switch, Route, Redirect } from 'react-router';
import { FC, useEffect, useState } from 'react';
import { CREATE_USER_PROGRAM } from '../../../client/core/consts';
import ReadUserAccount from '../../../client/programs/ReadUserAccount';
import { AppStore, useAppStore } from '../../store/store';
import { createUserAccountSeed } from '../../../client/core/seed';
import CreateNFTPro from '../../components/artwork/CreateNFTPro';
import CreateAccount from '../../components/user/account/CreateAccount';
import CreatorHeader from '../../components/header/CreatorHeader';
import Congrats from './Congrats';
import './style.css';

const Creator:FC = observer(()=>
{
    const store:AppStore = useAppStore();
    const [profileLoaded, setProfileLoaded] = useState<boolean>(false);
    
    useEffect(()=>{
        if(store.userData == null)
            getUserAccount();
        else
            setProfileLoaded(true);
    }, []);
    
    console.log("Render Creator");
    console.log(store);

    async function getUserAccount()
    {
        const programId = new PublicKey(CREATE_USER_PROGRAM);
        const accountSeed = await createUserAccountSeed(store.walletKey);
        const accountPublicKey = await PublicKey.createWithSeed(store.walletKey, accountSeed, programId);
        const userAccount = await new ReadUserAccount(accountPublicKey, store.cluster.url).run();
        if(userAccount)
        {
            runInAction(() => {
                store.userData = userAccount;
                store.userKey = accountPublicKey;
                console.log("Creator set state");
            });
        }
        setProfileLoaded(true);
    }
    
    return(
        <>
            <CreatorHeader />
            <div className="container">
            {
                (profileLoaded)&&
                <Switch>
                    <Redirect exact from="/" to={
                        (store.userData === null)
                        ? '/create/user'
                        : '/create/nftpro'
                    } />
                    <Route path="/create/nftpro" exact>
                        {
                            (store.userData !== null)
                            ? <CreateNFTPro />
                            : <Redirect exact to="/create/user"/>
                        }
                    </Route>
                    <Route path="/create/congrats" exact>
                        <Congrats />
                    </Route>
                    <Route path="/create/user" exact>
                        {
                            (store.userData === null)
                            ? <CreateAccount />
                            : <Redirect exact to="/create/nftpro"/>
                        }
                    </Route>
                </Switch>
            }
            </div>
        </>
    );
});
export default Creator;