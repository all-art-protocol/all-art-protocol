import { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { AppStore, useAppStore } from '../../store/store';

export default function Congrats():ReactElement
{
    const store:AppStore = useAppStore();
    return(
        <section className="nft-congrats">
            <div className="nft-congrats-content">
                <h1>CONGRATULATIONS</h1>
                <h3>
                    You have successfully minted your NFTPRO.
                </h3>
                <p>
                    PublicKey<br/>
                    <span>{store.nft?.toBase58()}</span>
                </p>
                <hr/>
                <div className="nft-congrats-controls">
                    <a href={`https://explorer.all.art?pk=${store.nft?.toBase58()}&cluster=${store.cluster.id}`}>Explore</a>
                    <Link to="/create/nftpro">Create another</Link>
                </div>
            </div>
        </section>
    );
}