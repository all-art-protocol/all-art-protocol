import { ReactElement } from 'react';
import { Route, Switch } from 'react-router';
import ReadLicenceCategories from '../../components/licence/ReadLicenceCategories';
import ReadLicencePresets from '../../components/licence/ReadLicencePresets';
import ReadRights from '../../components/rights/ReadRights';
import ReadAccount from '../../components/user/account/ReadAccount';
import Explorer from '../explore/Explorer';

export default function Read():ReactElement
{
    return(
        <Switch>
            <Route path="/read/user" exact>
                <ReadAccount />
            </Route>
            <Route path="/read/rights" exact>
                <ReadRights />
            </Route>
            <Route path="/read/licence-categories" exact>
                <ReadLicenceCategories />
            </Route>
            <Route path="/read/licence-presets" exact>
                <ReadLicencePresets />
            </Route>
            <Route path="/read/artwork" exact>
                <Explorer/>
            </Route>
        </Switch>
    );
}