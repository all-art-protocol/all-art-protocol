import React, { createContext, FC } from "react";
import { makeAutoObservable } from "mobx";
import { createWallet, IWallet } from "../../client/core/wallet";
import { WalletProviders } from "../../client/core/enums";
import { useContext } from "react";
import { defaultCluster, ICluster, walletUrl } from "../../client/core/consts";
import { PublicKey } from "@solana/web3.js";
import UserAccount from "../../client/data/UserAccount";

export class AppStore
{
    connected:boolean = false;
    wallet:IWallet = createWallet(WalletProviders.Sollet, defaultCluster.url);
    walletKey:PublicKey = new PublicKey("11111111111111111111111111111111");
    userKey:PublicKey | null = null;
    userData:UserAccount | null = null;
    nft:PublicKey | null = null;
    cluster:ICluster = defaultCluster;
    
    constructor()
    {
        makeAutoObservable(this);
    }
}

export const AppStoreContext = createContext<AppStore>(new AppStore());
export const AppStoreProvider: FC<{store:AppStore}> = ({store, children}) => {
    return <AppStoreContext.Provider value={store}>{children}</AppStoreContext.Provider>
}
export const useAppStore = ()=>{
    return useContext(AppStoreContext);
}