import { PublicKey, RpcResponseAndContext, TokenAccountBalancePair } from "@solana/web3.js";
import { useEffect, useState } from "react";
import { connect } from "../../client/core/connection";
import { TokenSwap, TOKEN_SWAP_PROGRAM_ID } from "../../client/programs/TokenSwap";
import { AccountLayout, MintLayout, u64 } from "@solana/spl-token";
import Licence from "../../client/data/Licence";
import NFTPro from "../../client/data/NFTPro";
import UserAccount from "../../client/data/UserAccount";
import { validate } from '../../client/core/validation';

interface IProps {
	publicKey: string | null,
	cluster:string,
}
export interface INFTProData{
	nftPro:NFTPro,
	licences:{
		licenceKey:PublicKey,
		licence:Licence, 
		mint:any,
	}[],
	artist:UserAccount,
	lort:IToken,
	artCoin:IToken,
	tokenSwap:TokenSwap,
	largestLortAccounts:RpcResponseAndContext<TokenAccountBalancePair[]>,
}
export interface IToken {
	amount: number;
	publicKey: PublicKey
}

export const useNFTPRO = ({publicKey, cluster}: IProps) => {
	const [result, setResult] = useState<INFTProData | null>(null);
	const [error, setError] = useState<Error | null>(null);

	useEffect(()=>{
		if(publicKey && cluster)
		{
			console.log("TRY LOAD NFT");
			loadNFT();
		}
	}, [publicKey, cluster]);
	
	const loadNFT = async () => {
		try
		{
			if(publicKey && publicKey.length === 44)
			{
				console.log("LOAD NFT");
				let nft:NFTPro | null = null;
				let tokenSwap:TokenSwap | null = null;
				let artist:UserAccount | null = null;
				let lort:IToken | null = null;
				let artCoin:IToken | null = null;
				let largestLortAccounts:RpcResponseAndContext<TokenAccountBalancePair[]> | null = null;
				const licences: {licenceKey:PublicKey, licence:Licence, mint:any}[] = [];
				
				const connection = await connect(cluster);
				const payer: any = {};
				const nftKey = new PublicKey(publicKey);
				const nftAccount = await connection.getAccountInfo(nftKey);
				if (nftAccount) 
				{
					nft = NFTPro.deserialize(nftAccount?.data);
					tokenSwap = await TokenSwap.loadTokenSwap(connection, nft.camm, TOKEN_SWAP_PROGRAM_ID, payer);
					
					if(nft.licences) 
					{
						nft.licences.forEach(async (key: PublicKey) => {
							const licence = await connection.getAccountInfo(key)
							if(licence)
							{
								const licenceData = Licence.deserialize(licence?.data);
								const mintInfo = await connection.getAccountInfo(licenceData.licence_mint);
								if(mintInfo)
								{
									const mintData = MintLayout.decode(mintInfo.data);
									licences.push({
										licenceKey:key,
										licence:licenceData,
										mint:mintData,
									});
								}
							}
						})
					}

					if(nft.artist) 
					{
						const artistAccount = await connection.getAccountInfo(nft.artist)
						if(artistAccount) 
							artist = UserAccount.deserialize(artistAccount.data);
					}
					
					if(tokenSwap.tokenAccountA) 
					{
						const lortAccount = await connection.getAccountInfo(tokenSwap.tokenAccountA);
						const lortAccountData = AccountLayout.decode(lortAccount?.data);
						lort = {
							publicKey: tokenSwap.tokenAccountA,
							amount: u64.fromBuffer(lortAccountData.amount).toNumber()
						};
						largestLortAccounts = await connection.getTokenLargestAccounts(tokenSwap.mintA);
					}
		
					if(tokenSwap.tokenAccountB)
					{
						const artCoinAccount = await connection.getAccountInfo(tokenSwap.tokenAccountB)
						const artCoinAccountData = AccountLayout.decode(artCoinAccount?.data)
						artCoin = {
							publicKey: tokenSwap.tokenAccountB,
							amount: u64.fromBuffer(artCoinAccountData.amount).toNumber()
						};
					}

					if(nft && licences && artist && lort && artCoin && tokenSwap && largestLortAccounts)
					{
						setResult({
							nftPro:nft,
							licences:licences,
							artist:artist,
							lort:lort,
							artCoin:artCoin,
							tokenSwap:tokenSwap,
							largestLortAccounts:largestLortAccounts
						});
						setError(null);
					}
					else
					{
						setResult(null);
						setError(new Error(`Unable to load NFT - ${publicKey}`));
					}
				}
				else{
					setResult(null);
					setError(new Error(`NFT ${publicKey} not found!`));
				}
			} 
			else 
			{
				setResult(null);
				setError(new Error(`Invalid NFT PublicKey!`));
			}
		}
		catch(err)
		{
			setResult(null);
			setError(new Error(`Unable to load NFT - ${publicKey}`));
		}
	}

	return {nftData:result, nftError:error};
}





// import { PublicKey, RpcResponseAndContext, TokenAccountBalancePair } from "@solana/web3.js"
// import { useEffect, useState } from "react"
// import { connect } from "../../client/core/connection"
// import Licence from "../../client/data/Licence"
// import NFTPro from "../../client/data/NFTPro"
// import { TokenSwap, TOKEN_SWAP_PROGRAM_ID } from "../../client/programs/TokenSwap"
// import { AccountLayout, MintLayout, u64 } from "@solana/spl-token"
// import UserAccount from "../../client/data/UserAccount"

// interface IProps {
// 	publicKey: string,
// 	cluster:string,
// }

// export interface IToken {
// 	amount: number;
// 	publicKey: PublicKey
// }

// export const useNFTPRO = ({publicKey, cluster}: IProps) => {
// 	const [loading, setLoading] = useState(true);
// 	const [tokenSwap, setTokenSwap] = useState<TokenSwap>();
// 	const [nftPro, setNFTPRO] = useState<NFTPro | null>();
// 	const [licences, setLicences] = useState<{
// 		licenceKey:PublicKey,
// 		licence:Licence, 
// 		mint:any,
// 	}[]>([]);
// 	const [artist, setArtist] = useState<UserAccount>();
// 	const [lort, setLorts ] = useState<IToken>();
// 	const [artCoin, setArtCoins ] = useState<IToken>();
// 	const [largestLortAccounts, setLargestLortAccounts] = useState<RpcResponseAndContext<TokenAccountBalancePair[]> | null>(null);

// 	const loadNFT = async () => {
// 		if(publicKey) 
// 		{
// 			setLoading(true);
// 			console.log("useNFTPRO load", publicKey);
// 			const licences: {licenceKey:PublicKey, licence:Licence, mint:any}[] = [];
// 			const key = new PublicKey(publicKey);
// 			const connection = await connect(cluster);
// 			const payer: any = {};
// 			const nftproAccount = await connection.getAccountInfo(key)

// 			if (nftproAccount) 
// 			{
// 				console.log("Existing nft account");
// 				const nft = NFTPro.deserialize(nftproAccount?.data)
// 				setNFTPRO(nft);
// 				console.log(nft);

// 				const tokenSwap = await TokenSwap.loadTokenSwap(connection, nft.camm, TOKEN_SWAP_PROGRAM_ID, payer);
// 				setTokenSwap(tokenSwap);
				
// 				if(nft.licences) {
// 					nft.licences.forEach(async (key: PublicKey) => {
// 						const licence = await connection.getAccountInfo(key)
// 						if(licence)
// 						{
// 							const licenceData = Licence.deserialize(licence?.data);
// 							const mintInfo = await connection.getAccountInfo(licenceData.licence_mint);
// 							if(mintInfo)
// 							{
// 								const mintData = MintLayout.decode(mintInfo.data);
// 								licences.push({
// 									licenceKey:key,
// 									licence:licenceData,
// 									mint:mintData,
// 								});
// 							}
// 						}
// 					})
// 					setLicences(licences);
// 				}
// 				if(nft.artist) {
// 					const artist = await connection.getAccountInfo(nft.artist)
// 					if(artist) {
// 						console.log("ARTIST",artist);
// 						setArtist(UserAccount.deserialize(artist.data))
// 					}
// 				}
				
// 				if(tokenSwap.tokenAccountA) {
// 					const lortInfo = await connection.getAccountInfo(tokenSwap.tokenAccountA)
// 					const data = AccountLayout.decode(lortInfo?.data)
// 					setLorts({
// 						publicKey: tokenSwap.tokenAccountA,
// 						amount: u64.fromBuffer(data.amount).toNumber()
// 					})
// 					// const largestLortAccounts = await connection.getTokenLargestAccounts(tokenSwap.mintA);
// 					// console.log("!!!!! LARGEST ACCOUNT", largestLortAccounts);
// 					setLargestLortAccounts(await connection.getTokenLargestAccounts(tokenSwap.mintA));
// 				}
	
// 				if(tokenSwap.tokenAccountB) {
// 					const artCoinInfo = await connection.getAccountInfo(tokenSwap.tokenAccountB)
// 					const data = AccountLayout.decode(artCoinInfo?.data)
	
// 					console.log(u64.fromBuffer(data.amount).toNumber());
					
// 					setArtCoins({
// 						publicKey: tokenSwap.tokenAccountB,
// 						amount: u64.fromBuffer(data.amount).toNumber()
// 					})
// 				}
// 				setLoading(false)
// 			}
// 			else{
// 				setNFTPRO(null);
// 				alert(`at cluster ${cluster} NFT "${publicKey}" not found!`);
// 			}
// 		} 
// 		else 
// 		{
// 			setLoading(false);
// 			setNFTPRO(null);
// 			throw new Error("wrong public key!")
// 		}
// 	}

// 	return {loadNFT, loading, data: { nftPro, licences, artist, lort, artCoin, tokenSwap, largestLortAccounts }}
// }