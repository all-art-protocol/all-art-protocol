import ReactDOM from 'react-dom';
import App from './front/app/App';
import {BrowserRouter as Router} from 'react-router-dom';
import * as dotenv from 'dotenv';
import path from 'path';
import './index.css';
import './front/assets/font-awesome/css/font-awesome.min.css';

dotenv.config({path:path.join(__dirname.toString(), '.env')});

ReactDOM.render(<Router><App /></Router>,document.getElementById('app'));