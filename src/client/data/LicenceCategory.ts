import { IData } from '../core/IData';
import { fromByteArray, toByteArray } from '../utils/string';
import { RightProgramTypes } from '../core/enums';
import { PublicKey } from '@solana/web3.js';
import * as BufferLayout from 'buffer-layout';
import * as Layout from "../../client/programs/TokenSwap/layout";
import BN from "bn.js";
import { isZeroArray } from '../utils/array';

export interface LicenceCategoryInput{
    parent:PublicKey | undefined;
	title: string;
	description: string;
    order:number;
    salt:PublicKey;
}

export const LicenceCategoryLayout = BufferLayout.struct([
    BufferLayout.u8('type'),
    Layout.publicKey("parent"),
    BufferLayout.blob(128, "title"),
    BufferLayout.blob(512, "description"),
    BufferLayout.u8('order'),
    Layout.publicKey("salt"),
]);

export default class LicenceCategory implements IData
{
    type:number = RightProgramTypes.LicenceCategory;
    parent:PublicKey | undefined;
    title:string;
    description:string;
    order:number;
    salt:PublicKey;
    
    constructor(data:LicenceCategoryInput)
    {
        this.parent = data.parent;
        this.title = data.title.slice(0, 128);
        this.description = data.description.slice(0, 512);
        this.order = (data.order < 255) ? data.order : 255;
        this.salt = data.salt;
    }

    getSize():number
    {
        return this.toBuffer().length;
    }
    
    toBuffer(): Buffer
    {
        const buf = new Array();
		buf.push(Buffer.from([this.type]));
        buf.push((this.parent)
            ? this.parent.toBuffer()
            : Buffer.from(new Uint8Array(32))
        );
		buf.push(Buffer.from(toByteArray(this.title, 128)));
		buf.push(Buffer.from(toByteArray(this.description, 512)));
        buf.push(Buffer.from([this.order]));
        buf.push(this.salt.toBuffer());
		return Buffer.concat(buf);
    }

    static deserialize(buffer:Buffer):LicenceCategory
	{
        const data = LicenceCategoryLayout.decode(buffer);
        const parent = (isZeroArray(data.parent, 32))
            ? undefined
            : new PublicKey(data.parent);
        const title = fromByteArray(data.title);
        const description = fromByteArray(data.description);
        const order = new BN(data.order).toNumber();
        const salt = new PublicKey(data.salt);
		
        return new LicenceCategory({
			title, description, parent, order, salt
		});
	}
}