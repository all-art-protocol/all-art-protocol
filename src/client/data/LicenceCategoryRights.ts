import { IData } from '../core/IData';
import { fromByteArray, toByteArray } from '../utils/string';
import { RightProgramTypes } from '../core/enums';
import { PublicKey } from '@solana/web3.js';
import * as BufferLayout from 'buffer-layout';
import * as Layout from "../../client/programs/TokenSwap/layout";
import BN from "bn.js";
import { isZeroArray } from '../utils/array';

export interface LicenceCategoryRightsInput{
    category:PublicKey;
    salt:PublicKey;
	rights: PublicKey[];
}

const MAX_RIGHTS:number = 20;

export const LicenceCategoryRightsLayout = BufferLayout.struct([
    BufferLayout.u8('type'),
    Layout.publicKey("category"),
    Layout.publicKey("salt"),
    BufferLayout.seq(Layout.publicKey("right"), MAX_RIGHTS, "rights"),
]);

export default class LicenceCategoryRights implements IData
{
    type:number = RightProgramTypes.LicenceCategoryRights;
    category:PublicKey;
    salt:PublicKey;
	rights: PublicKey[];
    
    constructor(data:LicenceCategoryRightsInput)
    {
        this.category = data.category;
        this.salt = data.salt;
        this.rights = data.rights.slice(0, MAX_RIGHTS);
    }

    getSize():number
    {
        return this.toBuffer().length;
    }
    
    toBuffer(): Buffer
    {
        const buf = new Array();
		buf.push(Buffer.from([this.type]));
        buf.push(this.category.toBuffer());
        buf.push(this.salt.toBuffer());
		for(let i=0; i<MAX_RIGHTS; i++)
		{
			(i < this.rights.length)
			? buf.push(this.rights[i].toBuffer())
			: buf.push(Buffer.from(new Uint8Array(32)));
		}
		return Buffer.concat(buf);
    }

    static deserialize(buffer:Buffer):LicenceCategoryRights
	{
        const data = LicenceCategoryRightsLayout.decode(buffer);
        const category = new PublicKey(data.salt);
        const salt = new PublicKey(data.salt);
		
        const rights:PublicKey[] = [];
        for(let i=0; i < data.rights.length; i++)
        {
            const right = data.rights[i];
            if(!isZeroArray(right, 32))
                rights.push(new PublicKey(right));
        }

        return new LicenceCategoryRights({
			category, salt, rights
		});
	}
}