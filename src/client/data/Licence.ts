import { IData } from "../core/IData";
import { fromByteArray, toByteArray } from "../utils/string";
import { PublicKey } from "@solana/web3.js";
import { uint32ToBuffer } from "../utils/number";
import { isZeroArray } from '../utils/array';
import BufferLayout from 'buffer-layout';
import * as Layout from "../../client/programs/TokenSwap/layout";
import BN from "bn.js";

export interface LicenceInput{
	title: string;
	licence_mint: PublicKey;
	nft_pro: PublicKey;
	licence_rights: PublicKey[];
	lort_licence_account:PublicKey;
	quantity:number;
	lorts:number;
	file_hash:string;
}
const MAX_RIGHTS:number = 20;

export const LicenceLayout = BufferLayout.struct([
	BufferLayout.blob(20, "title"),
	Layout.publicKey("licence_mint"),
	Layout.publicKey("nft_pro"),
	BufferLayout.seq(Layout.publicKey("right"), MAX_RIGHTS, "licence_rights"),
	Layout.publicKey("lort_licence_account"),
	BufferLayout.u32("quantity"),
	BufferLayout.u32("lorts"),
	BufferLayout.blob(46, "file_hash")
])


export default class Licence implements IData {
	title: string;
	licence_mint: PublicKey;
	nft_pro: PublicKey;
	licence_rights: PublicKey[];
	lort_licence_account:PublicKey;
	quantity:number;
	lorts:number;
	file_hash:string;
	
	constructor(data:LicenceInput) {
		this.title = data.title.slice(0, 20);
		this.licence_mint = data.licence_mint;
		this.licence_rights = data.licence_rights.slice(0, MAX_RIGHTS);
		this.lort_licence_account = data.lort_licence_account;
		this.quantity = data.quantity;
		this.lorts = data.lorts;
		this.nft_pro = data.nft_pro
		this.file_hash = data.file_hash.slice(0, 46);
	}

	getSize(): number {
		return this.toBuffer().length - 1;
	}

	toBuffer(): Buffer {		
		const buf = new Array();
		buf.push(Buffer.from([0]));
		buf.push(Buffer.from(toByteArray(this.title, 20)));
		buf.push(this.licence_mint.toBuffer());
		buf.push(this.nft_pro.toBuffer());
		for(let i=0; i<MAX_RIGHTS; i++)
		{
			(i < this.licence_rights.length)
			? buf.push(this.licence_rights[i].toBuffer())
			: buf.push(Buffer.from(new Uint8Array(32)));
		}
		buf.push(this.lort_licence_account.toBuffer());
		buf.push(uint32ToBuffer(this.quantity));
		buf.push(uint32ToBuffer(this.lorts));
		buf.push(Buffer.from(toByteArray(this.file_hash, 46)));
		return Buffer.concat(buf);
	}

	static deserialize(buffer: Buffer): Licence
	{
		const data = LicenceLayout.decode(buffer);
		
		const title = fromByteArray(data.title)
		const licence_mint = new PublicKey(data.licence_mint)
		const nft_pro = new PublicKey(data.nft_pro)
		const licence_rights: PublicKey[] = []
		const lort_licence_account = new PublicKey(data.lort_licence_account)

		try {
			for (let i = 0; i < data.licence_rights.length; i++) {
				const licence_right = data.licence_rights[i];
				if(isZeroArray(licence_right, 32)) {
					break
				}
				
				licence_rights.push(new PublicKey(licence_right))
			}
		} catch (error) {
			console.log(error);
		}
		
		const file_hash = data.file_hash

		const quantity = new BN(data.quantity).toNumber()
		const lorts = new BN(data.lorts).toNumber()
		
		return new Licence({
			title, 
			licence_mint, 
			licence_rights, 
			nft_pro, 
			file_hash, 
			lort_licence_account, 
			lorts, 
			quantity
		})
	}
}