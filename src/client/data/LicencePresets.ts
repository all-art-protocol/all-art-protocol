
export interface LicenceCategory {
    id: string,
    title: string,
    description: string,
    parent: string,
}
export interface LicencePreset{
    name:string,
    categories:string[],
    rights:number[]
}
export interface RightBase {
    id: number,
    title: string,
    description: string,
    text: string,
    category: string,
    paraphrase: string,
}
export interface RightTemplate {
    id: number,
    title: string,
    description: string,
    text: string,
    category: string,
    paraphrase: string,
    seed: string,
    key: string,
}

export const LICENCE_CATEGORIES: LicenceCategory[] = [
    {
        id: "1",
        title: "Private Use",
        description: "The right of Private Use includes non-commercial uses of the work for solely personal purposes of an individual.",
        parent: "0",
    },
    {
        id: "2",
        title: "Public Display",
        description: "The right of Public Display includes either commercial or non-commercial intentional communication or other making available of the work to the public.",
        parent: "0",
    },
    {
        id: "3",
        title: "Reproduction",
        description: "The right of Reproduction includes making copies of the work for commercial purposes.",
        parent: "0",
    },
    {
        id: "4",
        title: "Commercial Public Display",
        description: "The right of Commercial Public Display includes intentional communication or other making available of the work to the public for financial gain and/or exchange.",
        parent: "2",
    },
    {
        id: "5",
        title: "Non-Commercial Public Display",
        description: "The right of Non-commercial Public Display includes intentional communication or other making available of the work to the public not for financial gain and/or exchange.",
        parent: "2",
    },
    {
        id: "6",
        title: "Strictly Personal Use",
        description: "The right of Non-commercial Public Display includes intentional communication or other making available of the work to the public not for financial gain and/or exchange.",
        parent: "1",
    },
    
];

export const LICENCE_PRESETS:LicencePreset[] = [
    {
        name:'Strictly Private Use',
        categories:["1"],
        rights:[],
    },
    {
        name:'Private Use and Public Display',
        categories:["1", "2"],
        rights:[],
    },
    {
        name:'Private Use and Reproduction',
        categories:["1", "3"],
        rights:[],
    }
];

export const RIGHTS_BASE: RightBase[] = [
    // PUBLIC DISPLAY
        // Commercial Public Display
        {
            id:1,
            category: "4",
            title: "Commercial public display in digital format only, in a gallery.",
            description: "",
            text: "",
            paraphrase:"EMJUpHyKYgf8jNMLQJx237C1RRACV7Fj6C2uziy9pym9_3hCxR71cp79ob5YnSF6SKaSfYUC9m1YM5zHuzAiWRusu",
        },
        {
            id:2,
            category: "4",
            title: "Commercial public display in digital format only, in a museum.",
            description: "",
            text: "",
            paraphrase:"9uyV3bpXbKTh9EuM98GuAr8N6mJ8s1ncAbQoDV1vygpF_HkTFKzevgH6N7QLJEvBEpuE9LR4xcifE48dHcKHyVafv",
        },
        {
            id:3,
            category: "4",
            title: "Commercial public display in digital format only, at an art fair.",
            description: "",
            text: "",
            paraphrase:"GfbV45QUiBNqYNhbdPLBhBFxn5ZPWYfhkF6UVBBqcQCY_8CnjY3qzPyPXEnUMUbTj3dbKXeCRnXoTFRmt2evpkCSx",
        },
        {
            id:4,
            category: "4",
            title: "Commercial public display in digital format only, in a virtual exhibition.",
            description: "",
            text: "",
            paraphrase:"Dnnf63rgv1pdZxDciEtd7Sy15BvUikuhpWC6Ex9Ju1iQ_FqhYwaSCu5pjH6Y8X7VMzs8ELaGFeaPiTtm9sgrUkboG",
        },
        {
            id:5,
            category: "4",
            title: "Commercial public display of a physical copy, in a gallery.",
            description: "",
            text: "",
            paraphrase:"AYmE5WFxWda146h61QDC4ZvydqYxgKuXUrFnT8t793qQ_9YyiFaPYwRAV37Y6t3FmMFXVWDs6WDwtchYNPRM7vSK",
        },
        {
            id:6,
            category: "4",
            title: "Commercial public display of a physical copy, in a museum.",
            description: "",
            text: "",
            paraphrase:"6ZPphhFvWUQFpVwPv8JtyWynZikmnBTvaJPVyXHHhmLu_7eeTT3iV2JLuiniSEJXBDehobvmdGv3hzH5TNQ785jNj",
        },
        {
            id:7,
            category: "4",
            title: "Commercial public display of a physical copy, at an art fair.",
            description: "",
            text: "",
            paraphrase:"5QGzdpvPBsYw5B7noJZn19ruwbBeKpwm2Tsx5dYsTjUk_3a23rZWWML8ojDVakMhBf1TvPjCWFjZxWqTXYpE2ss4w",
        },

        // Non-Commercial Public Display
        {
            id:8,
            category:"5", 
            title:"Non-commercial public display in digital format only, in a cultural institution.", 
            description:"", 
            text:"",
            paraphrase:"DMfDw89GoepXwSdvQE3CT6hSgf3Z8XHWt8XV61fqWSut_2UJX5dpXdTFoNCdPLjKoT7iWN7mmNmCPrsnD8Ef4RPQY",
        },
        {
            id:9,
            category:"5", 
            title:"Non-commercial public display in digital format only, in a virtual exhibition.", 
            description:"", 
            text:"",
            paraphrase:"CvnPBLwHt69RruaFnnBm8sXZ8f7Y8fJu5rVgMdAk1vn2_B5cPCmJx7NDATzcxyN9Pq8S6p8jjuXGErwZqYQhP7JsC",
        },
        {
            id:10,
            category:"5", 
            title:"Non-commercial public display of a physical copy, in a cultural institution.", 
            description:"", 
            text:"",
            paraphrase:"CGKAeEGxfTTVSGCwHpLykwW3CXGXnj3xwkphPn8pSUP8_9sZKst88trvVZjf4effD4MPZwqaHktZCCr8tm16Y6Y4W",
        },
    // REPRODUCTION
        // Commercial Reproduction (Non-Commercial doesn't exist)
        {
            id:11,
            category: "3",
            title: "Reproduction in digital format.",
            description: "",
            text: "",
            paraphrase:"5avMJn8UtUm9xbaqM7zmm8KFFLNwUQkistnzSbN6zEb7_5daP7p4yJvyQbAmaj9d2Fji9dyfmtKZyXgVGTe47o3EM",
        },
        {
            id:12,
            category: "3",
            title: "Reproduction in physical format (e.g. for merchandise).",
            description: "",
            text: "",
            paraphrase:"CZ3E2XytdFa6yLFRo1Zu95wBZ2ZJoVxPzjLq87qc9nvD_GcfHVKU41yUAa2pCaaAY8aQ6g1YDwh9x9W5si7d8Z6ed",
        },
        {
            id:13,
            category: "6",
            title: "Non-commercial use of the work for personal purposes only.",
            description: "",
            text: "",
            paraphrase:"beGhf3H5yNnTeBCLHByEx2Hto3kSWZ9hgCEuU8Azh2g_34BRYGVQV3gzJQXGPB7Z47pgXzR6EGHYkeSqbDQRG6Jv",
        },
];

export const RIGHTS: RightTemplate[] = [
    {id:1, category:"4", title:"Commercial public display in digital format only, in a gallery.", description:"", text:"", paraphrase:"EMJUpHyKYgf8jNMLQJx237C1RRACV7Fj6C2uziy9pym9_3hCxR71cp79ob5YnSF6SKaSfYUC9m1YM5zHuzAiWRusu", seed:"5e3571e1023d89b723cdcd6fca7bcd20", key:"2bUNgurjJ3PQjBEGLuYFEJSVmjc6uRo9rhWexzeXMRnG"},
    {id:2, category:"4", title:"Commercial public display in digital format only, in a museum.", description:"", text:"", paraphrase:"9uyV3bpXbKTh9EuM98GuAr8N6mJ8s1ncAbQoDV1vygpF_HkTFKzevgH6N7QLJEvBEpuE9LR4xcifE48dHcKHyVafv", seed:"e328d277766eeed4207dcbe48fc4dc1b", key:"AhSnb6d2QJQ2WX7H2YLDnQNECLr6CQafZ9oMAc4CcNb7"},
    {id:3, category:"4", title:"Commercial public display in digital format only, at an art fair.", description:"", text:"", paraphrase:"GfbV45QUiBNqYNhbdPLBhBFxn5ZPWYfhkF6UVBBqcQCY_8CnjY3qzPyPXEnUMUbTj3dbKXeCRnXoTFRmt2evpkCSx", seed:"d1850414e93816f6eb8eb2f3ceded6fc", key:"Gd25urssheiiw32hMuio1FJbKk2hUagqCUmJKLwYwavx"},
    {id:4, category:"4", title:"Commercial public display in digital format only, in a virtual exhibition.", description:"", text:"", paraphrase:"Dnnf63rgv1pdZxDciEtd7Sy15BvUikuhpWC6Ex9Ju1iQ_FqhYwaSCu5pjH6Y8X7VMzs8ELaGFeaPiTtm9sgrUkboG", seed:"4dc5cf378efb5fcab0f15e7c0b044be1", key:"8xx1zGkCykRBA9abnnNLJ4GwqGA6KjqCSy93VtyAuiYm"},
    {id:5, category:"4", title:"Commercial public display of a physical copy, in a gallery.", description:"", text:"", paraphrase:"AYmE5WFxWda146h61QDC4ZvydqYxgKuXUrFnT8t793qQ_9YyiFaPYwRAV37Y6t3FmMFXVWDs6WDwtchYNPRM7vSK", seed:"9653e2725cf84ae59190a590bb4edc90", key:"EXrXEL3xxumLGg3hw6c2gNsqC3MFqpTMu3EATjq2WbEF"},
    {id:6, category:"4", title:"Commercial public display of a physical copy, in a museum.", description:"", text:"", paraphrase:"6ZPphhFvWUQFpVwPv8JtyWynZikmnBTvaJPVyXHHhmLu_7eeTT3iV2JLuiniSEJXBDehobvmdGv3hzH5TNQ785jNj", seed:"a4ac3387abd392e81c1449b733b314f6", key:"5ovhmnJgs6wpqQ41fUecj2bZabsMLoAD2yeDWd9hmZBT"},
    {id:7, category:"4", title:"Commercial public display of a physical copy, at an art fair.", description:"", text:"", paraphrase:"5QGzdpvPBsYw5B7noJZn19ruwbBeKpwm2Tsx5dYsTjUk_3a23rZWWML8ojDVakMhBf1TvPjCWFjZxWqTXYpE2ss4w", seed:"3b51b83ecc6f0df6a4d5c68e7b03b44e", key:"FU21Mvjsb6MiDtYe7vJJ56TL57jPgMiGpPnq5KSsRAys"},
    {id:8, category:"5", title:"Non-commercial public display in digital format only, in a cultural institution.", description:"", text:"", paraphrase:"DMfDw89GoepXwSdvQE3CT6hSgf3Z8XHWt8XV61fqWSut_2UJX5dpXdTFoNCdPLjKoT7iWN7mmNmCPrsnD8Ef4RPQY", seed:"6ac1603a9c6b60d479dbc84e8aeb85e4", key:"6BQmam3YoA9DJJ8rGFsDrYF5VrVFoVi7YqKsewicaJpR"},
    {id:9, category:"5", title:"Non-commercial public display in digital format only, in a virtual exhibition.", description:"", text:"", paraphrase:"CvnPBLwHt69RruaFnnBm8sXZ8f7Y8fJu5rVgMdAk1vn2_B5cPCmJx7NDATzcxyN9Pq8S6p8jjuXGErwZqYQhP7JsC", seed:"79f1633e92ea40d2285a8e042e7986e1", key:"3UhDUswGDgre6CsYogyoSdYngk7VqdvUKRVro4zojDji"},
    {id:10, category:"5", title:"Non-commercial public display of a physical copy, in a cultural institution.", description:"", text:"", paraphrase:"CGKAeEGxfTTVSGCwHpLykwW3CXGXnj3xwkphPn8pSUP8_9sZKst88trvVZjf4effD4MPZwqaHktZCCr8tm16Y6Y4W", seed:"e5b3e39eff2a4ee057b274f72aa04b47", key:"9ZVwU223FYktLSc9GqQ4LwcF9hNs7juYVtFSnvuE7b9"},
    {id:11, category:"3", title:"Reproduction in digital format.", description:"", text:"", paraphrase:"5avMJn8UtUm9xbaqM7zmm8KFFLNwUQkistnzSbN6zEb7_5daP7p4yJvyQbAmaj9d2Fji9dyfmtKZyXgVGTe47o3EM", seed:"575f8f25d412d5bd242a1e106b781bb3", key:"EikpdynDiY37VuAkHWW7jEoXnrhpyM2LniJCuCnmH3Uk"},
    {id:12, category:"3", title:"Reproduction in physical format (e.g. for merchandise).", description:"", text:"", paraphrase:"CZ3E2XytdFa6yLFRo1Zu95wBZ2ZJoVxPzjLq87qc9nvD_GcfHVKU41yUAa2pCaaAY8aQ6g1YDwh9x9W5si7d8Z6ed", seed:"9bec29cf116f61f209fce9489b474a1b", key:"GwXUP8yR21NeFeMhUw5qtMPrckoG18bCCoMp6gJmLDqp"},
    {id:13, category:"6", title:"Non-commercial use of the work for personal purposes only.", description:"", text:"", paraphrase:"beGhf3H5yNnTeBCLHByEx2Hto3kSWZ9hgCEuU8Azh2g_34BRYGVQV3gzJQXGPB7Z47pgXzR6EGHYkeSqbDQRG6Jv", seed:"c369228d47984574cad4c785987829af", key:"9CJcMruogvjoKsufEnXVGgrYcFNpj25Vv22d2ZRwEm3G"}
];


// Paraphrase Stack
/*
DCHYw1JnZJzbDH9hgAnPYcCYdv63rAkMcHAuLRS1Bwzz_3Dc1NBzmxoMLfHMLwfeaAHYVLVVSsqBzpt4HsLwU4RyY
4N5cRm3eduVmAbP6Nb48uGWL9wfWuZVzTVM8wk4GcpvD_HV172GkwPPUXwKPN8ZBzywDwnQ6jJ3J7hJDebkQ1ZUM3
4mqr6kxwGEkaB2V1Ku3AkAQUSQ8ib73niX9kLXr5micf_ByotJpccGGK5J7L83SmTXPWBq54pJo94VY6mUWSK46bC
GHPyEk2BjrsyB365mff9ZY9uhkCrcoXPzbnNi5ccraXo_ABkdviwMaxxtEv13LgAoTqPvziEWcVQp4Qhh1SsbCA3L
3HiFgG4z5Pb8RHxdYddub8K9c71b2YmZETo9az9dTDB2_AAJohDJLgDQw7ZP3LwepoNhnswpBkvqDc7NdQGUBuZEM
BXH8zGew2Dtf6XAd3gZrxKreKaWaQfKQXhc5CbPvovg1_33auAywZvj5G7yJdzzd9Le2q2rZLQyDFqtWtRw3vL1VQ
Fv3tGAEHvG5mJrsLJsrKvzkHJS8e873cufaebQjfNN3z_6eujffxJ7q7EEPUpQUAL9jjwqJMJBp2C98fGjVxLQX83
6CKD7XVLVjgM7DDLCnjPq5CoTkPQ7qNdS84oB6uenaxL_HobRtS48guBWPcuUVvUQEHgdbHxhyUhdYZP1mjpoW42z
93axtnymqXVaEwmdXTz4cTwkNA6qczBbKvx8XBpoK9u6_B9op22K2CGFTTp2AP7XZD6qYC1ZQsHszTXu5UScaLfSA
FgDh5LXFHMhvvs6RQawEM2DYR4HnENgJcZD4C46PaRQ_7mmxMdYUSJ7C7AUKuBXHA2sB4dHPV79fZhCtswrUgrYA
8EcyNe1PtvZj4MevDf7WCtPVCMjvjQiETVQ4GnWc9DZC_FPkzqiojmT75xVy3bAJkvesDYG5odzLuB7CsNY7cywW5
8QwnMnameTiZ1PJKQhFBueLHkmhiUAdMgfF8wdujvZ2e_Ec7oosYBgt3hWQgcs7Q5axHQUUKhcTJCkQTvQimxLJ9Z
FRfaRrRLiKDimMNA2Tp7nkojvPZ9KhkVvu1WaBz8JRnW_8BKUZVMMeEqHQWHm5ekc8BQM9inKubsZ2GY4aWSDxgQ2
ESW64PRZLuvbsqPnZPFC4327DHuD7hKv38iLtMyRnHLW_U6uNQyfZXVg4LZLaAwVaRDuRFismwk8oxojD5Dz3cQJ
3fqdYFq4JFVP4gvaF77eKcPoEC1Vo1NfqW1b9xuhnp2W_89c4ryQuKLQshrg6cCZraaqa3aYoHsQM5j4JqcNGVi7k
EULPtxJXeQEpAUQDJKMfXN1aKwVoKaWvMn7f9g4mZkkw_Ds3JVvtxhtY5vKTSmRZvzFMqAQGsuM7VWuy2tu37NXFa
ACjQFEZSKEQPG3cw4388gtAo1ytxP9sJodvGUqFCizxn_xExMvozgrasxj7ViUuqY3ireyGALyCVCyVbtn1pX8pt
2YzWEJBRgoxokWfbaCrebhMnSah6oeAAojUnzYiPDQiw_6mFkoK8bvwE8Brky8SQLp472MzN3165HnYPpCm4cDugt
A6ssiinzye99HDKaWoR6dGfHJ7trVNWAvz8gynawutzW_4nxTgDeGu8FBCAzk7uXTy7uqpDxzZ2h1RzEzHeRkD3wu
CkKLr4FaW6zhixa7Twcb7y9ZCt1ZToa2yurPYda8AgZQ_8tq5cXUQ8JHNJg443miwbszdnyBjF6rhC7c11TAXuEWi
FnynNKmjUwM2SALEer1Poz7gUY8SUiMJiB6pmGHitkwX_3Em85Ybo8sLS6DZh7yXvDHpjbiwGTStrxEypJipBph6b
9mjzDzDyciV9N517XmkvghQqGo927Yq1mFmLi3BtYCbC_HgdcbAo7oJsv3PyqWkMp3jbLde6gXgk89n3v9dY9tsVF
CPgyzBpkcvcHTuX3BGERrn1Rm61Mb7R7Lo1th1h2CXx2_69jrSPnto59Mgb4Qddy26z5QaYeHiTbtyTGseLJ1t7qs
9fNRVBF7Uc5Rfcq9qUvN193ou3NenEKzfjNhBkDbV96u_CHDhfRryoMd9Lkdsfgr3PtqE3xfkPuQjPW1gGQdxBYEK
8cZmmqsTEhCX5fTYZdmmcjLWC5brPTwyoKLYyNxU7GQV_EYo5aWoWLS4jhocCEYgxfzogSR6UMZFwKfhakF34opZh
2RXaTotXVP3pRu9oPFGLgiCkL8zS3MLFa9b8YYRJY5YJ_4kHeojmtzvDZ7aVw71ESWAaFJdkB6CGytvUARMF3hSPh
5ErmL5tq5qZt39PuEvf7kNkZqtJK7ocY8KKgqnHcjuYc_JDec6oV1whkHWQevGHa5MUbFXCiLEVHb5z6XYSgFpwU6
6wsYcDgCMteo5w2jZ9hhKANcX8qPbXdaaLci6pVxjqxb_2Fm6AQfo2Kd26gEXfoyhryGm6kBky817vk75dt5YocHK
8xGsrbvBom4qqwwdR9UL7CgE6S77HWYKTDU2joagScMi_4wAfTBHja1wWRi1x8ajwrLmj8Afi2NshDrY6hhSpr9HP
9m113eWKDB4jtfV7rdWLRWP4nCSkTRMCwA4APXFsjxmg_64pdJeEd2YMLoRQP9X2V2xa2gYGYh7bCymgEMTbh7dTb
Anv5MLbfc9F7jhFNRTkWQnGbLcxCZzk4xWXpzJYiamhV_Cg847WEx4x55tUpBWiuLQXLChhSAKyagVwCNg64CnZiA
DDcp3xPLzPsGtxZZNryUKKvGC5YeosdFeVUY1ah6Sofj_BVfY6TC7W4mxMnDC1Zvk48QYNXqWMwWhGMnGuL56EThk
CVayACdDBnKNesF3xTWcJ2hREeXET76Q8eNnsHM58xTj_8Q6Q5FodwyC7uD4RB78F5SiT6aoMWrADY3BLFWfmvqQ5
5GJnHpy3WgUK9kGeaQEFxRqDk7BiuZXTKbcuBizKbFr7_82uixEV5s6ad6Kqb4BFdRjiM8sDjGiuyMb41NR4UDsEJ
9GjXCeoHPcoYAFCpy4z5BqWUYtuCa3e3AH16UakrbhKN_BRJUYxrdnvWuz7vG9Q4RBRKHmRTcVATYhyf7ePqsG2qt
2LzPXsAGhsGgr9YCGGV4BNBgx9V192qhGsLx8oTLK2qv_FXBWnGphVpyHfYKTwdGgfwGQHspSscnkeMPSu6gGMcD6
2U6Phc8W6TVfHsdatbvvdHnCeoUFwyaegfjgHCDPtmnM_Bfo9y23ymFGEAxytLLxWqb9HrH3bLn39xkqnHhJGuhNa
Enp4Rzeyn3SAMpLR88rnJjahtf4wByS7SqqvppFe2Qo2_BEwrKb7ostP27279PPKP5M2DxwhbYytK5bqSHzbVDdWy
J8qb9JvXijpRfVEK53bwRQM1BHkYp8s2YjuqdsMCThc9_BqPDHVhzjWnVEjMM5xXPkcq3FXZJrZ7F6rFKR5SNitf1
9heN1EA1ruRKpiMATYg5L6VsEoVSzusqsZnE4AMT3u6u_3bLyVVhgbcZW6wffZnkkeoBeMQgxqzAfoAQB9vP7QoA1
6vquy25npue8VE3fpw1te5dzajhe2dgYGtu7aN73QonB_Efq2btYzBgxdgeTN6XwnMoZemtV1AhohAmRALn72y2VP
Dqqxw9QzPjej5sohJZgidz3AkbDzKaHuwRdUdGMqEY24_JCJdLwzBPsQoAD8iBoeg5E7xJEHNqa5VPKj1BrtqN1te
88J7t6LeqQasabHitLvXfaW6gc6qVgyy1CUw28N6nK6A_HMkBTinYnRcYuhjtYzbrPN12grwHY48FhQiGbiBxvosE
AfvT195wfwj5GVBDHpMQn8esMrnEQs7AX7hAkyoU56K6_931ioJUqhKrFdmn1UEi785jL4GVs2zSNBRH93vk1uBSH
GozEDKfDy7jxtyRgpBFG7AxjQz4bYpLbxuvYK7yq82i1_AGj7JqL5afrruf94BrktPHuc6twQANSNSr4rZktyEj4D
C7yteh8BiHJAaonVRmx4sVvWjN31BGv9Y9ZyYYtLn2D_HPK8ikR613FhEdifndVRNymk251WXopZkC4Pu3XYqZua
BwhJNhceAJXya2dtjg4hwrDu3fWC9k8QWoCyjGqfk8r7_dJz8h5ZQPAok34uvS6z8zpxarTwZiHTzzkgb1fw4AUM
DUiZBXAox5WNvviDToimkTGocj15JHfvyJpyLezRdq6e_71dQnA1KBsYnkUPTjtMrY8j2ZBSgD3jBktzp2jMbfZib
38XyqXCVnA1JgMSQzr6rVZg5s2DSAZgtt9JwFRzC9ufn_AbuWnKe4FRaWwP1DKq1bZWw9qAnZWaSnKoW6pMJuHrh7
GR5UeL4Uu97j5P8UYQj1VsPsXPzraz1LbPHxt1s7FxjX_Ej22rtj3Uq49MYUKfKHguiH9dtf8a47qTdcDE9DwZgeH
6FLiaJD3p6dPdPiTW9umVp77mE68akghcTRX9BjYparY_5tAaREsyQHaGafTGFkQHkbDE3DMyab28d6Ypv6LZFn8k
DQPfE4xdecdJ7tMRMoMVezofqYnB9EGeKHtaCTbLNNti_BqwmXMhThy2Q4B889T6BjpFbdfuCN8JQyPMLPMQtRPZf
6wuYsCM3b1SbebeugpDWWUtomQRfhJqYMgaCB2a4i3sF_VpRkgyMAQCeHCNVNdm76Ds2s3WQFb5sDSHvxHPfD1kZ
9nF8EUDcNsmGs5GmU5HJseLwfZUVVF9PJfqay3PjWDHg_EGByoxwUpZZWT7w5ubPUnH6jsBFYSbSRTMbJNJGu7FRb
62b278hqKm6jHsW8Z5ZQPDe9cS216NyDkpQQYzSEgH96_94Fu83fefoJbZTMTYt4j7pLdk27fdzV62ZhQaiy7AhKc
FV91WARHa5Kx6nqxLns2U3a6zs1w23SQPYzGJCqgkXdW_xmaXNWPveQ2MBbxjMrQb6A9NnB5aLbjt6xjS2DCE2Lp
8fBfu7oCJ1hx9bRYWUTssT5XcCwx8mURzpTBc59W6veg_8URW37tRiLqt2WDtBqRJPNejgSTpJhgVUVCpGqX1HHNP
5GXQ3dpVDFUZjGHhZeHb93yXzgKfeqrJbmpqW9uYn5jm_DwyvvgBcuouhyiih5jZ8Ac4dnjDH9J9Q9Sk7GH3apusu
8sot3mhEMyj9i9S3KnTiCYtY4aHYmoRYJXktP6epYTmR_FKtJUXDRxpPzGAsYN1XNyqtRK9sSac4cYjfuemeQMPCL
EG96PqQZfDJ5Yjj1ztx3XVSuhVZDbHRqPaGDBocZ5A2k_5BSZ6j6SjzAkJoFEc2X813RsgNDw2ZXE8KNe6bLi71KB
7dzUZ24ybnK1zHBeLUGQrsysodkYj9nJbDhXXV3sG8Zm_F1QCAVJnPfAzDV9LXCcjn9qDfJi9MvNtoq6gUgBZzeQh
9Jcz3w3KHkt3TMHMMQx8azxpNNxexWn7RvTcWYZPhWnR_J8XyXbijk7hZR2C8MnwJw2Mm4zVxHB5WfrWK7BWgAmhT
FwyAY67gVGqGYS3P2ydmpHEZfPJF6Qu6E4w6Eq7AVyLU_Bp9ook8aRWQA7bprYnQJkdn5R5PeMuPdgKbRJSKvZAqu
9Kta3P9n37GpnNc8fBsn89YEVKMvWU3aa7aKZpyemVtJ_CfNyoPmMgZKNy1zcyTTW2JNeQypHtKP5JyRPNJwccvNV
AkTLhXdGm9EBADtB3u8Gpq8BfDT69UqtqUwGfbH2e4xc_6w8VcanDBj4Nzsd8EnY5TkARtZABm5AHvKP5eYHN2cap
6oW3VwJKRv12gpFtbzn3a4ybjZkK6rxBLy2CbWqmqEwJ_3p6KStEoam5cV8KgBueGgqsKvTSNyq6kaH1EAhdfz3zv
2jKUoYH5huNJcC1uphHagsumvtCfdwJ5iNkjG5DdxhG8_DgNDCr1wJZnzNPAGzFxJtjAWZFmyVQ5BZA3DzmWUW9cE
FwNmoeZKTMYhVDjh32AcKNbDPNRxtsrXnnJFV1YVaDHa_F8gQqWqtbk55npxZe7pjBrp4VaTWdzmdJiHcpHFvm7BR
EEnfEZNPcv1QYLeTfohxkKiJynpLk1iQJyqnvBxLRtnf_J5Sfy5QY2isWL9qQpz8gVDrKoXEZXScV8EPigzDk4dxd
EwAPK6kT4wyBymN7NnTrSB4pkKUFLWhkPojt6ggSqDes_7mox8WDmDVBHz9TCaVZFvUPGj2TEmLmUsUWhhkMrSGgy
6cnMGEQVrh3cgxutWdnccpdzPsoKs6aN52BKT73Qy7hx_789eEvaFoYM5Xg4Ga1MU7wFUk8GCsqzkthY8FZ8kLFH3
9s2NC4ftkC8sXfNr3Ty4KXupnToCuM7ZMMhF6TrvWF8X_5dwvUnwtFfRrWwbY2SVUHbsFQ4SQUgmgVgsjriDzyw6y
Gr482RfN4QQ4ofV168LQZ1LoRzLU8ot4xLQUMHL7qTS9_ATLaXR16okURPc8NRyxHZ7RTgkyvZrHfFN5QVmaLgnBX
GfdSRhv9W34Ai16of79LTcnuw3rvhWsXB5FPQxHzyfcN_D8PVxzGXBtXAyBsjUrM1FMn5xnpnqJbyNh956BUzF2eK
KbyuDVATRLHBtCubL28tFaVGGW9Gg8UiwERqatrA4DD_BZFzHeoLPTeUP25vaVbB6cEdQtrWrWMKDZWdJjfVVg3h
DUhbnoirZKTU3ZM1sfM25Xa63cwdNXoF38z4SwCAcZR7_8wgfyKzXvzm9qUpatUKbqicNxqc5ohgSagb1SWa2b8eJ
GMQkN2UvpiRCFLGiZhmuLMvBsR88XoCLMkbjKmh32bLe_7easd9VmRZHSbFwq2BbqbCEGniKZUqSrAfNdmvYH56pG
pB3o3wGtYem5mKGCLiyvKV2SoPnMu7xVj4HjV5tzFdH_Bvks8xSYRx3xJGHp8V2S2xnJNArMwDQgtHVhsxTFbDLg
GBU88k7j58G2LeTpDhPqPTU56VZw4wobitVr679VYHPK_J6HnU8PsyA9KJ1SwFmQbzuragAVRe7KJAzuFpYkZo5Xi
HD4X6BUgqTJksHusbmDVqsJXRhCPW46kRtS4Y3vpSn3P_21UHuvZzPUE5KBzuH1a4PkvDBuTHeiPkdFYLhgJ4Lxts
GonfPEQQ1b8HHX9k13Ptv7dn3m22nnafpyvaRf7A8FBA_54fFsxF8wE6Aaa6pdfNhWUVHgjrLtWyudK8RaRz87WzZ
2uPfKxpbxBtJBV9VrEbHJtKxaS7x56x1H66vhm4sCmSq_SR6o2rMUrnLkaTPGfPCdAEXqj9rTZKBjRHgtMDzLtbY
5oqoGwfQ6BNW8sBncWxfmAByqVGQVBJ1RnF5Gd7raghE_88a8dFt3a9jDEMUaPdxyCZ9dnaSBZA8LAtqhEamGrzzt
BD4erN4VoLumc2ErYjwgJeHb3eLunRkaeqau2S3n5fCY_DueXb9hWxo2kwjQKUhSU2Boo81TSUK52Kc9Jx3AS1TMc
64YFz7g6dCi1bMXhfeAc7sLoeUmNZ8nFGRNQtGmD1WaX_9eUunCsQXoggvBdk9Ajo55LrnqMXofof9wTVgknzd2jv
9b5omrBopc69wHV4RFNMS2CYtgCnqbvg55kcCGhVhtHF_8fqDyFzbCCraTRcJKbh56PZ6p6ngQiZbyMaCDu4nAudk
NXWHmE3jtysRYjC9BSQjXVDWtCzwCyVq5jsGAhP3E5M_Cvrim39EifEhJAhc4z4N432GVKWDvzxjYjyiTp2mKcZ8
*/