import { IData } from "../core/IData";
import { PublicKey } from "@solana/web3.js";

export default class LicenceLort implements IData {
	lort_mint: PublicKey;
	
	constructor(lort_mint: PublicKey) {
		this.lort_mint = lort_mint;
	}

	getSize(): number {
		return this.toBuffer().length;
	}

	toBuffer(): Buffer {
		return this.lort_mint.toBuffer();
	}

	static deserialize(buffer:Buffer):LicenceLort
	{
		return new LicenceLort(new PublicKey(buffer.slice(0, 32)));
	}
}