import { IData } from "../core/IData";
import { fromByteArray, toByteArray } from "../utils/string";
import { PublicKey } from "@solana/web3.js";

export interface NFTProInput{
	title: string;
	description: string;
	licences: PublicKey[];
	artist:PublicKey;
	camm:PublicKey;
	lort_mint:PublicKey;
	file_hash:string;
}

export default class NFTPro implements IData {
	title: string;
	description: string;
	licences: PublicKey[];
	artist:PublicKey;
	camm:PublicKey;
	lort_mint:PublicKey;
	file_hash:string;
	
	constructor(data:NFTProInput) {
		this.title = data.title.slice(0, 20);
		this.description = data.description.slice(0, 64);
		this.licences = data.licences.slice(0, 20);
		this.artist = data.artist;
		this.camm = data.camm;
		this.lort_mint = data.lort_mint;
		this.file_hash = data.file_hash.slice(0, 46);
	}

	getSize(): number {
		return this.toBuffer().length - 1;
	}

	toBuffer(): Buffer {
		const buf = new Array();
		buf.push(Buffer.from([0]))
		buf.push(Buffer.from(toByteArray(this.title, 20)));
		buf.push(Buffer.from(toByteArray(this.description, 64)));
		for(let i=0; i<20; i++)
		{
			(i < this.licences.length)
			? buf.push(Buffer.from(this.licences[i].toBytes(), 32))
			: buf.push(Buffer.from(new Uint8Array(32)));
		}
		buf.push(Buffer.from(this.artist.toBytes(), 32));
		buf.push(Buffer.from(this.camm.toBytes(), 32));
		buf.push(Buffer.from(this.lort_mint.toBytes(), 32));
		buf.push(Buffer.from(toByteArray(this.file_hash, 46)));
		return Buffer.concat(buf);
	}

	static deserialize(buffer:Buffer):NFTPro
	{
		console.log("DESERIALIZE NFT");
		const title = fromByteArray(buffer.slice(0, 20));
		const description = fromByteArray(buffer.slice(20, 84));
		const licences:PublicKey[] = [];
		for(var i=84; i<32*20+84; i+=32)
		{
			const right = new PublicKey(buffer.slice(i, i+32));
			if(right.toBase58() !== '11111111111111111111111111111111') licences.push(right);
		}
		const artist = new PublicKey(buffer.slice(i, i+32));
		i += 32;
		const camm = new PublicKey(buffer.slice(i, i+32));
		i += 32;
		const lort_mint = new PublicKey(buffer.slice(i, i+32));
		i += 32;
		const file_hash = fromByteArray(buffer.slice(i));

		return new NFTPro({
			title, description, licences, artist, camm, lort_mint, file_hash
		});
	}
}