import { IData } from '../core/IData';
import { fromByteArray, toByteArray } from '../utils/string';
import { RightProgramTypes } from '../core/enums';
import { PublicKey } from '@solana/web3.js';
import * as BufferLayout from 'buffer-layout';
import * as Layout from "../../client/programs/TokenSwap/layout";

export interface RightInput{
	title: string;
	description: string;
    salt:PublicKey;
}

export const RightLayout = BufferLayout.struct([
    BufferLayout.u8('type'),
    BufferLayout.blob(128, "title"),
    BufferLayout.blob(512, "description"),
    Layout.publicKey("salt"),
]);

export default class Right implements IData
{
    type:number = RightProgramTypes.Right;
    title:string;
    description:string;
    salt:PublicKey;
    
    constructor(data:RightInput)
    {
        this.title = data.title.slice(0, 128);
        this.description = data.description.slice(0, 512);
        this.salt = data.salt;
    }

    getSize():number
    {
        return this.toBuffer().length;
    }
    
    toBuffer(): Buffer
    {
        const buf = new Array();
		buf.push(Buffer.from([this.type]));
		buf.push(Buffer.from(toByteArray(this.title, 128)));
		buf.push(Buffer.from(toByteArray(this.description, 512)));
        buf.push(this.salt.toBuffer());
		return Buffer.concat(buf);
    }

    static deserialize(buffer:Buffer):Right
	{
        const data = RightLayout.decode(buffer);
        const title = fromByteArray(data.title);
        const description = fromByteArray(data.description);
        const salt = new PublicKey(data.salt);
		
        return new Right({
			title, description, salt
		});
	}
}