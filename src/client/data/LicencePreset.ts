import { IData } from '../core/IData';
import { fromByteArray, toByteArray } from '../utils/string';
import { RightProgramTypes } from '../core/enums';
import { PublicKey } from '@solana/web3.js';
import * as BufferLayout from 'buffer-layout';
import * as Layout from "../../client/programs/TokenSwap/layout";
import { isZeroArray } from '../utils/array';

const MAX_CATEGORIES:number = 10;
const MAX_RIGHTS:number = 14;

export interface LicencePresetInput{
	title: string;
	salt: PublicKey;
	categories: PublicKey[];
	rights: PublicKey[];
}

export const LicencePresetLayout = BufferLayout.struct([
    BufferLayout.u8('type'),
    BufferLayout.blob(40, "title"),
    Layout.publicKey("salt"),
    BufferLayout.seq(Layout.publicKey(), MAX_CATEGORIES, "categories"),
    BufferLayout.seq(Layout.publicKey(), MAX_RIGHTS, "rights"),
]);

export default class LicencePreset implements IData
{
    type:number = RightProgramTypes.LicencePreset;
    title: string;
    salt: PublicKey;
	categories: PublicKey[];
	rights: PublicKey[];
    
    constructor(data:LicencePresetInput)
    {
        this.title = data.title.slice(0, 40);
        this.salt = data.salt;
        this.categories = data.categories.slice(0, MAX_CATEGORIES);
        this.rights = data.rights.slice(0, MAX_RIGHTS);
    }

    getSize():number
    {
        return this.toBuffer().length;
    }
    
    toBuffer(): Buffer
    {
        const buf = new Array();
		buf.push(Buffer.from([this.type]));
        buf.push(Buffer.from(toByteArray(this.title, 40)));
        buf.push(this.salt.toBuffer());
        for(let i=0; i<MAX_CATEGORIES; i++)
		{
			(i < this.categories.length)
			? buf.push(this.categories[i].toBuffer())
			: buf.push(Buffer.from(new Uint8Array(32)));
		}
        for(let i=0; i<MAX_RIGHTS; i++)
		{
			(i < this.rights.length)
			? buf.push(this.rights[i].toBuffer())
			: buf.push(Buffer.from(new Uint8Array(32)));
		}
		return Buffer.concat(buf);
    }

    static deserialize(buffer:Buffer):LicencePreset
	{
        const data = LicencePresetLayout.decode(buffer);
        const title = fromByteArray(data.title);
		const salt = new PublicKey(data.salt)
        const categories:PublicKey[] = [];
        for(let i=0; i < data.categories.length; i++)
        {
            const category = data.categories[i];
            if(!isZeroArray(category, 32))
                categories.push(new PublicKey(category));
        }
        const rights:PublicKey[] = [];
        for(let i=0; i < data.rights.length; i++)
        {
            const right = data.rights[i];
            if(!isZeroArray(right, 32))
                rights.push(new PublicKey(right));
        }
        
        return new LicencePreset({
			title, salt, categories, rights
		});
	}
}