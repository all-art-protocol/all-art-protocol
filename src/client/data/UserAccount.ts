import {IData} from '../core/IData';
import {fromByteArray, toByteArray} from '../utils/string';

export interface UserAccountInput{
	first_name: string;
	last_name: string;
	country: string;
	bio: string;
	image: string;
}

export default class UserAccount implements IData
{
    first_name:string;
    last_name:string;
    country:string;
    bio:string;
    image:string;
    
    constructor(data:UserAccountInput)
    {
        this.first_name = data.first_name.slice(0, 20);
        this.last_name = data.last_name.slice(0, 20);
        this.country = data.country.slice(0, 20);
        this.bio = data.bio.slice(0, 256);
        this.image = data.image.slice(0, 46);
    }

    getSize():number
    {
        return this.toBuffer().length;
    }
    
    toBuffer(): Buffer
    {
        const buf = new Array();
		buf.push(Buffer.from(toByteArray(this.first_name, 20)));
		buf.push(Buffer.from(toByteArray(this.last_name, 20)));
		buf.push(Buffer.from(toByteArray(this.country, 20)));
		buf.push(Buffer.from(toByteArray(this.bio, 256)));
		buf.push(Buffer.from(toByteArray(this.image, 46)));
		return Buffer.concat(buf);
    }

    static deserialize(buffer:Buffer):UserAccount
	{
		const first_name = fromByteArray(buffer.slice(0, 20));
		const last_name = fromByteArray(buffer.slice(20, 40));
		const country = fromByteArray(buffer.slice(40, 60));
		const bio = fromByteArray(buffer.slice(60, 316));
		const image = fromByteArray(buffer.slice(316));

		return new UserAccount({
			first_name, last_name, country, bio, image
		});
	}
}