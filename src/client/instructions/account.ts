import { PublicKey, SystemProgram, TransactionInstruction } from '@solana/web3.js';
import { connect } from '../core/connection';
import UserAccount from '../data/UserAccount';

export async function createAccount(
    walletKey:PublicKey, 
    account:PublicKey, 
    space:number, 
    programId:PublicKey,
    cluster:string
): Promise<[TransactionInstruction, PublicKey]>
{
    const connection = await connect(cluster);
    const rent = await connection.getMinimumBalanceForRentExemption(space);
    const instruction:TransactionInstruction = SystemProgram.createAccount({
        fromPubkey: walletKey,
        newAccountPubkey: account,
        lamports: rent,
        space,
        programId,
    });
    return [instruction, account];
}

export async function createAccountWithSeed(
    walletKey:PublicKey, 
    seed:string, 
    space:number, 
    programId:PublicKey,
    cluster:string
): Promise<[TransactionInstruction, PublicKey]>
{
    const connection = await connect(cluster);
    const account = await PublicKey.createWithSeed(walletKey, seed, programId);
    const rent = await connection.getMinimumBalanceForRentExemption(space);
    const instruction:TransactionInstruction = SystemProgram.createAccountWithSeed({
        fromPubkey: walletKey,
        basePubkey: walletKey,
        lamports: rent,
        newAccountPubkey: account,
        programId,
        seed,
        space,
    });
    return [instruction, account];
}

export function createUserAccount(
    accountPubkey:PublicKey, 
    isSigner:boolean, 
    isWritable:boolean, 
    programId:PublicKey, 
    data:UserAccount
):TransactionInstruction
{
    return new TransactionInstruction({
        keys: [{ pubkey: accountPubkey, isSigner: isSigner, isWritable: isWritable }],
        programId,
        data: data.toBuffer(),
    });
}