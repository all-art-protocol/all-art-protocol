import { PublicKey, TransactionInstruction } from "@solana/web3.js";
import NFTPro from "../data/NFTPro";

export function createArtwork(accountPubkey:PublicKey, isSigner:boolean, isWritable:boolean, programId:PublicKey, data:NFTPro):TransactionInstruction
{
    return new TransactionInstruction({
        keys: [{ pubkey: accountPubkey, isSigner: isSigner, isWritable: isWritable }],
        programId,
        data: data.toBuffer(),
    });
}