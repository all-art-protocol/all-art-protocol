import { PublicKey, TransactionInstruction } from "@solana/web3.js";
import { RightProgramAction, RightProgramTypes } from "../core/enums";
import LicenceCategory from "../data/LicenceCategory";
import LicenceCategoryRights from "../data/LicenceCategoryRights";
import LicencePreset from "../data/LicencePreset";
import Right from "../data/Right";

export function createRight(accountPubkey:PublicKey, isSigner:boolean, isWritable:boolean, programId:PublicKey, data:Right):TransactionInstruction
{
    const dataBuffer = data.toBuffer();
    const actionBuffer = Buffer.from([RightProgramAction.Create]);
    return new TransactionInstruction({
        keys: [{ pubkey: accountPubkey, isSigner: isSigner, isWritable: isWritable }],
        programId,
        data: Buffer.concat([actionBuffer, dataBuffer], actionBuffer.length + dataBuffer.length)
    });
}

export function createLicenceCategory(accountPubkey:PublicKey, isSigner:boolean, isWritable:boolean, programId:PublicKey, data:LicenceCategory):TransactionInstruction
{
    const dataBuffer = data.toBuffer();
    const actionBuffer = Buffer.from([RightProgramAction.Create]);
    return new TransactionInstruction({
        keys: [{ pubkey: accountPubkey, isSigner: isSigner, isWritable: isWritable }],
        programId,
        data: Buffer.concat([actionBuffer, dataBuffer], actionBuffer.length + dataBuffer.length)
    });
}

export function deleteLicenceCategory(account:PublicKey, wallet:PublicKey, programId:PublicKey):TransactionInstruction{
    return new TransactionInstruction({
        keys: [
            { pubkey: account, isSigner: false, isWritable: true},
            { pubkey: wallet, isSigner: true, isWritable: true },
        ],
        programId,
        data: Buffer.from([RightProgramAction.Delete]),
    });
}

export function createLicenceCategoryRights(accountPubkey:PublicKey, isSigner:boolean, isWritable:boolean, programId:PublicKey, data:LicenceCategoryRights):TransactionInstruction
{
    const dataBuffer = data.toBuffer();
    const actionBuffer = Buffer.from([RightProgramAction.Create]);
    return new TransactionInstruction({
        keys: [{ pubkey: accountPubkey, isSigner: isSigner, isWritable: isWritable }],
        programId,
        data: Buffer.concat([actionBuffer, dataBuffer], actionBuffer.length + dataBuffer.length)
    });
}

export function createLicencePreset(accountPubkey:PublicKey, programId:PublicKey, data:LicencePreset):TransactionInstruction
{
    const dataBuffer = data.toBuffer();
    const actionBuffer = Buffer.from([RightProgramAction.Create]);
    return new TransactionInstruction({
        keys: [{ pubkey: accountPubkey, isSigner: false, isWritable: true }],
        programId,
        data: Buffer.concat([actionBuffer, dataBuffer], actionBuffer.length + dataBuffer.length)
    });
}