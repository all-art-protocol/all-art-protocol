import Wallet from "@project-serum/sol-wallet-adapter";
import { IWallet } from "../../core/wallet";

export default class SolletProvider extends Wallet implements IWallet {
	constructor(cluster:string)
	{
		super("https://www.sollet.io", cluster);
	}
  }