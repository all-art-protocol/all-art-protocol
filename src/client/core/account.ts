import fs from 'mz/fs';
import { AccountInfo, Keypair, PublicKey, SystemProgram, SYSVAR_RENT_PUBKEY, Transaction, TransactionInstruction } from '@solana/web3.js';
import {connect} from './connection';
import { IWallet } from './wallet';
import {createAccountWithSeed} from '../instructions/account'
import { AccountLayout, ASSOCIATED_TOKEN_PROGRAM_ID, Token, TOKEN_PROGRAM_ID } from '@solana/spl-token';
import { sendTransaction } from './transactions';
import { WALLET_PUBLICKEY_UNDEFINED } from './errors';

export async function read(pubKey:PublicKey, cluster:string):Promise<[AccountInfo<Buffer> | null, PublicKey]>
{
    const connection = await connect(cluster);
    const accountInfo = await connection.getAccountInfo(pubKey);
    return [accountInfo, pubKey];
}

// export async function create(
//     wallet:IWallet, 
//     walletKey:PublicKey, 
//     account:PublicKey, 
//     space:number, 
//     programId:PublicKey,
//     cluster:string
// ): Promise<PublicKey>
// {
//     const accountInfo = await read(account, cluster);
//     if(accountInfo !== null)
//         return account;
    
//     console.log("Creating new account ...");
//     const connection = await connect(cluster);
//     const instruction = (await createAccountWithSeed(walletKey, "bla", space, programId, cluster))[0];
//     const transaction:Transaction = new Transaction().add(instruction);
//     transaction.recentBlockhash = (await connection.getRecentBlockhash()).blockhash;
//     transaction.feePayer = walletKey;
    
//     let signed:Transaction = await wallet.signTransaction(transaction);
//     let signature = await connection.sendRawTransaction(signed.serialize());
//     await connection.confirmTransaction(signature);
//     console.log("Account creation complete!");
//     console.log("Return Account: ", account.toString());
//     return account;
// }

export async function readKeyPair(keypair_path:string): Promise<Keypair>
{
    const keypairString = await fs.readFile(keypair_path, {encoding: 'utf8'});
    const keypairBuffer = Buffer.from(JSON.parse(keypairString));
    const account:Keypair = Keypair.fromSecretKey(keypairBuffer);
    return account;
}

export function createAssociatedTokenAccountInstruction(
    tokenMintPublicKey: PublicKey, 
    ownerPublicKey: PublicKey, 
    associatedTokenAccountPublicKey: PublicKey, 
    walletKey:PublicKey
) 
{
    return new TransactionInstruction({
        programId: ASSOCIATED_TOKEN_PROGRAM_ID,
        data: Buffer.from([]),
        keys: [
            { pubkey: walletKey, isSigner: true, isWritable: true },
            { pubkey: associatedTokenAccountPublicKey, isSigner: false, isWritable: true },
            { pubkey: ownerPublicKey, isSigner: false, isWritable: false },
            { pubkey: tokenMintPublicKey, isSigner: false, isWritable: false },
            { pubkey: SystemProgram.programId, isSigner: false, isWritable: false },
            { pubkey: TOKEN_PROGRAM_ID, isSigner: false, isWritable: false },
            { pubkey: SYSVAR_RENT_PUBKEY, isSigner: false, isWritable: false },
        ]
    })    
}

export function createAccountInstruction(newAccount: PublicKey, wallet: PublicKey, lamports: number, space: number = AccountLayout.span): TransactionInstruction {
    return SystemProgram.createAccount({
        fromPubkey: wallet,
        newAccountPubkey: newAccount,
        lamports,
        space,
        programId: TOKEN_PROGRAM_ID,
    })
}

export function createTokenAccountInstruction(newAccount: PublicKey, wallet: PublicKey, tokenMint: PublicKey, owner: PublicKey, lamports: number): TransactionInstruction[] {
    const createAccountInstruction = SystemProgram.createAccount({
        fromPubkey: wallet,
        newAccountPubkey: newAccount,
        lamports,
        space: AccountLayout.span,
        programId: TOKEN_PROGRAM_ID,
    })

    const createTokenAccountInstruction = Token.createInitAccountInstruction(TOKEN_PROGRAM_ID, tokenMint, newAccount, owner)
    return [createAccountInstruction, createTokenAccountInstruction]
}

export async function createAssociatedTokenAccount(
    tokenMintPublicKey: PublicKey, 
    ownerPublicKey: PublicKey, 
    wallet: IWallet, 
    walletKey:PublicKey,
    cluster:string
) 
{
    const associatedTokenAccountPublicKey = await findAssociatedTokenAccountPublicKey(ownerPublicKey, tokenMintPublicKey)
    const instruction = createAssociatedTokenAccountInstruction(tokenMintPublicKey, ownerPublicKey, associatedTokenAccountPublicKey, walletKey);
    const connection = await connect(cluster)

    return await sendTransaction([instruction], connection, [], wallet)
}

export async function findAssociatedTokenAccountPublicKey(ownerPublicKey: PublicKey, tokenMintPublicKey: PublicKey): Promise<PublicKey> {
    return (await PublicKey.findProgramAddress([ownerPublicKey.toBuffer(), TOKEN_PROGRAM_ID.toBuffer(), tokenMintPublicKey.toBuffer()], ASSOCIATED_TOKEN_PROGRAM_ID))[0]
}

export async function createPublicKeyFromSeed(seed:Buffer[], programId:PublicKey):Promise<PublicKey>
{
    return (await PublicKey.findProgramAddress(seed, programId))[0];
}