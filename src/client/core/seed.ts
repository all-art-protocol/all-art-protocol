import md5 from 'md5';
import {sha256} from 'crypto-hash';
import { PublicKey } from '@solana/web3.js';
import { CREATE_RIGHT_PROGRAM, CREATE_USER_PROGRAM } from './consts';
import { RightProgramTypes } from './enums';

export async function createUserAccountSeed(walletKey:PublicKey):Promise<string>
{
    return await createAccountSeed(`${walletKey.toBase58()}${CREATE_USER_PROGRAM}`);
}

export async function createLicenceCategoryAccountSeed(salt:string):Promise<string>
{
    return await createAccountSeed(`${CREATE_RIGHT_PROGRAM}_${salt}`)
}

export async function createLicenceCategoryRightsAccountSeed(salt:string, category:PublicKey):Promise<string>
{
    return await createAccountSeed(`${CREATE_RIGHT_PROGRAM}_${salt}_${category.toBase58()}`)
}

export async function createLicencePresetAccountSeed(salt:string):Promise<string>
{
    return await createAccountSeed(`LicencePreset_${CREATE_RIGHT_PROGRAM}_${salt}`)
}

export async function createRightAccountSeed(salt:string):Promise<string>
{
    return await createAccountSeed(`${CREATE_RIGHT_PROGRAM}_${salt}_${RightProgramTypes.Right}`)
}

export async function createAccountSeed(paraphrase:string):Promise<string>
{
    return md5(await sha256(new Uint8Array(Buffer.from(paraphrase))));
}