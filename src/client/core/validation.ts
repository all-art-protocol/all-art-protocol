export interface IValidationResult<T>
{
    data:T,
    isValid:boolean,
    error:string,
}

export function validate<T>(
    validator:(data:T)=>boolean,
    data:T,
    errorMessage:string = "Invalid data!",
    onValid?:(result:IValidationResult<T>)=>void,
    onInValid?:(result:IValidationResult<T>)=>void,
):IValidationResult<T>{
    const result:IValidationResult<T> = {
        data:data,
        isValid:validator(data),
        error:errorMessage,
    };
    (result.isValid)
    ? (onValid) && onValid(result)
    : (onInValid) && onInValid(result);
    return result;
}