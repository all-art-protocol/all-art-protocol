import { Keypair, PublicKey, SystemProgram, TransactionInstruction } from "@solana/web3.js";
import { connect } from "./connection";

interface iCreateAccountInstructionResult {
	instruction: TransactionInstruction | null
	newAccountPubKey: PublicKey
}

export async function createAccountInstruction(
	payerAccount: Keypair, 
	seed:string, 
	space:number, 
	programId:PublicKey,
	cluster:string
): Promise<iCreateAccountInstructionResult> 
{
	const connection = await connect(cluster);
	const accountPubkey = await PublicKey.createWithSeed(payerAccount.publicKey, seed, programId);
	const accountInfo = await connection.getAccountInfo(accountPubkey);
	if(accountInfo) {
		return { instruction: null, newAccountPubKey: accountPubkey }
	}
	const rent = await connection.getMinimumBalanceForRentExemption(space);
	const instructionParams = {
			fromPubkey: payerAccount.publicKey,
			basePubkey: payerAccount.publicKey,
			seed,
			newAccountPubkey: accountPubkey,
			lamports: rent,
			space,
			programId,
	};
	console.log("...2");
	console.log("Instruction params", instructionParams);
	const instruction = SystemProgram.createAccountWithSeed(instructionParams);
	return { instruction, newAccountPubKey: accountPubkey }
}