export interface IData
{
    getSize():number;
    toBuffer():Buffer;
}