export enum RightProgramTypes{
    Right = 0,
    LicenceCategory = 1,
    LicencePreset = 2,
    LicenceCategoryRights = 3,
}

export enum RightProgramAction{
    Create = 0,
    Edit = 1,
    Delete = 2,
}

export enum ClusterID{
    MainNet = "mainnet",
    TestNet = "testnet",
    DevNet = "devnet",
    Custom = "custom",
}

export enum WalletProviders{
	Sollet = 0, 
	Phantom = 1,
}