export default interface IProgram<ResultType>
{
    run():Promise<ResultType | null>;
}