import { Connection, TransactionInstruction, Signer, Transaction } from "@solana/web3.js";
import { IWallet } from "./wallet";

export async function sendTransaction(
	instructions: TransactionInstruction[],
	connection: Connection,
	signers: Signer[],
	wallet: IWallet
) {
	const transaction = new Transaction({feePayer: wallet.publicKey}).add(...instructions)
	
	// transaction.setSigners(wallet.publicKey, ...signers.map(s => s.publicKey))	

	transaction.recentBlockhash = (await connection.getRecentBlockhash("singleGossip")).blockhash;
	
	signers.forEach((s) => {
		transaction.partialSign(s)
	})

	const signed = await wallet.signTransaction(transaction)
	if(signed)
	{
		const txid = await connection.sendRawTransaction(signed.serialize())
		await connection.confirmTransaction(txid, "singleGossip")
		return txid
	}
	return null;
}

export async function createTransaction(instructions: TransactionInstruction[],
	connection: Connection,
	signers: Signer[],
	wallet: IWallet
) {
	const transaction = new Transaction({feePayer: wallet.publicKey}).add(...instructions)
	transaction.recentBlockhash = (await connection.getRecentBlockhash("singleGossip")).blockhash;
	signers.forEach((s) => {
		transaction.partialSign(s)
	})

	return transaction
}

export async function sendMultipleTransactions(transactions: Transaction[], connection: Connection, wallet: IWallet) {
	const signed = await wallet.signAllTransactions(transactions)
	const ids:string[] = []
	
	if(signed)
	{
		for (let i = 0; i < signed.length; i++) {
			const transaction = signed[i];
			const id = await connection.sendRawTransaction(transaction.serialize())
			ids.push(id)
			await connection.confirmTransaction(id, "singleGossip")
			console.log("transaction sent, id = ", id);
		}
	}
	return ids
}