import { Keypair, PublicKey, SystemProgram, Transaction } from '@solana/web3.js';
import {sha256} from 'crypto-hash';
import md5 from 'md5';
import { CREATE_NFTPRO_PROGRAM, CREATE_RIGHT_PROGRAM } from './core/consts';
import { RIGHTS_BASE } from './data/LicencePresets';
import Licence from '../client/data/Licence';
import CryptoJS from 'crypto-js';
import ReadRightAccounts from './programs/ReadRightAccounts';
import { connect } from './core/connection';
import LicenceCategory from './data/LicenceCategory';
import LicencePreset from './data/LicencePreset';
import { TOKEN_SWAP_PROGRAM_ID } from './programs/TokenSwap';


// Generate right templates
const createRightTemplates = async ()=>{
    const wallet = new PublicKey("GyVeDwRXyE9N9628YZiLW3CNzyvSZAPFgsu4CUsUZVUV");
    const programId = new PublicKey(CREATE_RIGHT_PROGRAM);
    const rightsBase = [...RIGHTS_BASE];
    for(let i=0; i<rightsBase.length; i++)
    {
        const base = rightsBase[i];
        const seed = md5(await sha256(new Uint8Array(Buffer.from(base.paraphrase))));
        const key = (await PublicKey.createWithSeed(wallet, seed, programId)).toBase58();
        console.log(`{id:${base.id}, category:"${base.category}", title:"${base.title}", description:"${base.description}", text:"${base.text}", paraphrase:"${base.paraphrase}", seed:"${seed}", key:"${key}"},`);
    }
}
// createRightTemplates();

const createParaphrase = async ()=>{
    for(let i=0; i<100; i++)
        console.log(`${Keypair.generate().publicKey.toBase58()}_${Keypair.generate().publicKey.toBase58()}`);
}
// createParaphrase();

const testRight = async ()=>{
    const programId = new PublicKey(CREATE_RIGHT_PROGRAM);
    const seed = "b692c7b02199ca2830a1fd4ec36347ff";
    const key = (await PublicKey.createWithSeed(programId, seed, programId)).toBase58();
    console.log(key);
}
// testRight();

const tokenSwapAuthorityFindProgramAddress = async()=>
{
	const [key, num] = await PublicKey.findProgramAddress([new TextEncoder().encode("artcoins")], TOKEN_SWAP_PROGRAM_ID);
	console.log("Find TOKEN SWAP Program Address Key", key.toBase58());
	console.log("Find TOKEN SWAP Program Address Num", num);
}
tokenSwapAuthorityFindProgramAddress();

const nftProAuthorityFindProgramAddress = async()=>
{
	const [key, num] = await PublicKey.findProgramAddress([new TextEncoder().encode("artcoins")], new PublicKey(CREATE_NFTPRO_PROGRAM));
	console.log("Find NFTPro Authority Key", key.toBase58());
	console.log("Find NFTPro Authority Num", num);
}
nftProAuthorityFindProgramAddress();

// Encryption
const encryptedText =  CryptoJS.AES.encrypt("Hello world", "mykey");
const decryptedBytes = CryptoJS.AES.decrypt(encryptedText, "mykey");
// console.log("Decrypted text: ", decryptedBytes.toString(CryptoJS.enc.Utf8));

// LicenceCategory
const lc = new LicenceCategory({
    title:"Some title",
    description: "Some Description",
    parent:new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh"),
    order:300,
    salt:new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh")
});
const lcBuffer = lc.toBuffer();
const lcDeserialized = LicenceCategory.deserialize(lcBuffer);
// console.log(lcDeserialized);

// Licence Preset
const lp = new LicencePreset({
    title:"Licence Preset Name",
    salt: new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh"),
    categories:[new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh"), new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh")],
    rights:[new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh"), new PublicKey("GqTFJAPNbjiD8GqfNqi3F9qAMTH2aip1cUwdPQJSjHHh")],
});
const lpBuffer = lp.toBuffer();
const lpDeserialized = LicencePreset.deserialize(lpBuffer);
// console.log(lpDeserialized);
