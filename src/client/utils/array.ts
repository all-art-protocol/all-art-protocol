export function isZeroArray(data: [], length: number) 
{
	for (let i = 0; i < length; i++) 
    {
		if(data[i]) 
			return false;
	}
	return true;
}