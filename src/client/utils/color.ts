const hexMap = {
	index: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"],
	value: {
		0: 0,
		1: 1,
		2: 2,
		3: 3,
		4: 4,
		5: 5,
		6: 6,
		7: 7,
		8: 8,
		9: 9,
		A: 10,
		B: 11,
		C: 12,
		D: 13,
		E: 14,
		F: 15,
	}
}

export const randomColor = (): string => {
	let color = '#';
	for (let i = 0; i < 6; i++)
		color += hexMap.index[Math.floor(Math.random() * hexMap.index.length)];
	return color;
}

export const randomShade = (color: string, offset:number = 20) => {
	const colors = [
		parseInt(color.substr(1, 2), 16),
		parseInt(color.substr(3, 2), 16),
		parseInt(color.substr(5, 2), 16)
	];
	
	colors[0] = (colors[0] + offset < 255) ? colors[0] + offset : (colors[0] - offset < 0) ? 0 : colors[0] - offset;
	colors[1] = (colors[1] + offset < 255) ? colors[1] + offset : (colors[1] - offset < 0) ? 0 : colors[1] - offset;
	colors[2] = (colors[2] + offset < 255) ? colors[2] + offset : (colors[2] - offset < 0) ? 0 : colors[2] - offset;
	
	return `#${colors[0].toString(16)}${colors[1].toString(16)}${colors[2].toString(16)}`;
}
