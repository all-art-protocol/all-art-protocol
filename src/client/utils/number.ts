export function uint8ToBuffer(int:number):Buffer
{
    const b:Buffer = Buffer.alloc(1);
    b.writeUInt8(int, 0);
    return b;
}
export function uint16ToBuffer(int:number):Buffer
{
    const b:Buffer = Buffer.alloc(2);
    b.writeUInt16LE(int, 0);
    return b;
}
export function uint32ToBuffer(int:number):Buffer
{
    const b:Buffer = Buffer.alloc(4);
    b.writeUInt32LE(int, 0);
    return b;
}