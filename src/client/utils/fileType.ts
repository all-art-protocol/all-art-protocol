import { fromByteArray } from "./string";

export function getFileTypeFromBuffer(buffer:Buffer) : string | undefined
{
    if(buffer && buffer.length > 0)
    {
        switch(true)
        {
            case isGlb(buffer):
                return 'glTF';
            case isPNG(buffer):
                return 'PNG';
            case isJPG(buffer):
                return 'jpg';
            default:
                return undefined;
        }
    }
}

const isGlb = (buffer:Buffer) => {
    if(fromByteArray(buffer.slice(0, 4)) === 'glTF')
        return true;
    return false;
}
const isPNG = (buffer:Buffer) => {
    if(buffer[0] === 137 && buffer[1] === 80 && buffer[2] === 78 && buffer[3] === 71 && buffer[4] === 13 && buffer[5] === 10 && buffer[6] === 26 && buffer[7] === 10)
        return true;
    return false;
}
const isJPG = (buffer:Buffer) => {
    if(buffer[0] === 255 && buffer[1] === 216)
        return true;
    return false;
}