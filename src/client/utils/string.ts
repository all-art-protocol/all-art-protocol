export function toByteArray(value:string, length: number):Uint8Array
{
    const result:Uint8Array = new Uint8Array(length);
    for (let i = 0; i < value.length; i++) {
        result[i] = value[i].charCodeAt(0);   
    }
    return result;
}

export function fromByteArray(value:Uint8Array):string
{
    // return String.fromCharCode(...value);
    let result:string = "";
    for(let i=0; i<value.length; i++)
    {
        if(value[i] !== 0)
        {
            result += String.fromCharCode(value[i]);
        }
    }
    return result.trim();
}

export function extractString(value: string) {
    return value.split('\x00')[0]
}