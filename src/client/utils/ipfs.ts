import {create , CID} from 'ipfs-http-client';
import { IPFS } from 'ipfs-core-types'

let ipfs:any;
export async function getIPFS():Promise<IPFS>
{
    console.log("IPFS: ", ipfs);
    if(!ipfs)
    {
        ipfs = create({
            url:'https://ipfs.all.art',
        });
    }
    return ipfs;
}

export async function postFile(image:Buffer):Promise<CID>
{
    const ipfs = await getIPFS();
    const { cid } = await ipfs.add(image);
    return cid;
}

export async function getFile(cid:CID | string):Promise<string>
{
    const ipfs = await getIPFS();
    const chunks:Uint8Array[] = [];
    let fileLength:number = 0;
    for await (const chunk of ipfs.cat(cid))
    {
        fileLength += chunk.length;
        chunks.push(chunk);
    }
    const file:Uint8Array = new Uint8Array(fileLength);
    let offset = 0;
    for(let i=0; i<chunks.length; i++)
    {
        file.set(chunks[i], offset);
        offset += chunks[i].length;
    }
    return await arrayBufferToBase64(Buffer.from(file));
}

export async function arrayBufferToBase64( buffer:Buffer ):Promise<string> {
    return new Promise((resolve, reject)=>{
        const blob = new Blob([buffer])
        const reader = new FileReader();
        reader.onload = function(event){
            if(event && event.target)
            {
                if(typeof event.target.result === 'string')
                    resolve(event.target.result);
                else
                    reject("Unable to convert");
            }
        };
        reader.readAsDataURL(blob);
    });
}