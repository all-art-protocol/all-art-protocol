import { Keypair, PublicKey, SystemProgram, Transaction, TransactionInstruction } from "@solana/web3.js";
import { AccountLayout, MintLayout, Token, TOKEN_PROGRAM_ID } from "@solana/spl-token"
import { createTokenAccountInstruction, createAccountInstruction } from "../core/account";
import { connect } from "../core/connection";
import { CREATE_NFTPRO_PROGRAM } from "../core/consts";
import IProgram from "../core/IProgram";
import { IWallet } from "../core/wallet";
import NFTPro from "../data/NFTPro";
import { createArtwork } from "../instructions/artwork";
import { CurveType, TokenSwap, TokenSwapLayout, TOKEN_SWAP_PROGRAM_ID } from "./TokenSwap";
import { createTransaction, sendMultipleTransactions, sendTransaction } from "../core/transactions";
import IPFS from 'ipfs-core';
import Licence from "../data/Licence";
import LicenceLort from "../data/LicenceLort";

const TRADING_FEE_NUMERATOR = 0;
const TRADING_FEE_DENOMINATOR = 0;
const OWNER_TRADING_FEE_NUMERATOR = 0;
const OWNER_TRADING_FEE_DENOMINATOR = 0;
const OWNER_WITHDRAW_FEE_NUMERATOR =  0;
const OWNER_WITHDRAW_FEE_DENOMINATOR = 0;
const HOST_FEE_NUMERATOR = 0;
const HOST_FEE_DENOMINATOR = 0;


export interface LicenceDataInput{
	title:string;
    rights:PublicKey[];
    quantity:number;
    lorts:number;
	file_hash:string;
}

export interface NFTProInput{
	title:string;
	description:string;
	licences:LicenceDataInput[];
	artist:PublicKey;
	preview:IPFS.CID;
	price:number;
}

export default class CreateNFTPRO implements IProgram<PublicKey[] | null> 
{
	input: NFTProInput;
	wallet: IWallet;
	walletKey: PublicKey;
	cluster:string;

	constructor(wallet:IWallet, walletKey:PublicKey, input:NFTProInput, cluster:string)
	{
		this.input = input;
		this.wallet = wallet;
		this.walletKey = walletKey;
		this.cluster = cluster;
	}

	async run(): Promise<PublicKey[] | null> 
	{
		const amount = this.input.price
		const connection = await connect(this.cluster);

		const lamports = await Token.getMinBalanceRentForExemptAccount(connection)
		const mintLamports = await connection.getMinimumBalanceForRentExemption(MintLayout.span, "singleGossip")
		const tokenSwapLamports = await connection.getMinimumBalanceForRentExemption(TokenSwapLayout.span, "singleGossip") 

		let transactions = new Array<Transaction>();
		let instructions: TransactionInstruction[] = []

		const NFT_PROGRAM_ID = new PublicKey(CREATE_NFTPRO_PROGRAM)
		const [authority, nonce] = await PublicKey.findProgramAddress(
			[new TextEncoder().encode("artcoins")],
			TOKEN_SWAP_PROGRAM_ID,
		);
		console.log("authority", authority.toString());
		
		const nftproAccount = Keypair.generate();
		const lort = Keypair.generate();
		const programArtCoinAccount = Keypair.generate();
		const lortArtistAccount = Keypair.generate();
		const programLortAccount = Keypair.generate();
		const poolToken = Keypair.generate();
		const poolTokenAccount = Keypair.generate();
		const feeAccount = Keypair.generate();
		const tokenSwapAccount = Keypair.generate();

		const licences = this.input.licences.map(item=>Keypair.generate());
		const data = new NFTPro({
			title:this.input.title,
			description:this.input.description,
			licences:licences.map(item=>item.publicKey),
			artist:this.input.artist,
			camm:tokenSwapAccount.publicKey,
			lort_mint:lort.publicKey,
			file_hash:this.input.preview.toString()
		});
		
		//public key for Art Coin Token
		const artCoinKey = new PublicKey(
			(process.env['REACT_APP_ART_COINT_TOKEN']) 
			? process.env['REACT_APP_ART_COINT_TOKEN'] 
			: "3dA6ceUAW7wiubUojFgaoF3Ayyy7mehAkaZJLXSFQhWo"
		);
		//2tncBoQKr5MM3rcBY8eyh46k6AydhRzTG4qQZQ9iyUse -> Authority pk

		console.log("wallet = ", this.walletKey.toString());
		
		// lort mint account
		console.log("create lort account", lort.publicKey.toString());
		instructions.push(createAccountInstruction(lort.publicKey, this.walletKey, mintLamports, MintLayout.span))
		
		// account na kojem ce stajati art coini u poolu
		console.log("programArtCoinAccount", programArtCoinAccount.publicKey.toString());
		instructions.push(...createTokenAccountInstruction(programArtCoinAccount.publicKey, this.walletKey, artCoinKey, authority , lamports))
		
		//inicijalizacija za lort mint		
		console.log("init lort mint");
		const initLortMintInstruction = Token.createInitMintInstruction(TOKEN_PROGRAM_ID, lort.publicKey, 2, authority, null)
		instructions.push(initLortMintInstruction)

		if(instructions.length > 0) {
			try {
				transactions.push(await createTransaction(instructions, connection, [lort, programArtCoinAccount], this.wallet))
				instructions = []
			} catch (error) {
				console.log(error);
			}
		}


		
		//account na kojem ce stajati lortovi za artista
		console.log("artist lort account", lortArtistAccount.publicKey.toString());
		instructions.push(...createTokenAccountInstruction(lortArtistAccount.publicKey, this.walletKey, lort.publicKey, this.walletKey , lamports))

		//account na kojem ce stajati lortovi u poolu
		console.log("program lort account", programLortAccount.publicKey.toString());
		instructions.push(...createTokenAccountInstruction(programLortAccount.publicKey, this.walletKey, lort.publicKey, authority , lamports))

		//account i inicijalizacija za pool token (potencijalno ce se brisati iz strukture)
		console.log("pool token", poolToken.publicKey.toString());
		instructions.push(createAccountInstruction(poolToken.publicKey, this.walletKey, mintLamports, MintLayout.span))
		instructions.push(Token.createInitMintInstruction(TOKEN_PROGRAM_ID, poolToken.publicKey, 0, authority, null))
		
		//account za NFTPRO
		instructions.push(SystemProgram.createAccount({
			fromPubkey: this.walletKey,
			newAccountPubkey: nftproAccount.publicKey,
			lamports,
			space: data.getSize(),
			programId: NFT_PROGRAM_ID,
		}))
		
		if(instructions.length > 0) {
			try {
				// const sign =  await sendTransaction(instructions, connection, [programArtCoinAccount, lort, programLortAccount], this.wallet)
				transactions.push(await createTransaction(instructions, connection, [lortArtistAccount, programLortAccount, poolToken, nftproAccount], this.wallet))
				instructions = []
			} catch (error) {
				console.log(error);
			}
		}


		//account na kojem ce stajati pool tokeni
		instructions.push(...createTokenAccountInstruction(poolTokenAccount.publicKey, this.walletKey, poolToken.publicKey, this.walletKey, lamports))
		//fee account
		instructions.push(...createTokenAccountInstruction(feeAccount.publicKey, this.walletKey, poolToken.publicKey, this.walletKey, lamports))

		
		//account za TokenSwap (CAMM)
		console.log("CAMM ACCOUNT = ", tokenSwapAccount.publicKey.toString());
		instructions.push(SystemProgram.createAccount({
			fromPubkey: this.walletKey,
			newAccountPubkey: tokenSwapAccount.publicKey,
			lamports: tokenSwapLamports,
			space: TokenSwapLayout.span,
			programId: TOKEN_SWAP_PROGRAM_ID,
		}))
		//inicijalizacija TokenSwap-a (CAMM)
		instructions.push(TokenSwap.createInitSwapInstruction(tokenSwapAccount, authority, lort.publicKey, artCoinKey, lortArtistAccount.publicKey, programLortAccount.publicKey, programArtCoinAccount.publicKey, poolToken.publicKey, feeAccount.publicKey, poolTokenAccount.publicKey, nftproAccount.publicKey, TOKEN_PROGRAM_ID, TOKEN_SWAP_PROGRAM_ID, nonce, amount, TRADING_FEE_NUMERATOR, TRADING_FEE_DENOMINATOR, OWNER_TRADING_FEE_NUMERATOR, OWNER_TRADING_FEE_DENOMINATOR, OWNER_WITHDRAW_FEE_NUMERATOR, OWNER_WITHDRAW_FEE_DENOMINATOR, HOST_FEE_NUMERATOR, HOST_FEE_DENOMINATOR, CurveType.ConstantProduct ))

		if(instructions.length > 0) {
			try {
				transactions.push(await createTransaction(instructions, connection, [poolTokenAccount, feeAccount, tokenSwapAccount], this.wallet))
				instructions = []
			} catch (error) {
				console.log(error);
			}
		}

		console.log("CREATE LICENCE");
		// Create Licences
		for(let i=0; i<licences.length; i++)
		{
			const licenceMintAccount = Keypair.generate();
			const lortLicenceAccount = Keypair.generate();
			const lortLicenceAccountData = new LicenceLort(lort.publicKey);
			
			const licenceInputData = this.input.licences[i];
			const licenceData = new Licence({
				title:licenceInputData.title,
				licence_mint:licenceMintAccount.publicKey,
				licence_rights:licenceInputData.rights,
				lort_licence_account:lortLicenceAccount.publicKey,
				quantity:licenceInputData.quantity,
				lorts:licenceInputData.lorts,
				file_hash:licenceInputData.file_hash,
				nft_pro: nftproAccount.publicKey,
			});

			
			
			// transactions.push(new Transaction().add(
			// 	createAccountInstruction(licenceMintAccount.publicKey, this.wallet.publicKey, lamports, space),
			// ));
			
			
			console.log(`adding licence: ${licences[i].publicKey}`);
			console.log(`adding licence lort account : ${lortLicenceAccount.publicKey}`);

			const [nftProAuthority] = await PublicKey.findProgramAddress([new TextEncoder().encode("artcoins")], NFT_PROGRAM_ID)

			// create licence and lort licence accounts
			transactions.push(
				await createTransaction(
					[
						SystemProgram.createAccount({
							fromPubkey: this.walletKey,
							newAccountPubkey: licenceMintAccount.publicKey,
							lamports: mintLamports,
							programId: TOKEN_PROGRAM_ID,
							space: MintLayout.span
						}),
						Token.createInitMintInstruction(
							TOKEN_PROGRAM_ID, 
							licenceMintAccount.publicKey, 
							2, 
							nftProAuthority, 
							null
						),
						SystemProgram.createAccount({
							fromPubkey: this.walletKey,
							newAccountPubkey: licences[i].publicKey,
							lamports: await connection.getMinimumBalanceForRentExemption(licenceData.getSize()),
							space: licenceData.getSize(),
							programId: NFT_PROGRAM_ID,
						}),
						...createTokenAccountInstruction(lortLicenceAccount.publicKey, this.walletKey, lort.publicKey, NFT_PROGRAM_ID, lamports)
						// SystemProgram.createAccount({
						// 	fromPubkey: this.wallet.publicKey,
						// 	newAccountPubkey: lortLicenceAccount.publicKey,
						// 	lamports,
						// 	space: AccountLayout.span,
						// 	programId: TOKEN_PROGRAM_ID,
						// })
					],
					connection, [licences[i], lortLicenceAccount, licenceMintAccount], this.wallet
				)
			);
			// push licence and lort licence accounts data
			transactions.push(
				await createTransaction(
					[
						new TransactionInstruction({
							keys: [{ pubkey: licences[i].publicKey, isSigner: true, isWritable: true }],
							programId: NFT_PROGRAM_ID,
							data: licenceData.toBuffer(),
						}),
						// new TransactionInstruction({
						// 	keys: [{ pubkey: lortLicenceAccount.publicKey, isSigner: true, isWritable: true }],
						// 	programId: NFT_PROGRAM_ID,
						// 	data: lortLicenceAccountData.toBuffer(),
						// })
					],
					connection, [licences[i]], this.wallet
				)
			);
		}
		console.log(`adding nftpro: ${nftproAccount.publicKey}`);
		transactions.push(
			await createTransaction(
				[createArtwork(nftproAccount.publicKey, true, true, NFT_PROGRAM_ID, data)],
				connection, [nftproAccount], this.wallet
			)
		);
		
		// submit transactions
		// try {
			await sendMultipleTransactions(transactions, connection, this.wallet)
		// } catch (error) {
		// 	console.log("Unable to create an NFT");
		// 	console.log(error);	
		// }

		return [tokenSwapAccount.publicKey, lort.publicKey, nftproAccount.publicKey]
	}
}