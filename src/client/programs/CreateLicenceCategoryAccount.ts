import { PublicKey, Transaction, TransactionSignature } from "@solana/web3.js";
import { connect } from "../core/connection";
import { CREATE_RIGHT_PROGRAM } from "../core/consts";
import IProgram from "../core/IProgram";
import { createLicenceCategoryAccountSeed, createLicenceCategoryRightsAccountSeed } from "../core/seed";
import { IWallet } from "../core/wallet";
import LicenceCategory from "../data/LicenceCategory";
import LicenceCategoryRights from "../data/LicenceCategoryRights";
import { createAccountWithSeed } from "../instructions/account";
import { createLicenceCategory, createLicenceCategoryRights } from "../instructions/rights";

export default class CreateLicenceCategoryAccount implements IProgram<[TransactionSignature[], PublicKey] | null> 
{
	category: LicenceCategory;
	rights: PublicKey[];
	wallet: IWallet;
	walletKey: PublicKey;
	cluster: string;

	constructor(wallet: IWallet, walletKey:PublicKey, category: LicenceCategory, rights: PublicKey[], cluster:string) {
		this.category = category;
		this.rights = rights;
		this.wallet = wallet;
		this.walletKey = walletKey;
		this.cluster = cluster;
	}

	async run(): Promise<[TransactionSignature[], PublicKey] | null> 
	{
		const connection = await connect(this.cluster);
		const transactions:Transaction[] = [];
		const programId = new PublicKey(CREATE_RIGHT_PROGRAM);
		const categorySeed = await createLicenceCategoryAccountSeed(this.category.salt.toBase58());
		let { blockhash } = await connection.getRecentBlockhash();
        const [categoryAccountInstruction, categoryAccount] = await createAccountWithSeed(this.walletKey, categorySeed, this.category.getSize(), programId, this.cluster);
		const rightsSeed = await createLicenceCategoryRightsAccountSeed(this.category.salt.toBase58(), categoryAccount);
		const rightsData = new LicenceCategoryRights({
			category:categoryAccount,
			rights:this.rights,
			salt:this.category.salt
		});
		const [rightsAccountInstruction, rightsAccount] = await createAccountWithSeed(this.walletKey, rightsSeed, rightsData.getSize(), programId, this.cluster);
		
		transactions.push(new Transaction().add(
			categoryAccountInstruction,
			createLicenceCategory(categoryAccount, false, true, programId, this.category)
		));
		transactions.push(new Transaction().add(
			rightsAccountInstruction,
			createLicenceCategoryRights(rightsAccount, false, true, programId, rightsData)
		));
		transactions.forEach((transaction)=>{
			transaction.recentBlockhash = blockhash;
			transaction.feePayer = this.walletKey;
		});
		console.log("SIGN TRANSACTION");
		const signatures:string[] = [];
		const signedTransactions:Transaction[] | null = await this.wallet.signAllTransactions(transactions);
		signedTransactions?.forEach(async (signedTransaction)=>{
			const signature = await connection.sendRawTransaction(signedTransaction.serialize());
			signatures.push(signature);
			const result = await connection.confirmTransaction(signature, "singleGossip");
			console.log("Create Licence Category Account Confirmation:", result);
		});
		console.log("SIGNATURES", signatures);

            
        return [signatures, categoryAccount];
	}
}