import IProgram from "../core/IProgram";
import { PublicKey } from "@solana/web3.js";
import { connect } from "../core/connection";
import { RightProgramTypes } from '../core/enums';
import bs58 from 'bs58';
import LicencePreset from "../data/LicencePreset";

export default class ReadLicencePresetAccounts implements IProgram<{presets:Array<LicencePreset>, keys:Array<PublicKey>}>
{
    programId:PublicKey;
    cluster:string;
    
    constructor(programId:PublicKey, cluster:string)
    {
        this.programId = programId;
        this.cluster = cluster;
    }

    async run(): Promise<{presets:Array<LicencePreset>, keys:Array<PublicKey>}>
    {
        const presets = new Array<LicencePreset>();
        const keys = new Array<PublicKey>();
        try
        {
            const connection = await connect(this.cluster);
            const accounts = await connection.getProgramAccounts(this.programId, {
                filters:[
                    {
                        memcmp:{
                            offset:0,
                            bytes: bs58.encode(Buffer.from([RightProgramTypes.LicencePreset]))
                        }
                    }
                ]
            });
            for(let i=0; i<accounts.length; i++)
            {
                presets.push(LicencePreset.deserialize(accounts[i].account.data));
                keys.push(accounts[i].pubkey);
            }
        }
        catch(err){console.log("Read account faild!", err)}
        return {presets: presets, keys};
    }
    
}