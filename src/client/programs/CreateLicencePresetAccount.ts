import { PublicKey, Transaction, TransactionSignature } from "@solana/web3.js";
import { connect } from "../core/connection";
import { CREATE_RIGHT_PROGRAM } from "../core/consts";
import IProgram from "../core/IProgram";
import { createLicencePresetAccountSeed } from "../core/seed";
import { IWallet } from "../core/wallet";
import LicencePreset from "../data/LicencePreset";
import { createAccountWithSeed } from "../instructions/account";
import { createLicencePreset } from "../instructions/rights";

export default class CreateLicencePresetAccount implements IProgram<[TransactionSignature, PublicKey] | null> 
{
	preset: LicencePreset;
	wallet: IWallet;
	walletKey: PublicKey;
	cluster: string;

	constructor(wallet: IWallet, walletKey:PublicKey, preset: LicencePreset, cluster:string) {
		this.preset = preset;
		this.wallet = wallet;
		this.walletKey = walletKey;
		this.cluster = cluster;
	}

	async run(): Promise<[TransactionSignature, PublicKey] | null> 
	{
		const connection = await connect(this.cluster);
		const programId = new PublicKey(CREATE_RIGHT_PROGRAM);
		const presetSeed = await createLicencePresetAccountSeed(this.preset.salt.toBase58());
		let { blockhash } = await connection.getRecentBlockhash();
        const [presetAccountInstruction, presetAccount] = await createAccountWithSeed(this.walletKey, presetSeed, this.preset.getSize(), programId, this.cluster);
		
		const transaction:Transaction = new Transaction().add(
			presetAccountInstruction,
			createLicencePreset(presetAccount, programId, this.preset)
		);
		transaction.recentBlockhash = blockhash;
		transaction.feePayer = this.walletKey;
		
		const signatures:string[] = [];
		const signedTransaction:Transaction | null = await this.wallet.signTransaction(transaction);
		if(signedTransaction)
		{
			const signature = await connection.sendRawTransaction(signedTransaction.serialize());
			const result = await connection.confirmTransaction(signature, "singleGossip");
			console.log("Create Licence Category Account Confirmation:", result);
			console.log("SIGNATURES", signatures);
			return [signature, presetAccount];
		}
		return null;
	}
}