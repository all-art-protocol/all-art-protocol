import { AccountLayout, ASSOCIATED_TOKEN_PROGRAM_ID, MintLayout, Token, TOKEN_PROGRAM_ID, u64 } from "@solana/spl-token"
import { connect } from "../core/connection";
import IProgram from "../core/IProgram";
import { IWallet } from "../core/wallet";
import { CurveType, TokenSwap, TokenSwapLayout, TOKEN_SWAP_PROGRAM_ID } from "./TokenSwap";
import { createTransaction, sendMultipleTransactions, sendTransaction } from "../core/transactions";
import { Keypair, PublicKey, SystemProgram, Transaction, TransactionInstruction } from "@solana/web3.js";
import { createTokenAccountInstruction } from "../core/account";
import BN from "bn.js";

interface iSwap {
	amount: number;
	tokenSwap: TokenSwap;
	isBuy: boolean;
}

export default class SwapNFTPRO implements IProgram<null> 
{
	data: iSwap;
	wallet: IWallet;
	walletKey: PublicKey;
	cluster:string;

	constructor(wallet:IWallet, walletKey:PublicKey, data:iSwap, cluster:string)
	{
			this.data = data;
			this.wallet = wallet;
			this.walletKey = walletKey;
			this.cluster = cluster;
	}

	async run(): Promise<null> {
		console.log("lort mint", this.data.tokenSwap.mintA.toString());
		console.log("art coin mint", this.data.tokenSwap.mintB.toString());
		if(this.data.isBuy) {
			this.buyLorts()
		} else {
			this.sellLorts()
		}
		
		return null
	}

	async buyLorts() 
	{
		const instructions:TransactionInstruction[] = []
		const transactions:Transaction[] = []
		const connection = await connect(this.cluster)
		const lamports = await Token.getMinBalanceRentForExemptAccount(connection)
		
		const sourceAccount = await this.getUserTokenAccount(this.data.tokenSwap.mintB)
		if(!sourceAccount) throw new Error("You dont have art coin account");
		console.log("src acc", sourceAccount.toString());
		let destinationAccount = await this.getUserTokenAccount(this.data.tokenSwap.mintA)
		if(!destinationAccount) {
			const acc = Keypair.generate()
			const accInstr = createTokenAccountInstruction(acc.publicKey, this.walletKey, this.data.tokenSwap.mintA, this.walletKey, lamports)
			transactions.push(await createTransaction([...accInstr], connection, [acc], this.wallet))
			destinationAccount = acc.publicKey
		}
	
		console.log("dest acc", destinationAccount.toString())

		instructions.push(TokenSwap.swapInstruction(
			this.data.tokenSwap.tokenSwap, 
			this.data.tokenSwap.authority, 
			this.walletKey, 
			sourceAccount, //source_info
			this.data.tokenSwap.tokenAccountB, //swap_source_info
			this.data.tokenSwap.tokenAccountA, //swap_desination_info
			destinationAccount,  //destination_info
			this.data.tokenSwap.poolToken, 
			this.data.tokenSwap.feeAccount, 
			null, 
			this.data.tokenSwap.swapProgramId, 
			this.data.tokenSwap.tokenProgramId, 
			this.data.amount, 0
		))

		transactions.push(await createTransaction(instructions, connection, [], this.wallet))

		try {
			await sendMultipleTransactions(transactions, connection, this.wallet)
		} catch(error) {
			console.log(error);
		}
		return null
	}
	async sellLorts() 
	{
		const instructions:TransactionInstruction[] = []
		const signers:Keypair[] = []
		const connection = await connect(this.cluster)
		const lamports = await Token.getMinBalanceRentForExemptAccount(connection)
		const sourceAccount = await this.getUserTokenAccount(this.data.tokenSwap.mintA)
		if(!sourceAccount) throw new Error("You dont have art coin account");

		console.log("src acc", sourceAccount.toString());
		

		let destinationAccount = await this.getUserTokenAccount(this.data.tokenSwap.mintB)
		if(!destinationAccount) {
			const acc = Keypair.generate()
			destinationAccount = acc.publicKey
			instructions.push(...createTokenAccountInstruction(acc.publicKey, this.walletKey, this.data.tokenSwap.mintB, this.walletKey, lamports))
			signers.push(acc)
		}

		console.log("dest acc", destinationAccount.toString());
		

		instructions.push(TokenSwap.swapInstruction(
			this.data.tokenSwap.tokenSwap, 
			this.data.tokenSwap.authority, 
			this.walletKey, 
			sourceAccount, //source_info
			this.data.tokenSwap.tokenAccountA, //swap_source_info
			this.data.tokenSwap.tokenAccountB, //swap_desination_info
			destinationAccount,  //destination_info
			this.data.tokenSwap.poolToken, 
			this.data.tokenSwap.feeAccount, null, 
			this.data.tokenSwap.swapProgramId, 
			this.data.tokenSwap.tokenProgramId, 
			this.data.amount, 0
		))

		const transaction = await createTransaction(instructions, connection, signers, this.wallet)

		try {
			await sendMultipleTransactions([transaction], connection, this.wallet)
		} catch(error) {
			console.log(error);
		}
		return null
	}

	async getUserTokenAccount(mint: PublicKey): Promise<PublicKey | null> 
	{
		const connection = await connect(this.cluster)
		const tokens = await connection.getProgramAccounts(TOKEN_PROGRAM_ID, {
			filters: [
				{
					memcmp: {
						offset: AccountLayout.offsetOf('owner'),
						bytes: this.walletKey.toBase58(),
					}
				},
				{
					memcmp: {
						offset: AccountLayout.offsetOf("mint"),
						bytes: mint.toBase58()
					}
				},
				{
					dataSize: AccountLayout.span,
				}
			]
		})
		console.log(tokens);
		
		for (let i = 0; i < tokens.length; i++) {
			const t = tokens[i];
			const decoded = AccountLayout.decode(t.account.data)
			const num = u64.fromBuffer(decoded.amount)
			if(num.gt(new BN(0))) {
				return t.pubkey
			}
		}

		return null
	}
}