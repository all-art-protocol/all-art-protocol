import IProgram from "../core/IProgram";
import { PublicKey } from "@solana/web3.js";
import { connect } from "../core/connection";
import LicenceCategory from "../data/LicenceCategory";
import { RightProgramTypes } from '../core/enums';
import bs58 from 'bs58';
import LicenceCategoryRights from "../data/LicenceCategoryRights";
import {Layout} from 'buffer-layout';
import { fromByteArray } from "../utils/string";

export default class ReadLicenceCategoryRightsAccounts implements IProgram<{pubkey:PublicKey, data:LicenceCategoryRights} | null>
{
    programId:PublicKey;
    cluster:string;
    category:PublicKey;
    
    constructor(programId:PublicKey, cluster:string, category:PublicKey)
    {
        this.programId = programId;
        this.cluster = cluster;
        this.category = category;
    }

    async run(): Promise<{pubkey:PublicKey, data:LicenceCategoryRights} | null>
    {
        try
        {
            const t = new LicenceCategoryRights({category:this.category, salt:this.category, rights:[]});
            console.log("SIZE", t.getSize());
            console.log("CATEGORY", this.category.toBase58());
            console.log("b58", bs58.encode(Buffer.from([RightProgramTypes.LicenceCategoryRights])));
            const connection = await connect(this.cluster);
            const accounts = await connection.getProgramAccounts(this.programId, {
                filters:[
                    {
                        memcmp:{
                            offset:0,
                            bytes: bs58.encode(Buffer.from([RightProgramTypes.LicenceCategoryRights]))
                        }
                    },
                    {
                        memcmp:{
                            offset:1,
                            bytes: bs58.encode(this.category.toBuffer())
                        }
                    },
                ]
            });
            if(accounts && accounts.length > 0)
            {
                return {
                    pubkey:accounts[0].pubkey, 
                    data:LicenceCategoryRights.deserialize(accounts[0].account.data)
                };
            }
        }
        catch(err){console.log("Read account faild!", err)}
        
        return null;
    }
    
}