import { Keypair, PublicKey, SystemProgram, Transaction, TransactionInstruction, TransactionSignature } from "@solana/web3.js";
import { read } from "../core/account";
import { connect } from "../core/connection";
import { CREATE_RIGHT_PROGRAM } from "../core/consts";
import IProgram from "../core/IProgram";
import { IWallet } from "../core/wallet";
import { deleteLicenceCategory } from "../instructions/rights";

export default class DeleteAccount implements IProgram<[TransactionSignature, PublicKey] | null> 
{
	account: PublicKey;
	seed: string;
	wallet: IWallet;
	walletKey: PublicKey;
	cluster: string;

	constructor(wallet: IWallet, walletKey:PublicKey, account: PublicKey, seed:string, cluster:string) {
		this.account = account;
		this.seed = seed;
		this.wallet = wallet;
		this.walletKey = walletKey;
		this.cluster = cluster;
	}

	async run(): Promise<[TransactionSignature, PublicKey] | null> 
	{
		const connection = await connect(this.cluster);
        const balance = await connection.getBalance(this.account);
		let { blockhash } = await connection.getRecentBlockhash();
        const eraseData:TransactionInstruction = deleteLicenceCategory(this.account, this.walletKey, new PublicKey(CREATE_RIGHT_PROGRAM));
		const walletAccount = await read(this.walletKey, this.cluster);
		console.log("WALLET ACCOUNT", walletAccount);
		const transferLamports:TransactionInstruction = SystemProgram.transfer({
            basePubkey:this.walletKey,
			fromPubkey:this.account,
            lamports:balance,
			programId: new PublicKey(CREATE_RIGHT_PROGRAM),
			seed:this.seed,
            toPubkey:this.walletKey
        });
		console.log(eraseData);
		// const transaction = new Transaction().add(transferLamports);
		// const transaction = new Transaction().add(eraseData, transferLamports);
		const transaction = new Transaction().add(eraseData);
		transaction.recentBlockhash = blockhash;
		transaction.feePayer = this.walletKey;
		// transaction.partialSign(account);
		
		const signedTransaction:Transaction | null = await this.wallet.signTransaction(transaction);
		if(signedTransaction)
		{
			const signature = await connection.sendRawTransaction(signedTransaction.serialize());
			const result = connection.confirmTransaction(signature, "singleGossip");
	
			console.log("Delete User Account Confirmation:", result);
				
			return [signature, this.account];
		}
		return null;
	}
}