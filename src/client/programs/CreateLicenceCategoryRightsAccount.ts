import { PublicKey, Transaction, TransactionSignature } from "@solana/web3.js";
import { connect } from "../core/connection";
import { CREATE_RIGHT_PROGRAM } from "../core/consts";
import IProgram from "../core/IProgram";
import { createLicenceCategoryAccountSeed, createLicenceCategoryRightsAccountSeed } from "../core/seed";
import { IWallet } from "../core/wallet";
import LicenceCategory from "../data/LicenceCategory";
import LicenceCategoryRights from "../data/LicenceCategoryRights";
import { createAccountWithSeed } from "../instructions/account";
import { createLicenceCategory, createLicenceCategoryRights } from "../instructions/rights";

export default class CreateLicenceCategoryRightsAccount implements IProgram<[TransactionSignature, PublicKey] | null> 
{
	data: LicenceCategoryRights;
	wallet: IWallet;
	walletKey: PublicKey;
	cluster: string;

	constructor(wallet: IWallet, walletKey:PublicKey, data: LicenceCategoryRights, cluster:string) {
		this.data = data;
		this.wallet = wallet;
		this.walletKey = walletKey;
		this.cluster = cluster;
	}

	async run(): Promise<[TransactionSignature, PublicKey] | null> 
	{
		const connection = await connect(this.cluster);
		const transaction = new Transaction();
		const programId = new PublicKey(CREATE_RIGHT_PROGRAM);
		const seed = await createLicenceCategoryRightsAccountSeed(this.data.salt.toBase58(), this.data.category);
		let { blockhash } = await connection.getRecentBlockhash();
        const [accountInstruction, account] = await createAccountWithSeed(this.walletKey, seed, this.data.getSize(), programId, this.cluster);
		
		transaction.add(accountInstruction);
		transaction.add(createLicenceCategoryRights(account, false, true, programId, this.data));
		transaction.recentBlockhash = blockhash;
		transaction.feePayer = this.walletKey;
		
		const signedTransaction:Transaction | null = await this.wallet.signTransaction(transaction);
		if(signedTransaction)
		{
			const signature = await connection.sendRawTransaction(signedTransaction.serialize());
			const result = connection.confirmTransaction(signature, "singleGossip");
	
			console.log("Create Licence Category Account Confirmation:", result);
				
			return [signature, account];
		}
		return null;
	}
}