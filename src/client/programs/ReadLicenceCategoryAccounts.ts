import IProgram from "../core/IProgram";
import { PublicKey } from "@solana/web3.js";
import { connect } from "../core/connection";
import LicenceCategory from "../data/LicenceCategory";
import { RightProgramTypes } from '../core/enums';
import bs58 from 'bs58';

export default class ReadLicenceCategoryAccounts implements IProgram<{categories:Array<LicenceCategory>, keys:Array<PublicKey>}>
{
    programId:PublicKey;
    cluster:string;
    
    constructor(programId:PublicKey, cluster:string)
    {
        this.programId = programId;
        this.cluster = cluster;
    }

    async run(): Promise<{categories:Array<LicenceCategory>, keys:Array<PublicKey>}>
    {
        const categories = new Array<LicenceCategory>();
        const keys = new Array<PublicKey>();
        try
        {
            const connection = await connect(this.cluster);
            const accounts = await connection.getProgramAccounts(this.programId, {
                filters:[
                    {
                        memcmp:{
                            offset:0,
                            bytes: bs58.encode(Buffer.from([RightProgramTypes.LicenceCategory]))
                        }
                    }
                ]
            });
            for(let i=0; i<accounts.length; i++)
            {
                categories.push(LicenceCategory.deserialize(accounts[i].account.data));
                keys.push(accounts[i].pubkey);
            }
        }
        catch(err){console.log("Read account faild!", err)}
        return {categories, keys};
    }
    
}