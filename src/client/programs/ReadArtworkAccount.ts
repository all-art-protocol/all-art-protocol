import { read } from "../core/account";
import IProgram from "../core/IProgram";
import UserAccount from "../data/UserAccount";
import * as borsh from 'borsh';
import { AccountInfo, ParsedAccountData, PublicKey } from "@solana/web3.js";
import { connect } from "../core/connection";
import Right from "../data/Right";
import NFTPro from "../data/NFTPro";

export default class ReadArtworkAccount implements IProgram<NFTPro>
{
    pubKey:PublicKey;
    cluster:string;
    
    constructor(pubKey:PublicKey, cluster:string)
    {
        this.pubKey = pubKey;
        this.cluster = cluster;
    }

    async run(): Promise<NFTPro | null>
    {
        try
        {
            const [account, pubkey] = await read(this.pubKey, this.cluster);
            if(account?.data)
                return NFTPro.deserialize(account.data);
        }
        catch(err){console.log("Read account faild!", err)}
        return null;
    }    
}