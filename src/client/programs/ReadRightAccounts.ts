import IProgram from "../core/IProgram";
import bs58 from 'bs58';
import { PublicKey } from "@solana/web3.js";
import { connect } from "../core/connection";
import Right from "../data/Right";
import { RightProgramTypes } from "../core/enums";

export default class ReadRightAccounts implements IProgram<{rights:Array<Right>, keys:Array<PublicKey>}>
{
    programId:PublicKey;
    cluster:string;
    
    constructor(programId:PublicKey, cluster:string)
    {
        this.programId = programId;
        this.cluster = cluster;
    }

    async run(): Promise<{rights:Array<Right>, keys:Array<PublicKey>}>
    {
        const rights = new Array<Right>();
        const keys = new Array<PublicKey>();
        try
        {
            const connection = await connect(this.cluster);
            const accounts = await connection.getProgramAccounts(this.programId, {
                filters:[
                    {
                        memcmp:{
                            offset:0,
                            bytes: bs58.encode(Buffer.from([RightProgramTypes.Right]))
                        }
                    }
                ]
            });
            for(let i=0; i<accounts.length; i++)
            {
                rights.push(Right.deserialize(accounts[i].account.data));
                keys.push(accounts[i].pubkey);
            }
        }
        catch(err){console.log("Read account faild!", err)}
        return {rights, keys};
    }
    
}