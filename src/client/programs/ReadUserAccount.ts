import { read } from "../core/account";
import IProgram from "../core/IProgram";
import UserAccount from "../data/UserAccount";
import * as borsh from 'borsh';
import { PublicKey } from "@solana/web3.js";

export default class ReadUserAccount implements IProgram<UserAccount>
{
    pubKey:PublicKey;
    cluster:string;
    
    constructor(pubKey:PublicKey, cluster:string)
    {
        this.pubKey = pubKey;
        this.cluster = cluster;
    }

    async run(): Promise<UserAccount | null>
    {
        try
        {
            const [account, pubkey] = await read(this.pubKey, this.cluster);
            if(account?.data)
                return UserAccount.deserialize(account?.data);
        }
        catch(err){console.log("Read account faild!", err)}
        return null;
    }
    
}