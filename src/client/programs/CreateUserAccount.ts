import { PublicKey, Transaction, TransactionSignature } from "@solana/web3.js";
import IProgram from "../core/IProgram";
import { IWallet } from "../core/wallet";
import { connect } from '../core/connection';
import { read } from '../core/account';
import { createAccountWithSeed, createUserAccount } from '../instructions/account';
import UserAccount from "../data/UserAccount";
import { CREATE_USER_PROGRAM } from "../core/consts";
import { createUserAccountSeed } from "../core/seed";

export default class CreateUserAccount implements IProgram<[TransactionSignature, PublicKey] | null>
{
    data: UserAccount;
    wallet: IWallet;
    walletKey: PublicKey;
    cluster:string;
    
    constructor(wallet:IWallet, walletKey:PublicKey, data:UserAccount, cluster:string)
    {
        this.data = data;
        this.wallet = wallet;
        this.walletKey = walletKey;
        this.cluster = cluster;
    }

    async run(): Promise<[TransactionSignature, PublicKey] | null>
    {
		const connection = await connect(this.cluster);
		const transaction = new Transaction();
		const programId = new PublicKey(CREATE_USER_PROGRAM);
		
		let accountSeed = await createUserAccountSeed(this.walletKey);
		var [accountInfo, accountPubkey] = await read(await PublicKey.createWithSeed(this.walletKey, accountSeed, programId), this.cluster);
		if(accountInfo === null)
		{
			var [crateAccountInstruction, accountPubkey] = await createAccountWithSeed(this.walletKey, accountSeed, this.data.getSize(), programId, this.cluster);
			transaction.add(crateAccountInstruction);
		}
		transaction.add(createUserAccount(accountPubkey, false, true, programId, this.data));
		let { blockhash } = await connection.getRecentBlockhash();
		transaction.recentBlockhash = blockhash;
		transaction.feePayer = this.walletKey;
		
		const signedTransaction:Transaction | null = await this.wallet.signTransaction(transaction);
		if(signedTransaction)
		{
			const signature = await connection.sendRawTransaction(signedTransaction.serialize());
			const result = connection.confirmTransaction(signature, "singleGossip");

			console.log("Create User Account Confirmation:", result);
			
			return [signature, accountPubkey];
		}
		return null;
    }
}